<?php
   include_once('module_session.php');
   @session_start() ;
   $_SESSION['scriptcase']['module']['glo_nm_perfil']          = "conn_mysql";
   $_SESSION['scriptcase']['module']['glo_nm_path_prod']       = "";
   $_SESSION['scriptcase']['module']['glo_nm_path_conf']       = "";
   $_SESSION['scriptcase']['module']['glo_nm_path_imagens']    = "";
   $_SESSION['scriptcase']['module']['glo_nm_path_imag_temp']  = "";
   $_SESSION['scriptcase']['module']['glo_nm_path_doc']        = "";
    //check publication with the prod
    $NM_dir_atual = getcwd();
    if (empty($NM_dir_atual))
    {
        $str_path_sys          = (isset($_SERVER['SCRIPT_FILENAME'])) ? $_SERVER['SCRIPT_FILENAME'] : $_SERVER['ORIG_PATH_TRANSLATED'];
        $str_path_sys          = str_replace("\\", '/', $str_path_sys);
    }
    else
    {
        $sc_nm_arquivo         = explode("/", $_SERVER['PHP_SELF']);
        $str_path_sys          = str_replace("\\", "/", getcwd()) . "/" . $sc_nm_arquivo[count($sc_nm_arquivo)-1];
    }
    $str_path_apl_url = $_SERVER['PHP_SELF'];
    $str_path_apl_url = str_replace("\\", '/', $str_path_apl_url);
    $str_path_apl_url = substr($str_path_apl_url, 0, strrpos($str_path_apl_url, "/"));
    $str_path_apl_url = substr($str_path_apl_url, 0, strrpos($str_path_apl_url, "/")+1);
    $str_path_apl_dir = substr($str_path_sys, 0, strrpos($str_path_sys, "/"));
    $str_path_apl_dir = substr($str_path_apl_dir, 0, strrpos($str_path_apl_dir, "/")+1);
    //check prod
    if(empty($_SESSION['scriptcase']['module']['glo_nm_path_prod']))
    {
            /*check prod*/$_SESSION['scriptcase']['module']['glo_nm_path_prod'] = $str_path_apl_url . "_lib/prod";
    }
    //check img
    if(empty($_SESSION['scriptcase']['module']['glo_nm_path_imagens']))
    {
            /*check img*/$_SESSION['scriptcase']['module']['glo_nm_path_imagens'] = $str_path_apl_url . "_lib/file/img";
    }
    //check tmp
    if(empty($_SESSION['scriptcase']['module']['glo_nm_path_imag_temp']))
    {
            /*check tmp*/$_SESSION['scriptcase']['module']['glo_nm_path_imag_temp'] = $str_path_apl_url . "_lib/tmp";
    }
    //check doc
    if(empty($_SESSION['scriptcase']['module']['glo_nm_path_doc']))
    {
            /*check doc*/$_SESSION['scriptcase']['module']['glo_nm_path_doc'] = $str_path_apl_dir . "_lib/file/doc";
    }
    //end check publication with the prod
//
class module_ini
{
   var $nm_cod_apl;
   var $nm_nome_apl;
   var $nm_seguranca;
   var $nm_grupo;
   var $nm_autor;
   var $nm_versao_sc;
   var $nm_tp_lic_sc;
   var $nm_dt_criacao;
   var $nm_hr_criacao;
   var $nm_autor_alt;
   var $nm_dt_ult_alt;
   var $nm_hr_ult_alt;
   var $nm_timestamp;
   var $nm_app_version;
   var $cor_link_dados;
   var $root;
   var $server;
   var $java_protocol;
   var $server_pdf;
   var $Arr_result;
   var $sc_protocolo;
   var $path_prod;
   var $path_link;
   var $path_aplicacao;
   var $path_embutida;
   var $path_botoes;
   var $path_img_global;
   var $path_img_modelo;
   var $path_icones;
   var $path_imagens;
   var $path_imag_cab;
   var $path_imag_temp;
   var $path_libs;
   var $path_doc;
   var $str_lang;
   var $str_conf_reg;
   var $str_schema_all;
   var $Str_btn_grid;
   var $path_cep;
   var $path_secure;
   var $path_js;
   var $path_help;
   var $path_adodb;
   var $path_grafico;
   var $path_atual;
   var $Gd_missing;
   var $sc_site_ssl;
   var $nm_falta_var;
   var $nm_falta_var_db;
   var $nm_tpbanco;
   var $nm_servidor;
   var $nm_usuario;
   var $nm_senha;
   var $nm_database_encoding;
   var $nm_arr_db_extra_args = array();
   var $nm_con_db2 = array();
   var $nm_con_persistente;
   var $nm_con_use_schema;
   var $nm_tabela;
   var $nm_ger_css_emb;
   var $sc_tem_trans_banco;
   var $nm_bases_all;
   var $nm_bases_access;
   var $nm_bases_db2;
   var $nm_bases_ibase;
   var $nm_bases_informix;
   var $nm_bases_mssql;
   var $nm_bases_mysql;
   var $nm_bases_postgres;
   var $nm_bases_oracle;
   var $nm_bases_sqlite;
   var $nm_bases_sybase;
   var $nm_bases_vfp;
   var $nm_bases_odbc;
   var $nm_bases_progress;
   var $sc_page;
   var $sc_lig_md5 = array();
   var $sc_lig_target = array();
   var $sc_export_ajax = false;
   var $sc_export_ajax_img = false;
//
   function init($Tp_init = "")
   {
       global
             $nm_url_saida, $nm_apl_dependente, $script_case_init, $nmgp_opcao;

      if (!function_exists("sc_check_mobile"))
      {
          include_once("../_lib/lib/php/nm_check_mobile.php");
      }
      $_SESSION['scriptcase']['proc_mobile'] = sc_check_mobile();
      @ini_set('magic_quotes_runtime', 0);
      $this->sc_page = $script_case_init;
      $_SESSION['scriptcase']['sc_num_page'] = $script_case_init;
      $_SESSION['scriptcase']['sc_cnt_sql']  = 0;
      $this->sc_charset['UTF-8'] = 'utf-8';
      $this->sc_charset['ISO-2022-JP'] = 'iso-2022-jp';
      $this->sc_charset['ISO-2022-KR'] = 'iso-2022-kr';
      $this->sc_charset['ISO-8859-1'] = 'iso-8859-1';
      $this->sc_charset['ISO-8859-2'] = 'iso-8859-2';
      $this->sc_charset['ISO-8859-3'] = 'iso-8859-3';
      $this->sc_charset['ISO-8859-4'] = 'iso-8859-4';
      $this->sc_charset['ISO-8859-5'] = 'iso-8859-5';
      $this->sc_charset['ISO-8859-6'] = 'iso-8859-6';
      $this->sc_charset['ISO-8859-7'] = 'iso-8859-7';
      $this->sc_charset['ISO-8859-8'] = 'iso-8859-8';
      $this->sc_charset['ISO-8859-8-I'] = 'iso-8859-8-i';
      $this->sc_charset['ISO-8859-9'] = 'iso-8859-9';
      $this->sc_charset['ISO-8859-10'] = 'iso-8859-10';
      $this->sc_charset['ISO-8859-13'] = 'iso-8859-13';
      $this->sc_charset['ISO-8859-14'] = 'iso-8859-14';
      $this->sc_charset['ISO-8859-15'] = 'iso-8859-15';
      $this->sc_charset['WINDOWS-1250'] = 'windows-1250';
      $this->sc_charset['WINDOWS-1251'] = 'windows-1251';
      $this->sc_charset['WINDOWS-1252'] = 'windows-1252';
      $this->sc_charset['TIS-620'] = 'tis-620';
      $this->sc_charset['WINDOWS-1253'] = 'windows-1253';
      $this->sc_charset['WINDOWS-1254'] = 'windows-1254';
      $this->sc_charset['WINDOWS-1255'] = 'windows-1255';
      $this->sc_charset['WINDOWS-1256'] = 'windows-1256';
      $this->sc_charset['WINDOWS-1257'] = 'windows-1257';
      $this->sc_charset['KOI8-R'] = 'koi8-r';
      $this->sc_charset['BIG-5'] = 'big5';
      $this->sc_charset['EUC-CN'] = 'EUC-CN';
      $this->sc_charset['GB18030'] = 'GB18030';
      $this->sc_charset['GB2312'] = 'gb2312';
      $this->sc_charset['EUC-JP'] = 'euc-jp';
      $this->sc_charset['SJIS'] = 'shift-jis';
      $this->sc_charset['EUC-KR'] = 'euc-kr';
      $_SESSION['scriptcase']['charset_entities']['UTF-8'] = 'UTF-8';
      $_SESSION['scriptcase']['charset_entities']['ISO-8859-1'] = 'ISO-8859-1';
      $_SESSION['scriptcase']['charset_entities']['ISO-8859-5'] = 'ISO-8859-5';
      $_SESSION['scriptcase']['charset_entities']['ISO-8859-15'] = 'ISO-8859-15';
      $_SESSION['scriptcase']['charset_entities']['WINDOWS-1251'] = 'cp1251';
      $_SESSION['scriptcase']['charset_entities']['WINDOWS-1252'] = 'cp1252';
      $_SESSION['scriptcase']['charset_entities']['BIG-5'] = 'BIG5';
      $_SESSION['scriptcase']['charset_entities']['EUC-CN'] = 'GB2312';
      $_SESSION['scriptcase']['charset_entities']['GB2312'] = 'GB2312';
      $_SESSION['scriptcase']['charset_entities']['SJIS'] = 'Shift_JIS';
      $_SESSION['scriptcase']['charset_entities']['EUC-JP'] = 'EUC-JP';
      $_SESSION['scriptcase']['charset_entities']['KOI8-R'] = 'KOI8-R';
      $_SESSION['scriptcase']['trial_version'] = 'N';
      $_SESSION['sc_session'][$this->sc_page]['module']['decimal_db'] = "."; 

      $this->nm_cod_apl      = "module"; 
      $this->nm_nome_apl     = ""; 
      $this->nm_seguranca    = ""; 
      $this->nm_grupo        = "Service_Call_Addict"; 
      $this->nm_grupo_versao = "1"; 
      $this->nm_autor        = "fahmy"; 
      $this->nm_script_by    = "netmake";
      $this->nm_script_type  = "PHP";
      $this->nm_versao_sc    = "v9"; 
      $this->nm_tp_lic_sc    = "ep_bronze"; 
      $this->nm_dt_criacao   = "20190211"; 
      $this->nm_hr_criacao   = "131541"; 
      $this->nm_autor_alt    = "fahmy"; 
      $this->nm_dt_ult_alt   = "20190228"; 
      $this->nm_hr_ult_alt   = "103324"; 
      $this->Apl_paginacao   = "PARCIAL"; 
      $temp_bug_list         = explode(" ", microtime()); 
      list($NM_usec, $NM_sec) = $temp_bug_list; 
      $this->nm_timestamp    = (float) $NM_sec; 
      $this->nm_app_version  = "1.0.0";
// 
// 
      $NM_dir_atual = getcwd();
      if (empty($NM_dir_atual))
      {
          $str_path_sys          = (isset($_SERVER['SCRIPT_FILENAME'])) ? $_SERVER['SCRIPT_FILENAME'] : $_SERVER['ORIG_PATH_TRANSLATED'];
          $str_path_sys          = str_replace("\\", '/', $str_path_sys);
      }
      else
      {
          $sc_nm_arquivo         = explode("/", $_SERVER['PHP_SELF']);
          $str_path_sys          = str_replace("\\", "/", getcwd()) . "/" . $sc_nm_arquivo[count($sc_nm_arquivo)-1];
      }
      $this->sc_site_ssl     = $this->appIsSsl();
      $this->sc_protocolo    = $this->sc_site_ssl ? 'https://' : 'http://';
      $this->sc_protocolo    = "";
      $this->path_prod       = $_SESSION['scriptcase']['module']['glo_nm_path_prod'];
      $this->path_conf       = $_SESSION['scriptcase']['module']['glo_nm_path_conf'];
      $this->path_imagens    = $_SESSION['scriptcase']['module']['glo_nm_path_imagens'];
      $this->path_imag_temp  = $_SESSION['scriptcase']['module']['glo_nm_path_imag_temp'];
      $this->path_doc        = $_SESSION['scriptcase']['module']['glo_nm_path_doc'];
      if (!isset($_SESSION['scriptcase']['str_lang']) || empty($_SESSION['scriptcase']['str_lang']))
      {
          $_SESSION['scriptcase']['str_lang'] = "id";
      }
      if (!isset($_SESSION['scriptcase']['str_conf_reg']) || empty($_SESSION['scriptcase']['str_conf_reg']))
      {
          $_SESSION['scriptcase']['str_conf_reg'] = "id_id";
      }
      $this->str_lang        = $_SESSION['scriptcase']['str_lang'];
      $this->str_conf_reg    = $_SESSION['scriptcase']['str_conf_reg'];
      $this->str_schema_all    = (isset($_SESSION['scriptcase']['str_schema_all']) && !empty($_SESSION['scriptcase']['str_schema_all'])) ? $_SESSION['scriptcase']['str_schema_all'] : "SCP_1/SCP_1";
      $_SESSION['scriptcase']['erro']['str_schema'] = $this->str_schema_all . "_error.css";
      $_SESSION['scriptcase']['erro']['str_lang']   = $this->str_lang;
      $this->server          = (!isset($_SERVER['HTTP_HOST'])) ? $_SERVER['SERVER_NAME'] : $_SERVER['HTTP_HOST'];
      if (!isset($_SERVER['HTTP_HOST']) && isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] != 80 && !$this->sc_site_ssl )
      {
          $this->server         .= ":" . $_SERVER['SERVER_PORT'];
      }
      $this->java_protocol   = ($this->sc_site_ssl) ? 'https://' : 'http://';
      $this->server_pdf      = $this->java_protocol . $this->server;
      $this->server          = "";
      $str_path_web          = $_SERVER['PHP_SELF'];
      $str_path_web          = str_replace("\\", '/', $str_path_web);
      $str_path_web          = str_replace('//', '/', $str_path_web);
      $this->root            = substr($str_path_sys, 0, -1 * strlen($str_path_web));
      $this->path_aplicacao  = substr($str_path_sys, 0, strrpos($str_path_sys, '/'));
      $this->path_aplicacao  = substr($this->path_aplicacao, 0, strrpos($this->path_aplicacao, '/')) . '/module';
      $this->path_embutida   = substr($this->path_aplicacao, 0, strrpos($this->path_aplicacao, '/') + 1);
      $this->path_aplicacao .= '/';
      $this->path_link       = substr($str_path_web, 0, strrpos($str_path_web, '/'));
      $this->path_link       = substr($this->path_link, 0, strrpos($this->path_link, '/')) . '/';
      $this->path_botoes     = $this->path_link . "_lib/img";
      $this->path_img_global = $this->path_link . "_lib/img";
      $this->path_img_modelo = $this->path_link . "_lib/img";
      $this->path_icones     = $this->path_link . "_lib/img";
      $this->path_imag_cab   = $this->path_link . "_lib/img";
      $this->path_help       = $this->path_link . "_lib/webhelp/";
      $this->path_font       = $this->root . $this->path_link . "_lib/font/";
      $this->path_btn        = $this->root . $this->path_link . "_lib/buttons/";
      $this->path_css        = $this->root . $this->path_link . "_lib/css/";
      $this->path_lib_php    = $this->root . $this->path_link . "_lib/lib/php";
      $this->path_lib_js     = $this->root . $this->path_link . "_lib/lib/js";
      $this->path_lang       = "../_lib/lang/";
      $this->path_lang_js    = "../_lib/js/";
      $this->path_chart_theme = $this->root . $this->path_link . "_lib/chart/";
      $this->path_cep        = $this->path_prod . "/cep";
      $this->path_cor        = $this->path_prod . "/cor";
      $this->path_js         = $this->path_prod . "/lib/js";
      $this->path_libs       = $this->root . $this->path_prod . "/lib/php";
      $this->path_third      = $this->root . $this->path_prod . "/third";
      $this->path_secure     = $this->root . $this->path_prod . "/secure";
      $this->path_adodb      = $this->root . $this->path_prod . "/third/adodb";
      $_SESSION['scriptcase']['dir_temp'] = $this->root . $this->path_imag_temp;
      if (isset($_SESSION['scriptcase']['module']['session_timeout']['lang'])) {
          $this->str_lang = $_SESSION['scriptcase']['module']['session_timeout']['lang'];
      }
      elseif (!isset($_SESSION['scriptcase']['module']['actual_lang']) || $_SESSION['scriptcase']['module']['actual_lang'] != $this->str_lang) {
          $_SESSION['scriptcase']['module']['actual_lang'] = $this->str_lang;
          $SC_arq_lang = fopen($_SESSION['scriptcase']['dir_temp'] . "/sc_actual_lang_" . $_SERVER['SERVER_NAME'] . ".txt", "w");
          fwrite ($SC_arq_lang, $this->str_lang);
          fclose ($SC_arq_lang);
      }
      if (!isset($_SESSION['scriptcase']['fusioncharts_new']))
      {
          $_SESSION['scriptcase']['fusioncharts_new'] = @is_dir($this->path_third . '/oem_fs');
      }
      if (!isset($_SESSION['scriptcase']['phantomjs_charts']))
      {
          $_SESSION['scriptcase']['phantomjs_charts'] = @is_dir($this->path_third . '/phantomjs');
      }
      if (isset($_SESSION['scriptcase']['phantomjs_charts']))
      {
          $aTmpOS = $this->getRunningOS();
          $_SESSION['scriptcase']['phantomjs_charts'] = @is_dir($this->path_third . '/phantomjs/' . $aTmpOS['os']);
      }
      if (!class_exists('Services_JSON'))
      {
          include_once("module_json.php");
      }
      $this->SC_Link_View = (isset($_SESSION['sc_session'][$this->sc_page]['module']['SC_Link_View'])) ? $_SESSION['sc_session'][$this->sc_page]['module']['SC_Link_View'] : false;
      if (isset($_GET['SC_Link_View']) && !empty($_GET['SC_Link_View']) && is_numeric($_GET['SC_Link_View']))
      {
          if ($_SESSION['sc_session'][$this->sc_page]['module']['embutida'])
          {
              $this->SC_Link_View = true;
              $_SESSION['sc_session'][$this->sc_page]['module']['SC_Link_View'] = true;
          }
      }
      if (isset($_POST['nmgp_opcao']) && $_POST['nmgp_opcao'] == "ajax_save_ancor")
      {
          $_SESSION['sc_session'][$this->sc_page]['module']['ancor_save'] = $_POST['ancor_save'];
          $oJson = new Services_JSON();
          exit;
      }
      if (isset($_SESSION['scriptcase']['user_logout']))
      {
          foreach ($_SESSION['scriptcase']['user_logout'] as $ind => $parms)
          {
              if (isset($_SESSION[$parms['V']]) && $_SESSION[$parms['V']] == $parms['U'])
              {
                  unset($_SESSION['scriptcase']['user_logout'][$ind]);
                  $nm_apl_dest = $parms['R'];
                  $dir = explode("/", $nm_apl_dest);
                  if (count($dir) == 1)
                  {
                      $nm_apl_dest = str_replace(".php", "", $nm_apl_dest);
                      $nm_apl_dest = $this->path_link . SC_dir_app_name($nm_apl_dest) . "/";
                  }
                  if (isset($_POST['nmgp_opcao']) && ($_POST['nmgp_opcao'] == "ajax_event" || $_POST['nmgp_opcao'] == "ajax_navigate"))
                  {
                      $this->Arr_result = array();
                      $this->Arr_result['redirInfo']['action']              = $nm_apl_dest;
                      $this->Arr_result['redirInfo']['target']              = $parms['T'];
                      $this->Arr_result['redirInfo']['metodo']              = "post";
                      $this->Arr_result['redirInfo']['script_case_init']    = $this->sc_page;
                      $this->Arr_result['redirInfo']['script_case_session'] = session_id();
                      $oJson = new Services_JSON();
                      echo $oJson->encode($this->Arr_result);
                      exit;
                  }
?>
                  <html>
                  <body>
                  <form name="FRedirect" method="POST" action="<?php echo $nm_apl_dest; ?>" target="<?php echo $parms['T']; ?>">
                  </form>
                  <script>
                   document.FRedirect.submit();
                  </script>
                  </body>
                  </html>
<?php
                  exit;
              }
          }
      }
      global $under_dashboard, $dashboard_app, $own_widget, $parent_widget, $compact_mode;
      if (!isset($_SESSION['sc_session'][$this->sc_page]['module']['dashboard_info']['under_dashboard']))
      {
          $_SESSION['sc_session'][$this->sc_page]['module']['dashboard_info']['under_dashboard'] = false;
          $_SESSION['sc_session'][$this->sc_page]['module']['dashboard_info']['dashboard_app']   = '';
          $_SESSION['sc_session'][$this->sc_page]['module']['dashboard_info']['own_widget']      = '';
          $_SESSION['sc_session'][$this->sc_page]['module']['dashboard_info']['parent_widget']   = '';
          $_SESSION['sc_session'][$this->sc_page]['module']['dashboard_info']['compact_mode']    = false;
      }
      if (isset($_GET['under_dashboard']) && 1 == $_GET['under_dashboard'])
      {
          if (isset($_GET['own_widget']) && 'dbifrm_widget' == substr($_GET['own_widget'], 0, 13)) {
              $_SESSION['sc_session'][$this->sc_page]['module']['dashboard_info']['own_widget'] = $_GET['own_widget'];
              $_SESSION['sc_session'][$this->sc_page]['module']['dashboard_info']['under_dashboard'] = true;
              if (isset($_GET['dashboard_app'])) {
                  $_SESSION['sc_session'][$this->sc_page]['module']['dashboard_info']['dashboard_app'] = $_GET['dashboard_app'];
              }
              if (isset($_GET['parent_widget'])) {
                  $_SESSION['sc_session'][$this->sc_page]['module']['dashboard_info']['parent_widget'] = $_GET['parent_widget'];
              }
              if (isset($_GET['compact_mode'])) {
                  $_SESSION['sc_session'][$this->sc_page]['module']['dashboard_info']['compact_mode'] = 1 == $_GET['compact_mode'];
              }
          }
      }
      elseif (isset($under_dashboard) && 1 == $under_dashboard)
      {
          if (isset($own_widget) && 'dbifrm_widget' == substr($own_widget, 0, 13)) {
              $_SESSION['sc_session'][$this->sc_page]['module']['dashboard_info']['own_widget'] = $own_widget;
              $_SESSION['sc_session'][$this->sc_page]['module']['dashboard_info']['under_dashboard'] = true;
              if (isset($dashboard_app)) {
                  $_SESSION['sc_session'][$this->sc_page]['module']['dashboard_info']['dashboard_app'] = $dashboard_app;
              }
              if (isset($parent_widget)) {
                  $_SESSION['sc_session'][$this->sc_page]['module']['dashboard_info']['parent_widget'] = $parent_widget;
              }
              if (isset($compact_mode)) {
                  $_SESSION['sc_session'][$this->sc_page]['module']['dashboard_info']['compact_mode'] = 1 == $compact_mode;
              }
          }
      }
      if (!isset($_SESSION['sc_session'][$this->sc_page]['module']['dashboard_info']['maximized']))
      {
          $_SESSION['sc_session'][$this->sc_page]['module']['dashboard_info']['maximized'] = false;
      }
      if (isset($_GET['maximized']))
      {
          $_SESSION['sc_session'][$this->sc_page]['module']['dashboard_info']['maximized'] = 1 == $_GET['maximized'];
      }
      if ($_SESSION['sc_session'][$this->sc_page]['module']['dashboard_info']['under_dashboard'])
      {
          $sTmpDashboardApp = $_SESSION['sc_session'][$this->sc_page]['module']['dashboard_info']['dashboard_app'];
          if ('' != $sTmpDashboardApp && isset($_SESSION['scriptcase']['dashboard_targets'][$sTmpDashboardApp]["module"]))
          {
              foreach ($_SESSION['scriptcase']['dashboard_targets'][$sTmpDashboardApp]["module"] as $sTmpTargetLink => $sTmpTargetWidget)
              {
                  if (isset($this->sc_lig_target[$sTmpTargetLink]))
                  {
                      $this->sc_lig_target[$sTmpTargetLink] = $sTmpTargetWidget;
                  }
              }
          }
      }
      if ($Tp_init == "Path_sub")
      {
          return;
      }
      $str_path = substr($this->path_prod, 0, strrpos($this->path_prod, '/') + 1);
      if (!is_file($this->root . $str_path . 'devel/class/xmlparser/nmXmlparserIniSys.class.php'))
      {
          unset($_SESSION['scriptcase']['nm_sc_retorno']);
          unset($_SESSION['scriptcase']['module']['glo_nm_conexao']);
      }
      include($this->path_lang . $this->str_lang . ".lang.php");
      include($this->path_lang . "config_region.php");
      include($this->path_lang . "lang_config_region.php");
      asort($this->Nm_lang_conf_region);
      $_SESSION['scriptcase']['charset']  = "UTF-8";
      ini_set('default_charset', $_SESSION['scriptcase']['charset']);
      $_SESSION['scriptcase']['charset_html']  = (isset($this->sc_charset[$_SESSION['scriptcase']['charset']])) ? $this->sc_charset[$_SESSION['scriptcase']['charset']] : $_SESSION['scriptcase']['charset'];
      if (!function_exists("mb_convert_encoding"))
      {
          echo "<div><font size=6>" . $this->Nm_lang['lang_othr_prod_xtmb'] . "</font></div>";exit;
      } 
      elseif (!function_exists("sc_convert_encoding"))
      {
          echo "<div><font size=6>" . $this->Nm_lang['lang_othr_prod_xtsc'] . "</font></div>";exit;
      } 
      foreach ($this->Nm_lang_conf_region as $ind => $dados)
      {
         if ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($dados))
         {
             $this->Nm_lang_conf_region[$ind] = sc_convert_encoding($dados, $_SESSION['scriptcase']['charset'], "UTF-8");
         }
      }
      foreach ($this->Nm_conf_reg[$this->str_conf_reg] as $ind => $dados)
      {
         if ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($dados))
         {
             $this->Nm_conf_reg[$this->str_conf_reg][$ind] = sc_convert_encoding($dados, $_SESSION['scriptcase']['charset'], "UTF-8");
         }
      }
      foreach ($this->Nm_lang as $ind => $dados)
      {
         if ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($ind))
         {
             $ind = sc_convert_encoding($ind, $_SESSION['scriptcase']['charset'], "UTF-8");
             $this->Nm_lang[$ind] = $dados;
         }
         if ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($dados))
         {
             $this->Nm_lang[$ind] = sc_convert_encoding($dados, $_SESSION['scriptcase']['charset'], "UTF-8");
         }
      }
      $_SESSION['sc_session']['SC_download_violation'] = $this->Nm_lang['lang_errm_fnfd'];
      if (isset($_SESSION['sc_session']['SC_parm_violation']) && !isset($_SESSION['scriptcase']['module']['session_timeout']['redir']))
      {
          unset($_SESSION['sc_session']['SC_parm_violation']);
          echo "<html>";
          echo "<body>";
          echo "<table align=\"center\" width=\"50%\" border=1 height=\"50px\">";
          echo "<tr>";
          echo "   <td align=\"center\">";
          echo "       <b><font size=4>" . $this->Nm_lang['lang_errm_ajax_data'] . "</font>";
          echo "   </b></td>";
          echo " </tr>";
          echo "</table>";
          echo "</body>";
          echo "</html>";
          exit;
      }
      if (isset($this->Nm_lang['lang_errm_dbcn_conn']))
      {
          $_SESSION['scriptcase']['db_conn_error'] = $this->Nm_lang['lang_errm_dbcn_conn'];
      }
      $PHP_ver = str_replace(".", "", phpversion()); 
      if (substr($PHP_ver, 0, 3) < 434)
      {
          echo "<div><font size=6>" . $this->Nm_lang['lang_othr_prod_phpv'] . "</font></div>";exit;
      } 
      if (file_exists($this->path_libs . "/ver.dat"))
      {
          $SC_ver = file($this->path_libs . "/ver.dat"); 
          $SC_ver = str_replace(".", "", $SC_ver[0]); 
          if (substr($SC_ver, 0, 5) < 40015)
          {
              echo "<div><font size=6>" . $this->Nm_lang['lang_othr_prod_incp'] . "</font></div>";exit;
          } 
      } 
      $_SESSION['sc_session'][$this->sc_page]['module']['path_doc'] = $this->path_doc; 
      $_SESSION['scriptcase']['nm_path_prod'] = $this->root . $this->path_prod . "/"; 
      if (empty($this->path_imag_cab))
      {
          $this->path_imag_cab = $this->path_img_global;
      }
      if (!is_dir($this->root . $this->path_prod))
      {
          echo "<style type=\"text/css\">";
          echo ".scButton_default { font-family: Arial, sans-serif; font-size: 11px; color: #FFFFFF; font-weight: bold; border-style: none; border-width: 1px; padding: 3px 14px; background-image: url(../../img/scriptcase__NM__bgButtonGreen.jpg); }";
          echo ".scButton_disabled { font-family: Arial, sans-serif; font-size: 11px; color: #666666; font-weight: bold; border-style: none; border-width: 1px; padding: 3px 14px; background-image: url(../../img/scriptcase__NM__btn_cromo_off.png); }";
          echo ".scButton_onmousedown { font-family: Arial, sans-serif; font-size: 11px; color: #666666; font-weight: bold; border-style: none; border-width: 1px; padding: 3px 14px; background-image: url(../../img/scriptcase__NM__V7Softgraybgcalendar.png); }";
          echo ".scButton_onmouseover { font-family: Arial, sans-serif; font-size: 11px; color: #f3f3f3; font-weight: bold; background-color: #666666; border-style: none; border-width: 1px; padding: 3px 14px; background-image: url(../../img/scriptcase__NM__btn_cromo_off.png); }";
          echo ".scButton_small { font-family: Tahoma, Arial, sans-serif; font-size: 8px; color: #000000; font-weight: normal; background-color: #EEEEEE; border-style: solid; border-width: 1px; padding: 4px 8px;  }";
          echo ".scLink_default { text-decoration: underline; font-size: 12px; color: #0000AA;  }";
          echo ".scLink_default:visited { text-decoration: underline; font-size: 12px; color: #0000AA;  }";
          echo ".scLink_default:active { text-decoration: underline; font-size: 12px; color: #0000AA;  }";
          echo ".scLink_default:hover { text-decoration: none; font-size: 12px; color: #0000AA;  }";
          echo "</style>";
          echo "<table width=\"80%\" border=\"1\" height=\"117\">";
          echo "<tr>";
          echo "   <td bgcolor=\"\">";
          echo "       <b><font size=\"4\">" . $this->Nm_lang['lang_errm_cmlb_nfnd'] . "</font>";
          echo "  " . $this->root . $this->path_prod;
          echo "   </b></td>";
          echo " </tr>";
          echo "</table>";
          if (!$_SESSION['sc_session'][$script_case_init]['module']['iframe_menu'] && (!isset($_SESSION['sc_session'][$script_case_init]['module']['sc_outra_jan']) || !$_SESSION['sc_session'][$script_case_init]['module']['sc_outra_jan'])) 
          { 
              if (isset($_SESSION['scriptcase']['nm_sc_retorno']) && !empty($_SESSION['scriptcase']['nm_sc_retorno'])) 
              { 
               $btn_value = "" . $this->Ini->Nm_lang['lang_btns_back'] . "";
               if ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($btn_value))
               {
                   $btn_value = sc_convert_encoding($btn_value, $_SESSION['scriptcase']['charset'], "UTF-8");
               }
               $btn_hint = "" . $this->Ini->Nm_lang['lang_btns_back_hint'] . "";
               if ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($btn_hint))
               {
                   $btn_hint = sc_convert_encoding($btn_hint, $_SESSION['scriptcase']['charset'], "UTF-8");
               }
?>
                   <input type="button" id="sai" onClick="window.location='<?php echo $_SESSION['scriptcase']['nm_sc_retorno'] ?>'; return false" class="scButton_default" value="<?php echo $btn_value ?>" title="<?php echo $btn_hint ?>" style="vertical-align: middle;">

<?php
              } 
              else 
              { 
               $btn_value = "" . $this->Ini->Nm_lang['lang_btns_exit'] . "";
               if ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($btn_value))
               {
                   $btn_value = sc_convert_encoding($btn_value, $_SESSION['scriptcase']['charset'], "UTF-8");
               }
               $btn_hint = "" . $this->Ini->Nm_lang['lang_btns_exit_hint'] . "";
               if ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($btn_hint))
               {
                   $btn_hint = sc_convert_encoding($btn_hint, $_SESSION['scriptcase']['charset'], "UTF-8");
               }
?>
                   <input type="button" id="sai" onClick="window.location='<?php echo $nm_url_saida ?>'; return false" class="scButton_default" value="<?php echo $btn_value ?>" title="<?php echo $btn_hint ?>" style="vertical-align: middle;">

<?php
              } 
          } 
          exit ;
      }

      $this->nm_ger_css_emb = true;
      $this->path_atual     = getcwd();
      $opsys = strtolower(php_uname());

// 
      include_once($this->path_aplicacao . "module_erro.class.php"); 
      $this->Erro = new module_erro();
      include_once($this->path_adodb . "/adodb.inc.php"); 
      $this->sc_Include($this->path_libs . "/nm_sec_prod.php", "F", "nm_reg_prod") ; 
      $this->sc_Include($this->path_libs . "/nm_ini_perfil.php", "F", "perfil_lib") ; 
// 
 if(function_exists('set_php_timezone')) set_php_timezone('module'); 
// 
      $this->sc_Include($this->path_lib_php . "/nm_functions.php", "", "") ; 
      $this->sc_Include($this->path_lib_php . "/nm_api.php", "", "") ; 
      $this->sc_Include($this->path_lib_php . "/nm_edit.php", "F", "nmgp_Form_Num_Val") ; 
      $this->sc_Include($this->path_lib_php . "/nm_conv_dados.php", "F", "nm_conv_limpa_dado") ; 
      $this->sc_Include($this->path_lib_php . "/nm_data.class.php", "C", "nm_data") ; 
      $this->nm_data = new nm_data("id");
      include("../_lib/css/" . $this->str_schema_all . "_grid.php");
      $this->Tree_img_col    = trim($str_tree_col);
      $this->Tree_img_exp    = trim($str_tree_exp);
      $_SESSION['scriptcase']['nmamd'] = array();
      perfil_lib($this->path_libs);
      if (!isset($_SESSION['sc_session'][$this->sc_page]['SC_Check_Perfil']))
      {
          if(function_exists("nm_check_perfil_exists")) nm_check_perfil_exists($this->path_libs, $this->path_prod);
          $_SESSION['sc_session'][$this->sc_page]['SC_Check_Perfil'] = true;
      }
      if (function_exists("nm_check_pdf_server")) $this->server_pdf = nm_check_pdf_server($this->path_libs, $this->server_pdf);
      if (!isset($_SESSION['scriptcase']['sc_num_img']))
      { 
          $_SESSION['scriptcase']['sc_num_img'] = 1;
      } 
      $this->regionalDefault();
      $this->Str_btn_grid    = trim($str_button) . "/" . trim($str_button) . $_SESSION['scriptcase']['reg_conf']['css_dir'] . ".php";
      $this->Str_btn_css     = trim($str_button) . "/" . trim($str_button) . ".css";
      include($this->path_btn . $this->Str_btn_grid);
      $_SESSION['scriptcase']['erro']['str_schema_dir'] = $this->str_schema_all . "_error" . $_SESSION['scriptcase']['reg_conf']['css_dir'] . ".css";
      $this->sc_tem_trans_banco = false;
      if (isset($_SESSION['scriptcase']['module']['session_timeout']['redir'])) {
          $SS_cod_html  = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">';
          $SS_cod_html .= "<HTML>\r\n";
          $SS_cod_html .= " <HEAD>\r\n";
          $SS_cod_html .= "  <TITLE></TITLE>\r\n";
          $SS_cod_html .= "   <META http-equiv=\"Content-Type\" content=\"text/html; charset=" . $_SESSION['scriptcase']['charset_html'] . "/>\r\n";
          if ($_SESSION['scriptcase']['proc_mobile']) {
              $SS_cod_html .= "   <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0\"/>\r\n";
          }
          $SS_cod_html .= "   <META http-equiv=\"Expires\" content=\"Fri, Jan 01 1900 00:00:00 GMT\"/>\r\n";
          $SS_cod_html .= "    <META http-equiv=\"Pragma\" content=\"no-cache\"/>\r\n";
          if ($_SESSION['scriptcase']['module']['session_timeout']['redir_tp'] == "R") {
              $SS_cod_html .= "  </HEAD>\r\n";
              $SS_cod_html .= "   <body>\r\n";
          }
          else {
              $SS_cod_html .= "    <link rel=\"shortcut icon\" href=\"../_lib/img/scriptcase__NM__ico__NM__favicon.ico\">\r\n";
              $SS_cod_html .= "    <link rel=\"stylesheet\" type=\"text/css\" href=\"../_lib/css/" . $this->str_schema_all . "_grid.css\"/>\r\n";
              $SS_cod_html .= "    <link rel=\"stylesheet\" type=\"text/css\" href=\"../_lib/css/" . $this->str_schema_all . "_grid" . $_SESSION['scriptcase']['reg_conf']['css_dir'] . ".css\"/>\r\n";
              $SS_cod_html .= "  </HEAD>\r\n";
              $SS_cod_html .= "   <body class=\"scGridPage\">\r\n";
              $SS_cod_html .= "    <table align=\"center\"><tr><td style=\"padding: 0\"><div class=\"scGridBorder\">\r\n";
              $SS_cod_html .= "    <table class=\"scGridTabela\" width='100%' cellspacing=0 cellpadding=0><tr class=\"scGridFieldOdd\"><td class=\"scGridFieldOddFont\" style=\"padding: 15px 30px; text-align: center\">\r\n";
              $SS_cod_html .= $this->Nm_lang['lang_errm_expired_session'] . "\r\n";
              $SS_cod_html .= "     <form name=\"Fsession_redir\" method=\"post\"\r\n";
              $SS_cod_html .= "           target=\"_self\">\r\n";
              $SS_cod_html .= "           <input type=\"button\" name=\"sc_sai_seg\" value=\"OK\" onclick=\"sc_session_redir('" . $_SESSION['scriptcase']['module']['session_timeout']['redir'] . "');\">\r\n";
              $SS_cod_html .= "     </form>\r\n";
              $SS_cod_html .= "    </td></tr></table>\r\n";
              $SS_cod_html .= "    </div></td></tr></table>\r\n";
          }
          $SS_cod_html .= "    <script type=\"text/javascript\">\r\n";
          if ($_SESSION['scriptcase']['module']['session_timeout']['redir_tp'] == "R") {
              $SS_cod_html .= "      sc_session_redir('" . $_SESSION['scriptcase']['module']['session_timeout']['redir'] . "');\r\n";
          }
          $SS_cod_html .= "      function sc_session_redir(url_redir)\r\n";
          $SS_cod_html .= "      {\r\n";
          $SS_cod_html .= "         if (window.parent && window.parent.document != window.document && typeof window.parent.sc_session_redir === 'function')\r\n";
          $SS_cod_html .= "         {\r\n";
          $SS_cod_html .= "            window.parent.sc_session_redir(url_redir);\r\n";
          $SS_cod_html .= "         }\r\n";
          $SS_cod_html .= "         else\r\n";
          $SS_cod_html .= "         {\r\n";
          $SS_cod_html .= "             if (window.opener && typeof window.opener.sc_session_redir === 'function')\r\n";
          $SS_cod_html .= "             {\r\n";
          $SS_cod_html .= "                 window.close();\r\n";
          $SS_cod_html .= "                 window.opener.sc_session_redir(url_redir);\r\n";
          $SS_cod_html .= "             }\r\n";
          $SS_cod_html .= "             else\r\n";
          $SS_cod_html .= "             {\r\n";
          $SS_cod_html .= "                 window.location = url_redir;\r\n";
          $SS_cod_html .= "             }\r\n";
          $SS_cod_html .= "         }\r\n";
          $SS_cod_html .= "      }\r\n";
          $SS_cod_html .= "    </script>\r\n";
          $SS_cod_html .= " </body>\r\n";
          $SS_cod_html .= "</HTML>\r\n";
          unset($_SESSION['scriptcase']['module']['session_timeout']);
          unset($_SESSION['sc_session']);
      }
      if (isset($SS_cod_html) && isset($_GET['nmgp_opcao']) && (substr($_GET['nmgp_opcao'], 0, 14) == "ajax_aut_comp_" || substr($_GET['nmgp_opcao'], 0, 13) == "ajax_autocomp"))
      {
          unset($_SESSION['sc_session']);
          $oJson = new Services_JSON();
          echo $oJson->encode("ss_time_out");
          exit;
      }
      elseif (isset($SS_cod_html) && ((isset($_POST['nmgp_opcao']) && substr($_POST['nmgp_opcao'], 0, 5) == "ajax_") || (isset($_GET['nmgp_opcao']) && substr($_GET['nmgp_opcao'], 0, 5) == "ajax_")))
      {
          unset($_SESSION['sc_session']);
          $this->Arr_result = array();
          $this->Arr_result['ss_time_out'] = true;
          $oJson = new Services_JSON();
          echo $oJson->encode($this->Arr_result);
          exit;
      }
      elseif (isset($SS_cod_html))
      {
          echo $SS_cod_html;
          exit;
      }
      $this->nm_bases_access     = array("access", "ado_access");
      $this->nm_bases_db2        = array("db2", "db2_odbc", "odbc_db2", "odbc_db2v6", "pdo_db2_odbc", "pdo_ibm");
      $this->nm_bases_ibase      = array("ibase", "firebird", "pdo_firebird", "borland_ibase");
      $this->nm_bases_informix   = array("informix", "informix72", "pdo_informix");
      $this->nm_bases_mssql      = array("mssql", "ado_mssql", "odbc_mssql", "mssqlnative", "pdo_sqlsrv", "pdo_dblib");
      $this->nm_bases_mysql      = array("mysql", "mysqlt", "mysqli", "maxsql", "pdo_mysql");
      $this->nm_bases_postgres   = array("postgres", "postgres64", "postgres7", "pdo_pgsql");
      $this->nm_bases_oracle     = array("oci8", "oci805", "oci8po", "odbc_oracle", "oracle", "pdo_oracle");
      $this->sqlite_version      = "old";
      $this->nm_bases_sqlite     = array("sqlite", "sqlite3", "pdosqlite");
      $this->nm_bases_sybase     = array("sybase", "pdo_sybase_odbc", "pdo_sybase_dblib");
      $this->nm_bases_vfp        = array("vfp");
      $this->nm_bases_odbc       = array("odbc");
      $this->nm_bases_progress     = array("progress", "pdo_progress_odbc");
      $this->nm_bases_all        = array_merge($this->nm_bases_access, $this->nm_bases_db2, $this->nm_bases_ibase, $this->nm_bases_informix, $this->nm_bases_mssql, $this->nm_bases_mysql, $this->nm_bases_postgres, $this->nm_bases_oracle, $this->nm_bases_sqlite, $this->nm_bases_sybase, $this->nm_bases_vfp, $this->nm_bases_odbc, $this->nm_bases_progress);
      $this->nm_font_ttf = array("ar", "ja", "pl", "ru", "sk", "thai", "zh_cn", "zh_hk", "cz", "el", "ko", "mk");
      $this->nm_ttf_arab = array("ar");
      $this->nm_ttf_jap  = array("ja");
      $this->nm_ttf_rus  = array("pl", "ru", "sk", "cz", "el", "mk");
      $this->nm_ttf_thai = array("thai");
      $this->nm_ttf_chi  = array("zh_cn", "zh_hk", "ko");
      $_SESSION['sc_session'][$this->sc_page]['module']['seq_dir'] = 0; 
      $_SESSION['sc_session'][$this->sc_page]['module']['sub_dir'] = array(); 
      $_SESSION['scriptcase']['nm_bases_security']  = "enc_nm_enc_v1DcJeZ9rqZ1BYHuFUDMBODkBOHEX7HMF7DcBqVINUHABYV5JsDMBYHEXeDWr/HMX7HQXODQFUHAveD5NUHgvsV9FeDWXCDoJsDcBwH9B/Z1rYHQJwHgveHArCV5B7ZuJsDcXGDQBqDSzGVWJsDMvOVIB/DWXKDoXGHQBqZ1BOD1rKHQJeDMveHENiDWrGVoFGHQFYZ9XGD1NKV5BOHgrwZSJqH5XKDoXGDcFYZ1X7D1rKHuFGDMveHErsDWrGDoF7D9XsDQJsDSBYV5FGHgNKDkBsV5X/VEBiHQXOZ1FGHIveHuBOHgrKZSJ3DWFGVoFGHQNwH9FUHAvOV5BqDMvOVIBsDWBmDoXGHQNwZkFGHIBeHuJsHgBOHEJqDurmVoFGHQNwZ9F7DSBYHuJeDMBOV9FeHEFGVoBqD9BsZ1F7DSrYD5rqDMrYDkBsHEB3VoJsHQJwZ9JeHAveHuFGHgrKZSrCDWXCDoXGD9BiZ1B/HIrwZMB/DEBeHEJGDWF/VoX7DcBwDuBOHAveHuFGDMNOVcrsH5B7VoFaDcBqZ1FaHAN7V5FaDMrYHEBUH5FYVoJeD9XsZSFGHIrwD5NUDMrwVcFiDWXCVEFGD9BsH9FaD1zGD5raDMBYZSXeDWX7VoBiDcJUDQJwHABYV5BqHgrKV9FiDWXCDoraD9JmZ1FaD1rKV5XGDMzGHEJGDWrGDoNUD9XsZSX7HAvCV5BiHuBYVcFKDur/DoraD9BsZkFUHArKV5BqDEBOZSJGDWrGDoNUD9XsDQJsDSBYV5FGHgrKVcBOV5F/VoFGD9JmZ1B/Z1NOV5BOHgvCDkFeH5FYVoX7D9JKDQX7D1BOV5FGDMzGV9BUHEBmVoFGHQBsH9BOHANOHQXGHgveDkFeV5FqHIX7HQXODQFaDSN7HQJeDMvOZSrCV5FYHIFUHQNwH9BqDSBOZMB/HgBeDkFeV5FqHIrqDcBiDQBqD1veHuBqDMvsV9FiH5FqDoJeD9JmZ1B/D1NaD5rqHgrKHArsHEXCHMFaDcXGDuFaD1BeHuBODMBYZSrCV5X/VEF7DcNmZ1BOHANOD5JwHgNOHAFKV5FqHMJsDcBiH9BiZ1BYHQXGDMrYV9FiV5FYHIJeDcNmZ1BiHAvmZMBqHgNKHAFKH5FYVoX7D9JKDQX7D1BOV5FGDMBYVcBUHEF/HIraHQNwZSBOHINKD5JeHgNKDkFeV5FqHMFaHQJKZSFUDSN7HQrqHgvOZSrCV5FYHIraHQXOH9BOD1rKHuBqHgBYHEFKV5FqHMB/HQJeDQB/DSzGVWBODMrYVcFiH5FqDoJeD9JmZ1B/D1NaD5rqDErKZSXeH5FYDoFUD9NwDQJsHArYVWJsHuvmVcXKV5FGVoraD9BsH9FaHArKZMB/HgvCZSJ3V5XCVoB/D9NmDQFaZ1BYV5FUHuvmDkBOH5XKVoraD9BiVINUDSvOD5FaDEvsZSJGDuFaZuBqD9NwH9X7Z1rwV5JwHuBYVcFiV5X7VoFGDcBqH9FaHAN7V5JeDErKHEBUH5F/DoF7DcJeDQFGD1BeD5JwDMrwZSJ3H5FqDoJeD9JmZ1B/D1NaD5rqDErKZSXeH5FYDoFUD9JKDQJsZ1rwV5BqHuBYVcXKV5X7DoFGD9BsZSB/HIveD5FaDEvsHEJGH5FYVoB/HQBiDQBqHAN7HuFaHuNOZSrCH5FqDoXGHQJmZ1BiHArKHuJwDENOHErsDWF/HIBODcXOZSX7HAvOV5XGDMrYVIBsHEFYHMBiD9BsVIraD1rwV5X7HgBeHEFKV5FaVoFGD9XsZSX7D1veVWJwHuNOVcFKH5XKDoJsD9XOZ1F7HIveD5BqHgBeHEFiV5B3DoF7D9XsDuFaHAveHuFaHuNOZSrCH5FqDoXGHQJmZ1F7HIBOD5JeDErKVkJ3V5FaDoJsHQFYDQFGHAN7HurqDMvsVcBUDWJeVEraD9JmZSB/Z1BOZMB/DMBYZSXeDWFqHIJsD9XsZ9JeD1BeD5F7DMvmVcFKDWFYVorqDcNwH9B/HAN7D5XGDEBOZSXeV5XCZuJsDcBwDuFaHAveD5NUHgNKDkBOV5FYHMBiHQNmVINUHAvsD5BOHgBYHArsDWBmDoJeHQBiZ9XGHIrKHuFaHuNOZSrCH5FqDoXGHQJmZ1F7HIBOZMBOHgrKHEJqDWX7HIBqHQFYZSBiZ1N7HQrqDMBOVcXKDWB3VoF7HQJmZ1F7Z1vmD5rqDEBOHArCDWF/HIFUD9NwH9X7DSBYV5FGHuvmZSJ3DWXCHIXGD9JmZ1FaD1rKD5FaDErKVkJGDWFqDoXGHQJKDQJsZ1vCV5FGHuNOV9FeDWB3VEFGHQFYVINUHAvsZMNU";
      $this->prep_conect();
      $this->conectDB();
      if (!in_array(strtolower($this->nm_tpbanco), $this->nm_bases_all))
      {
          echo "<tr>";
          echo "   <td bgcolor=\"\">";
          echo "       <b><font size=\"4\">" . $this->Nm_lang['lang_errm_dbcn_nspt'] . "</font>";
          echo "  " . $perfil_trab;
          echo "   </b></td>";
          echo " </tr>";
          echo "</table>";
          if (!$_SESSION['sc_session'][$script_case_init]['module']['iframe_menu'] && (!isset($_SESSION['sc_session'][$script_case_init]['module']['sc_outra_jan']) || !$_SESSION['sc_session'][$script_case_init]['module']['sc_outra_jan'])) 
          { 
              if (isset($_SESSION['scriptcase']['nm_sc_retorno']) && !empty($_SESSION['scriptcase']['nm_sc_retorno'])) 
              { 
                  echo "<a href='" . $_SESSION['scriptcase']['nm_sc_retorno'] . "' target='_self'><img border='0' src='" . $this->path_botoes . "/nm_Scriptcase7_SoftGray_bvoltar.gif' title='" . $this->Nm_lang['lang_btns_rtrn_scrp_hint'] . "' align=absmiddle></a> \n" ; 
              } 
              else 
              { 
                  echo "<a href='$nm_url_saida' target='_self'><img border='0' src='" . $this->path_botoes . "/nm_Scriptcase7_SoftGray_bsair.gif' title='" . $this->Nm_lang['lang_btns_exit_appl_hint'] . "' align=absmiddle></a> \n" ; 
              } 
          } 
          exit ;
      } 
      if (empty($this->nm_tabela))
      {
          $this->nm_tabela = ""; 
      }
   }

   function getRunningOS()
   {
       $aOSInfo = array();

       if (FALSE !== strpos(strtolower(php_uname()), 'windows')) 
       {
           $aOSInfo['os'] = 'win';
       }
       elseif (FALSE !== strpos(strtolower(php_uname()), 'linux')) 
       {
           $aOSInfo['os'] = 'linux-i386';
           if(strpos(strtolower(php_uname()), 'x86_64') !== FALSE) 
            {
               $aOSInfo['os'] = 'linux-amd64';
            }
       }
       elseif (FALSE !== strpos(strtolower(php_uname()), 'darwin'))
       {
           $aOSInfo['os'] = 'macos';
       }

       return $aOSInfo;
   }

   function prep_conect()
   {
      if (isset($_SESSION['scriptcase']['sc_connection']) && !empty($_SESSION['scriptcase']['sc_connection']))
      {
          foreach ($_SESSION['scriptcase']['sc_connection'] as $NM_con_orig => $NM_con_dest)
          {
              if (isset($_SESSION['scriptcase']['module']['glo_nm_conexao']) && $_SESSION['scriptcase']['module']['glo_nm_conexao'] == $NM_con_orig)
              {
/*NM*/            $_SESSION['scriptcase']['module']['glo_nm_conexao'] = $NM_con_dest;
              }
              if (isset($_SESSION['scriptcase']['module']['glo_nm_perfil']) && $_SESSION['scriptcase']['module']['glo_nm_perfil'] == $NM_con_orig)
              {
/*NM*/            $_SESSION['scriptcase']['module']['glo_nm_perfil'] = $NM_con_dest;
              }
              if (isset($_SESSION['scriptcase']['module']['glo_con_' . $NM_con_orig]))
              {
                  $_SESSION['scriptcase']['module']['glo_con_' . $NM_con_orig] = $NM_con_dest;
              }
          }
      }
      $con_devel             = (isset($_SESSION['scriptcase']['module']['glo_nm_conexao'])) ? $_SESSION['scriptcase']['module']['glo_nm_conexao'] : ""; 
      $perfil_trab           = ""; 
      $this->nm_falta_var    = ""; 
      $this->nm_falta_var_db = ""; 
      $nm_crit_perfil        = false;
      if (isset($_SESSION['scriptcase']['module']['glo_nm_conexao']) && !empty($_SESSION['scriptcase']['module']['glo_nm_conexao']))
      {
          ob_start();
          db_conect_devel($con_devel, $this->root . $this->path_prod, 'Service_Call_Addict', 2); 
          if (!isset($this->Ajax_result_set)) {$this->Ajax_result_set = ob_get_contents();}
          ob_end_clean();
          if (empty($_SESSION['scriptcase']['glo_tpbanco']) && empty($_SESSION['scriptcase']['glo_banco']))
          {
              $nm_crit_perfil = true;
          }
      }
      if (isset($_SESSION['scriptcase']['module']['glo_nm_perfil']) && !empty($_SESSION['scriptcase']['module']['glo_nm_perfil']))
      {
          $perfil_trab = $_SESSION['scriptcase']['module']['glo_nm_perfil'];
      }
      elseif (isset($_SESSION['scriptcase']['glo_perfil']) && !empty($_SESSION['scriptcase']['glo_perfil']))
      {
          $perfil_trab = $_SESSION['scriptcase']['glo_perfil'];
      }
      if (!empty($perfil_trab))
      {
          $_SESSION['scriptcase']['glo_senha_protect'] = "";
          carrega_perfil($perfil_trab, $this->path_libs, "S", $this->path_conf);
          if (empty($_SESSION['scriptcase']['glo_senha_protect']))
          {
              $nm_crit_perfil = true;
          }
      }
      else
      {
          $perfil_trab = $con_devel;
      }
      if (!isset($_SESSION['sc_session'][$this->sc_page]['module']['embutida_init']) || !$_SESSION['sc_session'][$this->sc_page]['module']['embutida_init']) 
      {
      }
// 
      if (!isset($_SESSION['scriptcase']['glo_tpbanco']))
      {
          if (!$nm_crit_perfil)
          {
              $this->nm_falta_var_db .= "glo_tpbanco; ";
          }
      }
      else
      {
          $this->nm_tpbanco = $_SESSION['scriptcase']['glo_tpbanco']; 
      }
      if (!isset($_SESSION['scriptcase']['glo_servidor']))
      {
          if (!$nm_crit_perfil)
          {
              $this->nm_falta_var_db .= "glo_servidor; ";
          }
      }
      else
      {
          $this->nm_servidor = $_SESSION['scriptcase']['glo_servidor']; 
      }
      if (!isset($_SESSION['scriptcase']['glo_banco']))
      {
          if (!$nm_crit_perfil)
          {
              $this->nm_falta_var_db .= "glo_banco; ";
          }
      }
      else
      {
          $this->nm_banco = $_SESSION['scriptcase']['glo_banco']; 
      }
      if (!isset($_SESSION['scriptcase']['glo_usuario']))
      {
          if (!$nm_crit_perfil)
          {
              $this->nm_falta_var_db .= "glo_usuario; ";
          }
      }
      else
      {
          $this->nm_usuario = $_SESSION['scriptcase']['glo_usuario']; 
      }
      if (!isset($_SESSION['scriptcase']['glo_senha']))
      {
          if (!$nm_crit_perfil)
          {
              $this->nm_falta_var_db .= "glo_senha; ";
          }
      }
      else
      {
          $this->nm_senha = $_SESSION['scriptcase']['glo_senha']; 
      }
      if (isset($_SESSION['scriptcase']['glo_database_encoding']))
      {
          $this->nm_database_encoding = $_SESSION['scriptcase']['glo_database_encoding']; 
      }
      $this->nm_arr_db_extra_args = array(); 
      if (isset($_SESSION['scriptcase']['glo_use_ssl']))
      {
          $this->nm_arr_db_extra_args['use_ssl'] = $_SESSION['scriptcase']['glo_use_ssl']; 
      }
      if (isset($_SESSION['scriptcase']['glo_mysql_ssl_key']))
      {
          $this->nm_arr_db_extra_args['mysql_ssl_key'] = $_SESSION['scriptcase']['glo_mysql_ssl_key']; 
      }
      if (isset($_SESSION['scriptcase']['glo_mysql_ssl_cert']))
      {
          $this->nm_arr_db_extra_args['mysql_ssl_cert'] = $_SESSION['scriptcase']['glo_mysql_ssl_cert']; 
      }
      if (isset($_SESSION['scriptcase']['glo_mysql_ssl_capath']))
      {
          $this->nm_arr_db_extra_args['mysql_ssl_capath'] = $_SESSION['scriptcase']['glo_mysql_ssl_capath']; 
      }
      if (isset($_SESSION['scriptcase']['glo_mysql_ssl_ca']))
      {
          $this->nm_arr_db_extra_args['mysql_ssl_ca'] = $_SESSION['scriptcase']['glo_mysql_ssl_ca']; 
      }
      if (isset($_SESSION['scriptcase']['glo_mysql_ssl_cipher']))
      {
          $this->nm_arr_db_extra_args['mysql_ssl_cipher'] = $_SESSION['scriptcase']['glo_mysql_ssl_cipher']; 
      }
      if (isset($_SESSION['scriptcase']['glo_db2_autocommit']))
      {
          $this->nm_con_db2['db2_autocommit'] = $_SESSION['scriptcase']['glo_db2_autocommit']; 
      }
      if (isset($_SESSION['scriptcase']['glo_db2_i5_lib']))
      {
          $this->nm_con_db2['db2_i5_lib'] = $_SESSION['scriptcase']['glo_db2_i5_lib']; 
      }
      if (isset($_SESSION['scriptcase']['glo_db2_i5_naming']))
      {
          $this->nm_con_db2['db2_i5_naming'] = $_SESSION['scriptcase']['glo_db2_i5_naming']; 
      }
      if (isset($_SESSION['scriptcase']['glo_db2_i5_commit']))
      {
          $this->nm_con_db2['db2_i5_commit'] = $_SESSION['scriptcase']['glo_db2_i5_commit']; 
      }
      if (isset($_SESSION['scriptcase']['glo_db2_i5_query_optimize']))
      {
          $this->nm_con_db2['db2_i5_query_optimize'] = $_SESSION['scriptcase']['glo_db2_i5_query_optimize']; 
      }
      if (isset($_SESSION['scriptcase']['glo_use_persistent']))
      {
          $this->nm_con_persistente = $_SESSION['scriptcase']['glo_use_persistent']; 
      }
      if (isset($_SESSION['scriptcase']['glo_use_schema']))
      {
          $this->nm_con_use_schema = $_SESSION['scriptcase']['glo_use_schema']; 
      }
      $this->date_delim  = "'";
      $this->date_delim1 = "'";
      if (in_array(strtolower($this->nm_tpbanco), $this->nm_bases_sybase))
      {
          $this->date_delim  = "";
          $this->date_delim1 = "";
      }
      if (in_array(strtolower($this->nm_tpbanco), $this->nm_bases_access))
      {
          $this->date_delim  = "#";
          $this->date_delim1 = "#";
      }
      if (isset($_SESSION['scriptcase']['glo_decimal_db']) && !empty($_SESSION['scriptcase']['glo_decimal_db']))
      {
          $_SESSION['sc_session'][$this->sc_page]['module']['decimal_db'] = $_SESSION['scriptcase']['glo_decimal_db']; 
      }
      if (isset($_SESSION['scriptcase']['glo_date_separator']) && !empty($_SESSION['scriptcase']['glo_date_separator']))
      {
          $SC_temp = trim($_SESSION['scriptcase']['glo_date_separator']);
          if (strlen($SC_temp) == 2)
          {
              $_SESSION['sc_session'][$this->sc_page]['module']['SC_sep_date']  = substr($SC_temp, 0, 1); 
              $_SESSION['sc_session'][$this->sc_page]['module']['SC_sep_date1'] = substr($SC_temp, 1, 1); 
          }
          else
           {
              $_SESSION['sc_session'][$this->sc_page]['module']['SC_sep_date']  = $SC_temp; 
              $_SESSION['sc_session'][$this->sc_page]['module']['SC_sep_date1'] = $SC_temp; 
          }
          $this->date_delim  = $_SESSION['sc_session'][$this->sc_page]['module']['SC_sep_date'];
          $this->date_delim1 = $_SESSION['sc_session'][$this->sc_page]['module']['SC_sep_date1'];
      }
// 
      if (!empty($this->nm_falta_var) || !empty($this->nm_falta_var_db) || $nm_crit_perfil)
      {
          echo "<style type=\"text/css\">";
          echo ".scButton_default { font-family: Arial, sans-serif; font-size: 11px; color: #FFFFFF; font-weight: bold; border-style: none; border-width: 1px; padding: 3px 14px; background-image: url(../../img/scriptcase__NM__bgButtonGreen.jpg); }";
          echo ".scButton_disabled { font-family: Arial, sans-serif; font-size: 11px; color: #666666; font-weight: bold; border-style: none; border-width: 1px; padding: 3px 14px; background-image: url(../../img/scriptcase__NM__btn_cromo_off.png); }";
          echo ".scButton_onmousedown { font-family: Arial, sans-serif; font-size: 11px; color: #666666; font-weight: bold; border-style: none; border-width: 1px; padding: 3px 14px; background-image: url(../../img/scriptcase__NM__V7Softgraybgcalendar.png); }";
          echo ".scButton_onmouseover { font-family: Arial, sans-serif; font-size: 11px; color: #f3f3f3; font-weight: bold; background-color: #666666; border-style: none; border-width: 1px; padding: 3px 14px; background-image: url(../../img/scriptcase__NM__btn_cromo_off.png); }";
          echo ".scButton_small { font-family: Tahoma, Arial, sans-serif; font-size: 8px; color: #000000; font-weight: normal; background-color: #EEEEEE; border-style: solid; border-width: 1px; padding: 4px 8px;  }";
          echo ".scLink_default { text-decoration: underline; font-size: 12px; color: #0000AA;  }";
          echo ".scLink_default:visited { text-decoration: underline; font-size: 12px; color: #0000AA;  }";
          echo ".scLink_default:active { text-decoration: underline; font-size: 12px; color: #0000AA;  }";
          echo ".scLink_default:hover { text-decoration: none; font-size: 12px; color: #0000AA;  }";
          echo "</style>";
          echo "<table width=\"80%\" border=\"1\" height=\"117\">";
          if (empty($this->nm_falta_var_db))
          {
              if (!empty($this->nm_falta_var))
              {
                  echo "<tr>";
                  echo "   <td bgcolor=\"\">";
                  echo "       <b><font size=\"4\">" . $this->Nm_lang['lang_errm_glob'] . "</font>";
                  echo "  " . $this->nm_falta_var;
                  echo "   </b></td>";
                  echo " </tr>";
              }
              if ($nm_crit_perfil)
              {
                  echo "<tr>";
                  echo "   <td bgcolor=\"\">";
                  echo "       <b><font size=\"4\">" . $this->Nm_lang['lang_errm_dbcn_nfnd'] . "</font>";
                  echo "  " . $perfil_trab;
                  echo "   </b></td>";
                  echo " </tr>";
              }
          }
          else
          {
              echo "<tr>";
              echo "   <td bgcolor=\"\">";
              echo "       <b><font size=\"4\">" . $this->Nm_lang['lang_errm_dbcn_data'] . "</font></b>";
              echo "   </td>";
              echo " </tr>";
          }
          echo "</table>";
          if (!$_SESSION['sc_session'][$script_case_init]['module']['iframe_menu'] && (!isset($_SESSION['sc_session'][$script_case_init]['module']['sc_outra_jan']) || !$_SESSION['sc_session'][$script_case_init]['module']['sc_outra_jan'])) 
          { 
              if (isset($_SESSION['scriptcase']['nm_sc_retorno']) && !empty($_SESSION['scriptcase']['nm_sc_retorno'])) 
              { 
               $btn_value = "" . $this->Ini->Nm_lang['lang_btns_back'] . "";
               if ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($btn_value))
               {
                   $btn_value = sc_convert_encoding($btn_value, $_SESSION['scriptcase']['charset'], "UTF-8");
               }
               $btn_hint = "" . $this->Ini->Nm_lang['lang_btns_back_hint'] . "";
               if ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($btn_hint))
               {
                   $btn_hint = sc_convert_encoding($btn_hint, $_SESSION['scriptcase']['charset'], "UTF-8");
               }
?>
                   <input type="button" id="sai" onClick="window.location='<?php echo $_SESSION['scriptcase']['nm_sc_retorno'] ?>'; return false" class="scButton_default" value="<?php echo $btn_value ?>" title="<?php echo $btn_hint ?>" style="vertical-align: middle;">

<?php
              } 
              else 
              { 
               $btn_value = "" . $this->Ini->Nm_lang['lang_btns_exit'] . "";
               if ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($btn_value))
               {
                   $btn_value = sc_convert_encoding($btn_value, $_SESSION['scriptcase']['charset'], "UTF-8");
               }
               $btn_hint = "" . $this->Ini->Nm_lang['lang_btns_exit_hint'] . "";
               if ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($btn_hint))
               {
                   $btn_hint = sc_convert_encoding($btn_hint, $_SESSION['scriptcase']['charset'], "UTF-8");
               }
?>
                   <input type="button" id="sai" onClick="window.location='<?php echo $nm_url_saida ?>'; return false" class="scButton_default" value="<?php echo $btn_value ?>" title="<?php echo $btn_hint ?>" style="vertical-align: middle;">

<?php
              } 
          } 
          exit ;
      }
      if (isset($_SESSION['scriptcase']['glo_db_master_usr']) && !empty($_SESSION['scriptcase']['glo_db_master_usr']))
      {
          $this->nm_usuario = $_SESSION['scriptcase']['glo_db_master_usr']; 
      }
      if (isset($_SESSION['scriptcase']['glo_db_master_pass']) && !empty($_SESSION['scriptcase']['glo_db_master_pass']))
      {
          $this->nm_senha = $_SESSION['scriptcase']['glo_db_master_pass']; 
      }
      if (isset($_SESSION['scriptcase']['glo_db_master_cript']) && !empty($_SESSION['scriptcase']['glo_db_master_cript']))
      {
          $_SESSION['scriptcase']['glo_senha_protect'] = $_SESSION['scriptcase']['glo_db_master_cript']; 
      }
   }
   function conectDB()
   {
      global $glo_senha_protect;
      $glo_senha_protect = (isset($_SESSION['scriptcase']['glo_senha_protect'])) ? $_SESSION['scriptcase']['glo_senha_protect'] : "S";
      if (isset($_SESSION['scriptcase']['nm_sc_retorno']) && !empty($_SESSION['scriptcase']['nm_sc_retorno']) && isset($_SESSION['scriptcase']['module']['glo_nm_conexao']) && !empty($_SESSION['scriptcase']['module']['glo_nm_conexao']))
      { 
          $this->Db = db_conect_devel($_SESSION['scriptcase']['module']['glo_nm_conexao'], $this->root . $this->path_prod, 'Service_Call_Addict'); 
      } 
      else 
      { 
          ob_start();
          $this->Db = db_conect($this->nm_tpbanco, $this->nm_servidor, $this->nm_usuario, $this->nm_senha, $this->nm_banco, $glo_senha_protect, "S", $this->nm_con_persistente, $this->nm_con_db2, $this->nm_database_encoding, $this->nm_arr_db_extra_args); 
          if (!isset($this->Ajax_result_set)) {$this->Ajax_result_set = ob_get_contents();}
          ob_end_clean();
      } 
      if (!$_SESSION['sc_session'][$this->sc_page]['module']['embutida'])
      {
          if (substr($_POST['nmgp_opcao'], 0, 5) == "ajax_")
          {
              ob_start();
          } 
      } 
      if (in_array(strtolower($this->nm_tpbanco), $this->nm_bases_ibase))
      {
          if (function_exists('ibase_timefmt'))
          {
              ibase_timefmt('%Y-%m-%d %H:%M:%S');
          } 
          $GLOBALS["NM_ERRO_IBASE"] = 1;  
          $this->Ibase_version = "old";
          if ($ibase_version = $this->Db->Execute("SELECT RDB\$GET_CONTEXT('SYSTEM','ENGINE_VERSION') AS \"Version\" FROM RDB\$DATABASE"))
          {
              if (isset($ibase_version->fields[0]) && substr($ibase_version->fields[0], 0, 1) > 2) {$this->Ibase_version = "new";}
          }
      } 
      if (in_array(strtolower($this->nm_tpbanco), $this->nm_bases_sybase))
      {
          $this->Db->fetchMode = ADODB_FETCH_BOTH;
          $this->Db->Execute("set dateformat ymd");
          $this->Db->Execute("set quoted_identifier ON");
      } 
      if (in_array(strtolower($this->nm_tpbanco), $this->nm_bases_db2))
      {
          $this->Db->fetchMode = ADODB_FETCH_NUM;
      } 
      if (in_array(strtolower($this->nm_tpbanco), $this->nm_bases_mssql))
      {
          $this->Db->Execute("set dateformat ymd");
      } 
      if (in_array(strtolower($this->nm_tpbanco), $this->nm_bases_oracle))
      {
          $this->Db->Execute("alter session set nls_date_format         = 'yyyy-mm-dd hh24:mi:ss'");
          $this->Db->Execute("alter session set nls_timestamp_format    = 'yyyy-mm-dd hh24:mi:ss'");
          $this->Db->Execute("alter session set nls_timestamp_tz_format = 'yyyy-mm-dd hh24:mi:ss'");
          $this->Db->Execute("alter session set nls_time_format         = 'hh24:mi:ss'");
          $this->Db->Execute("alter session set nls_time_tz_format      = 'hh24:mi:ss'");
          $this->Db->Execute("alter session set nls_numeric_characters  = '.,'");
          $_SESSION['sc_session'][$this->sc_page]['module']['decimal_db'] = "."; 
      } 
      if (in_array(strtolower($this->nm_tpbanco), $this->nm_bases_postgres))
      {
          $this->Db->Execute("SET DATESTYLE TO ISO");
      } 
   }
   function regionalDefault()
   {
       $_SESSION['scriptcase']['reg_conf']['date_format']   = (isset($this->Nm_conf_reg[$this->str_conf_reg]['data_format']))              ?  $this->Nm_conf_reg[$this->str_conf_reg]['data_format'] : "ddmmyyyy";
       $_SESSION['scriptcase']['reg_conf']['date_sep']      = (isset($this->Nm_conf_reg[$this->str_conf_reg]['data_sep']))                 ?  $this->Nm_conf_reg[$this->str_conf_reg]['data_sep'] : "/";
       $_SESSION['scriptcase']['reg_conf']['date_week_ini'] = (isset($this->Nm_conf_reg[$this->str_conf_reg]['prim_dia_sema']))            ?  $this->Nm_conf_reg[$this->str_conf_reg]['prim_dia_sema'] : "SU";
       $_SESSION['scriptcase']['reg_conf']['time_format']   = (isset($this->Nm_conf_reg[$this->str_conf_reg]['hora_format']))              ?  $this->Nm_conf_reg[$this->str_conf_reg]['hora_format'] : "hhiiss";
       $_SESSION['scriptcase']['reg_conf']['time_sep']      = (isset($this->Nm_conf_reg[$this->str_conf_reg]['hora_sep']))                 ?  $this->Nm_conf_reg[$this->str_conf_reg]['hora_sep'] : ":";
       $_SESSION['scriptcase']['reg_conf']['time_pos_ampm'] = (isset($this->Nm_conf_reg[$this->str_conf_reg]['hora_pos_ampm']))            ?  $this->Nm_conf_reg[$this->str_conf_reg]['hora_pos_ampm'] : "right_without_space";
       $_SESSION['scriptcase']['reg_conf']['time_simb_am']  = (isset($this->Nm_conf_reg[$this->str_conf_reg]['hora_simbolo_am']))          ?  $this->Nm_conf_reg[$this->str_conf_reg]['hora_simbolo_am'] : "am";
       $_SESSION['scriptcase']['reg_conf']['time_simb_pm']  = (isset($this->Nm_conf_reg[$this->str_conf_reg]['hora_simbolo_pm']))          ?  $this->Nm_conf_reg[$this->str_conf_reg]['hora_simbolo_pm'] : "pm";
       $_SESSION['scriptcase']['reg_conf']['simb_neg']      = (isset($this->Nm_conf_reg[$this->str_conf_reg]['num_sinal_neg']))            ?  $this->Nm_conf_reg[$this->str_conf_reg]['num_sinal_neg'] : "-";
       $_SESSION['scriptcase']['reg_conf']['grup_num']      = (isset($this->Nm_conf_reg[$this->str_conf_reg]['num_sep_agr']))              ?  $this->Nm_conf_reg[$this->str_conf_reg]['num_sep_agr'] : ".";
       $_SESSION['scriptcase']['reg_conf']['dec_num']       = (isset($this->Nm_conf_reg[$this->str_conf_reg]['num_sep_dec']))              ?  $this->Nm_conf_reg[$this->str_conf_reg]['num_sep_dec'] : ",";
       $_SESSION['scriptcase']['reg_conf']['neg_num']       = (isset($this->Nm_conf_reg[$this->str_conf_reg]['num_format_num_neg']))       ?  $this->Nm_conf_reg[$this->str_conf_reg]['num_format_num_neg'] : 2;
       $_SESSION['scriptcase']['reg_conf']['monet_simb']    = (isset($this->Nm_conf_reg[$this->str_conf_reg]['unid_mont_simbolo']))        ?  $this->Nm_conf_reg[$this->str_conf_reg]['unid_mont_simbolo'] : "$";
       $_SESSION['scriptcase']['reg_conf']['monet_f_pos']   = (isset($this->Nm_conf_reg[$this->str_conf_reg]['unid_mont_format_num_pos'])) ?  $this->Nm_conf_reg[$this->str_conf_reg]['unid_mont_format_num_pos'] : 3;
       $_SESSION['scriptcase']['reg_conf']['monet_f_neg']   = (isset($this->Nm_conf_reg[$this->str_conf_reg]['unid_mont_format_num_neg'])) ?  $this->Nm_conf_reg[$this->str_conf_reg]['unid_mont_format_num_neg'] : 13;
       $_SESSION['scriptcase']['reg_conf']['grup_val']      = (isset($this->Nm_conf_reg[$this->str_conf_reg]['unid_mont_sep_agr']))        ?  $this->Nm_conf_reg[$this->str_conf_reg]['unid_mont_sep_agr'] : ".";
       $_SESSION['scriptcase']['reg_conf']['dec_val']       = (isset($this->Nm_conf_reg[$this->str_conf_reg]['unid_mont_sep_dec']))        ?  $this->Nm_conf_reg[$this->str_conf_reg]['unid_mont_sep_dec'] : ",";
       $_SESSION['scriptcase']['reg_conf']['html_dir']      = (isset($this->Nm_conf_reg[$this->str_conf_reg]['ger_ltr_rtl']))              ?  " DIR='" . $this->Nm_conf_reg[$this->str_conf_reg]['ger_ltr_rtl'] . "'" : "";
       $_SESSION['scriptcase']['reg_conf']['css_dir']       = (isset($this->Nm_conf_reg[$this->str_conf_reg]['ger_ltr_rtl']))              ?  $this->Nm_conf_reg[$this->str_conf_reg]['ger_ltr_rtl'] : "LTR";
       $_SESSION['scriptcase']['reg_conf']['num_group_digit']       = (isset($this->Nm_conf_reg[$this->str_conf_reg]['num_group_digit']))       ?  $this->Nm_conf_reg[$this->str_conf_reg]['num_group_digit'] : "1";
       $_SESSION['scriptcase']['reg_conf']['unid_mont_group_digit'] = (isset($this->Nm_conf_reg[$this->str_conf_reg]['unid_mont_group_digit'])) ?  $this->Nm_conf_reg[$this->str_conf_reg]['unid_mont_group_digit'] : "1";
   }
// 
   function sc_Include($path, $tp, $name)
   {
       if ((empty($tp) && empty($name)) || ($tp == "F" && !function_exists($name)) || ($tp == "C" && !class_exists($name)))
       {
           include_once($path);
       }
   } // sc_Include
   function sc_Sql_Protect($var, $tp, $conex="")
   {
       if (empty($conex) || $conex == "conn_mysql")
       {
           $TP_banco = $_SESSION['scriptcase']['glo_tpbanco'];
       }
       else
       {
           eval ("\$TP_banco = \$this->nm_con_" . $conex . "['tpbanco'];");
       }
       if ($tp == "date")
       {
           $delim  = "'";
           $delim1 = "'";
           if (in_array(strtolower($TP_banco), $this->nm_bases_access))
           {
               $delim  = "#";
               $delim1 = "#";
           }
           if (isset($_SESSION['sc_session'][$this->sc_page]['module']['SC_sep_date']) && !empty($_SESSION['sc_session'][$this->sc_page]['module']['SC_sep_date']))
           {
               $delim  = $_SESSION['sc_session'][$this->sc_page]['module']['SC_sep_date'];
               $delim1 = $_SESSION['sc_session'][$this->sc_page]['module']['SC_sep_date1'];
           }
           return $delim . $var . $delim1;
       }
       else
       {
           return $var;
       }
   } // sc_Sql_Protect
   function sc_Date_Protect($val_dt)
   {
       $dd = substr($val_dt, 8, 2);
       $mm = substr($val_dt, 5, 2);
       $yy = substr($val_dt, 0, 4);
       $hh = (strlen($val_dt) > 10) ? substr($val_dt, 10) : "";
       if ($mm > 12) {
           $mm = 12;
       }
       $dd_max = 31;
       if ($mm == '04' || $mm == '06' || $mm == '09' || $mm == 11) {
           $dd_max = 30;
       }
       if ($mm == '02') {
           $dd_max = ($yy % 4 == 0) ? 29 : 28;
       }
       if ($dd > $dd_max) {
           $dd = $dd_max;
       }
       return $yy . "-" . $mm . "-" . $dd . $hh;
   }
	function appIsSsl() {
		if (isset($_SERVER['HTTPS'])) {
			if ('on' == strtolower($_SERVER['HTTPS'])) {
				return true;
			}
			if ('1' == $_SERVER['HTTPS']) {
				return true;
			}
		}

		if (isset($_SERVER['REQUEST_SCHEME'])) {
			if ('https' == $_SERVER['REQUEST_SCHEME']) {
				return true;
			}
		}

		if (isset($_SERVER['SERVER_PORT'])) {
			if ('443' == $_SERVER['SERVER_PORT']) {
				return true;
			}
		}

		return false;
	}
   function Get_Gb_date_format($GB, $cmp)
   {
       return (isset($_SESSION['sc_session'][$this->sc_page]['module']['SC_Gb_date_format'][$GB][$cmp])) ? $_SESSION['sc_session'][$this->sc_page]['module']['SC_Gb_date_format'][$GB][$cmp] : "";
   }

   function Get_Gb_prefix_date_format($GB, $cmp)
   {
       return (isset($_SESSION['sc_session'][$this->sc_page]['module']['SC_Gb_prefix_date_format'][$GB][$cmp])) ? $_SESSION['sc_session'][$this->sc_page]['module']['SC_Gb_prefix_date_format'][$GB][$cmp] : "";
   }

   function GB_date_format($val, $format, $prefix, $conf_region="S", $mask="")
   {
           return $val;
   }
   function Get_arg_groupby($val, $format)
   {
       return $val; 
   }
   function Get_format_dimension($ind_ini, $ind_qb, $campo, $rs, $conf_region="S", $mask="")
   {
       $retorno    = array();
       $format     = $this->Get_Gb_date_format($ind_qb, $campo);
       $Prefix_dat = $this->Get_Gb_prefix_date_format($ind_qb, $campo);
       if (empty($format) || $rs->fields[$ind_ini] == "")
       {
           $retorno['orig'] = $rs->fields[$ind_ini];
           $retorno['fmt']  = $rs->fields[$ind_ini];
           return $retorno;
       }
       if ($format == 'YYYYMMDDHHIISS')
       {
           $retorno['orig'] = $rs->fields[$ind_ini];
           $retorno['fmt']  = $this->GB_date_format($rs->fields[$ind_ini], $format, $Prefix_dat, $conf_region, $mask);
           return $retorno;
       }
       if ($format == 'YYYYMMDDHHII')
       {
           $this->Ajust_fields($ind_ini, $rs, "1,2,3,4");
           $temp            = $rs->fields[$ind_ini] . "-" . $rs->fields[$ind_ini + 1] . "-" . $rs->fields[$ind_ini + 2] . " " . $rs->fields[$ind_ini + 3] . ":" . $rs->fields[$ind_ini + 4];
           $retorno['orig'] = $temp;
           $retorno['fmt']  = $this->GB_date_format($temp, $format, $Prefix_dat, $conf_region, $mask);
           return $retorno;
       }
       if ($format == 'YYYYMMDDHH')
       {
           $this->Ajust_fields($ind_ini, $rs, "1,2,3");
           $temp            = $rs->fields[$ind_ini] . "-" . $rs->fields[$ind_ini + 1] . "-" . $rs->fields[$ind_ini + 2] . " " . $rs->fields[$ind_ini + 3];
           $retorno['orig'] = $temp;
           $retorno['fmt']  = $this->GB_date_format($temp, $format, $Prefix_dat, $conf_region, $mask);
           return $retorno;
       }
       if ($format == 'YYYYMMDD2')
       {
           $this->Ajust_fields($ind_ini, $rs, "1,2");
           $temp            = $rs->fields[$ind_ini] . "-" . $rs->fields[$ind_ini + 1] . "-" . $rs->fields[$ind_ini + 2];
           $retorno['orig'] = $temp;
           $retorno['fmt']  = $this->GB_date_format($temp, $format, $Prefix_dat, $conf_region, $mask);
           return $retorno;
       }
       if ($format == 'YYYYMM')
       {
           $this->Ajust_fields($ind_ini, $rs, "1");
           $temp            = $rs->fields[$ind_ini] . "-" . $rs->fields[$ind_ini + 1];
           $retorno['orig'] = $temp;
           $retorno['fmt']  = $this->GB_date_format($temp, $format, $Prefix_dat, $conf_region, $mask);
           return $retorno;
       }
       if ($format == 'YYYY')
       {
           $retorno['orig'] = $rs->fields[$ind_ini];
           $retorno['fmt']  = $this->GB_date_format($rs->fields[$ind_ini], $format, $Prefix_dat, $conf_region, $mask);
           return $retorno;
       }
       if ($format == 'BIMONTHLY' || $format == 'QUARTER' || $format == 'FOURMONTHS' || $format == 'SEMIANNUAL' || $format == 'WEEK')
       {
           $temp            = (substr($rs->fields[$ind_ini], 0, 1) == 0) ? substr($rs->fields[$ind_ini], 1) : $rs->fields[$ind_ini];
           $retorno['orig'] = $rs->fields[$ind_ini];
           $retorno['fmt']  = $Prefix_dat . $temp;
           return $retorno;
       }
       if ($format == 'DAYNAME'|| $format == 'YYYYDAYNAME')
       {
           if ($format == 'DAYNAME')
           {
               $retorno['orig'] = $rs->fields[$ind_ini];
               $ano             = "";
               $daynum          = $rs->fields[$ind_ini];
           }
           else
           {
               $retorno['orig'] = $rs->fields[$ind_ini] . $rs->fields[$ind_ini + 1];
               $ano             = " " . $rs->fields[$ind_ini];
               $daynum          = $rs->fields[$ind_ini + 1];
           }
           if (in_array(strtolower($this->nm_tpbanco), $this->nm_bases_access) || in_array(strtolower($this->nm_tpbanco), $this->nm_bases_oracle) || in_array(strtolower($this->nm_tpbanco), $this->nm_bases_mssql) || in_array(strtolower($this->nm_tpbanco), $this->nm_bases_db2) || in_array(strtolower($this->nm_tpbanco), $this->nm_bases_progress))
           {
               $daynum--;
           }
           if (in_array(strtolower($this->nm_tpbanco), $this->nm_bases_mysql))
           {
               $daynum = ($daynum == 6) ? 0 : $daynum + 1;
           }
           if ($daynum == 0) {
               $retorno['fmt'] = $Prefix_dat . $this->Nm_lang['lang_days_sund'] . $ano;
           }
           if ($daynum == 1) {
               $retorno['fmt'] = $Prefix_dat . $this->Nm_lang['lang_days_mond'] . $ano;
           }
           if ($daynum == 2) {
               $retorno['fmt'] = $Prefix_dat . $this->Nm_lang['lang_days_tued'] . $ano;
           }
           if ($daynum == 3) {
               $retorno['fmt'] = $Prefix_dat . $this->Nm_lang['lang_days_wend'] . $ano;
           }
           if ($daynum == 4) {
               $retorno['fmt'] = $Prefix_dat . $this->Nm_lang['lang_days_thud'] . $ano;
           }
           if ($daynum == 5) {
               $retorno['fmt'] = $Prefix_dat . $this->Nm_lang['lang_days_frid'] . $ano;
           }
           if ($daynum == 6) {
               $retorno['fmt'] = $Prefix_dat . $this->Nm_lang['lang_days_satd'] . $ano;
           }
           return $retorno;
       }
       if ($format == 'HH')
       {
           $this->Ajust_fields($ind_ini, $rs, "0");
           $temp            = "0000-00-00 " . $rs->fields[$ind_ini];
           $retorno['orig'] = $rs->fields[$ind_ini];
           $retorno['fmt']  = $this->GB_date_format($temp, $format, $Prefix_dat, $conf_region, $mask);
           return $retorno;
       }
       if ($format == 'DD')
       {
           $this->Ajust_fields($ind_ini, $rs, "0");
           $temp            = "0000-00-" . $rs->fields[$ind_ini];
           $retorno['orig'] = $rs->fields[$ind_ini];
           $retorno['fmt']  = $this->GB_date_format($temp, $format, $Prefix_dat, $conf_region, $mask);
           return $retorno;
       }
       if ($format == 'MM')
       {
           $this->Ajust_fields($ind_ini, $rs, "0");
           $temp            = "0000-" . $rs->fields[$ind_ini];
           $retorno['orig'] = $rs->fields[$ind_ini];
           $retorno['fmt']  = $this->GB_date_format($temp, $format, $Prefix_dat, $conf_region, $mask);
           return $retorno;
       }
       if ($format == 'YYYY')
       {
           $temp            = $rs->fields[$ind_ini];
           $retorno['orig'] = $rs->fields[$ind_ini];
           $retorno['fmt']  = $this->GB_date_format($temp, $format, $Prefix_dat, $conf_region, $mask);
           return $retorno;
       }
       if ($format == 'YYYYHH')
       {
           $this->Ajust_fields($ind_ini, $rs, "1");
           $temp            = $rs->fields[$ind_ini] . "-00-00 " . $rs->fields[$ind_ini + 1];
           $retorno['orig'] = $rs->fields[$ind_ini] . $rs->fields[$ind_ini + 1];
           $retorno['fmt']  = $this->GB_date_format($temp, $format, $Prefix_dat, $conf_region, $mask);
           return $retorno;
       }
       if ($format == 'YYYYDD')
       {
           $this->Ajust_fields($ind_ini, $rs, "1");
           $temp            = $rs->fields[$ind_ini] . "-00-" . $rs->fields[$ind_ini + 1];
           $retorno['orig'] = $rs->fields[$ind_ini] . $rs->fields[$ind_ini + 1];
           $retorno['fmt']  = $this->GB_date_format($temp, $format, $Prefix_dat, $conf_region, $mask);
           return $retorno;
       }
       elseif ($format == 'YYYYWEEK' || $format == 'YYYYBIMONTHLY' || $format == 'YYYYQUARTER' || $format == 'YYYYFOURMONTHS' || $format == 'YYYYSEMIANNUAL')
       {
           $temp            = (substr($rs->fields[$ind_ini + 1], 0, 1) == 0) ? substr($rs->fields[$ind_ini + 1], 1) : $rs->fields[$ind_ini + 1];
           $retorno['orig'] = $rs->fields[$ind_ini] . $rs->fields[$ind_ini + 1];
           $retorno['fmt']  = $Prefix_dat . $temp . " " . $rs->fields[$ind_ini];
           return $retorno;
       }
       if ($format == 'YYYYHH' || $format == 'YYYYDD')
       {
           $this->Ajust_fields($ind_ini, $rs, "1");
           $retorno['orig'] = $rs->fields[$ind_ini] . $rs->fields[$ind_ini + 1];
           $retorno['fmt']  = $rs->fields[$ind_ini] . $_SESSION['scriptcase']['reg_conf']['date_sep'] . $rs->fields[$ind_ini + 1];
           return $retorno;
       }
       elseif ($format == 'HHIISS')
       {
           $this->Ajust_fields($ind_ini, $rs, "0,1,2");
           $retorno['orig'] = $rs->fields[$ind_ini] . ":" . $rs->fields[$ind_ini + 1] . ":" . $rs->fields[$ind_ini + 2];
           $retorno['fmt']  = $this->GB_date_format("0000-00-00 " . $retorno['orig'], $format, $Prefix_dat, $conf_region, $mask);
           return $retorno;
       }
       elseif ($format == 'HHII')
       {
           $this->Ajust_fields($ind_ini, $rs, "0,1");
           $retorno['orig'] = $rs->fields[$ind_ini] . ":" . $rs->fields[$ind_ini + 1];
           $retorno['fmt']  = $this->GB_date_format("0000-00-00 " . $retorno['orig'], $format, $Prefix_dat, $conf_region, $mask);
           return $retorno;
       }
       else
       {
           $retorno['orig'] = $rs->fields[$ind_ini];
           $retorno['fmt']  = $rs->fields[$ind_ini];
           return $retorno;
       }
   }
   function Ajust_fields($ind_ini, &$rs, $parts)
   {
       $prep = explode(",", $parts);
       foreach ($prep as $ind)
       {
           $ind_ok = $ind_ini + $ind;
           $rs->fields[$ind_ok] = (int) $rs->fields[$ind_ok];
           if (strlen($rs->fields[$ind_ok]) == 1)
           {
               $rs->fields[$ind_ok] = "0" . $rs->fields[$ind_ok];
           }
       }
   }
   function Get_date_order_groupby($sql_def, $order, $format="", $order_old="")
   {
       $order      = " " . trim($order);
       $order_old .= (!empty($order_old)) ? ", " : "";
       return $order_old . $sql_def . $order;
   }
}
//===============================================================================
//
class module_apl
{
   var $Ini;
   var $Erro;
   var $Db;
   var $Lookup;
   var $nm_location;
//
//----- 
   function prep_modulos($modulo)
   {
      $this->$modulo->Ini = $this->Ini;
      $this->$modulo->Db = $this->Db;
      $this->$modulo->Erro = $this->Erro;
   }
//
//----- 
   function controle()
   {
      global $nm_saida, $nm_url_saida, $script_case_init, $glo_senha_protect;

      $this->Ini = new module_ini(); 
      $this->Ini->init();
      set_time_limit(10); 
      $this->Change_Menu = false;
      if (isset($_SESSION['scriptcase']['menu_atual']) && (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['module']['sc_outra_jan']) || !$_SESSION['sc_session'][$this->Ini->sc_page]['module']['sc_outra_jan']))
      {
          $this->sc_init_menu = "x";
          if (isset($_SESSION['scriptcase'][$_SESSION['scriptcase']['menu_atual']]['sc_init']['module']))
          {
              $this->sc_init_menu = $_SESSION['scriptcase'][$_SESSION['scriptcase']['menu_atual']]['sc_init']['module'];
          }
          elseif (isset($_SESSION['scriptcase']['menu_apls'][$_SESSION['scriptcase']['menu_atual']]))
          {
              foreach ($_SESSION['scriptcase']['menu_apls'][$_SESSION['scriptcase']['menu_atual']] as $init => $resto)
              {
                  if ($this->Ini->sc_page == $init)
                  {
                      $this->sc_init_menu = $init;
                      break;
                  }
              }
          }
          if ($this->Ini->sc_page == $this->sc_init_menu && !isset($_SESSION['scriptcase']['menu_apls'][$_SESSION['scriptcase']['menu_atual']][$this->sc_init_menu]['module']))
          {
               $_SESSION['scriptcase']['menu_apls'][$_SESSION['scriptcase']['menu_atual']][$this->sc_init_menu]['module']['link'] = $this->Ini->sc_protocolo . $this->Ini->server . $this->Ini->path_link . "" . SC_dir_app_name('module') . "/";
               $_SESSION['scriptcase']['menu_apls'][$_SESSION['scriptcase']['menu_atual']][$this->sc_init_menu]['module']['label'] = "" . $this->Ini->Nm_lang['lang_othr_blank_title'] . "";
               $this->Change_Menu = true;
          }
          elseif ($this->Ini->sc_page == $this->sc_init_menu)
          {
              $achou = false;
              foreach ($_SESSION['scriptcase']['menu_apls'][$_SESSION['scriptcase']['menu_atual']][$this->sc_init_menu] as $apl => $parms)
              {
                  if ($apl == "module")
                  {
                      $achou = true;
                  }
                  elseif ($achou)
                  {
                      unset($_SESSION['scriptcase']['menu_apls'][$_SESSION['scriptcase']['menu_atual']][$this->sc_init_menu][$apl]);
                      $this->Change_Menu = true;
                  }
              }
          }
      }
      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['module']['exit']) && $_SESSION['scriptcase']['sc_apl_conf']['module']['exit'] != '')
      {
          $_SESSION['scriptcase']['sc_url_saida'][$this->Ini->sc_page] = $_SESSION['scriptcase']['sc_apl_conf']['module']['exit'];
      }
      $glo_senha_protect = (isset($_SESSION['scriptcase']['glo_senha_protect'])) ? $_SESSION['scriptcase']['glo_senha_protect'] : "S";

      $this->Ini->sc_Include($this->Ini->path_libs . "/nm_gc.php", "F", "nm_gc") ; 
      nm_gc($this->Ini->path_libs);
      $this->nm_data = new nm_data("id");
      $_SESSION['scriptcase']['sc_tab_meses']['int'] = array(
                                  $this->Ini->Nm_lang['lang_mnth_janu'],
                                  $this->Ini->Nm_lang['lang_mnth_febr'],
                                  $this->Ini->Nm_lang['lang_mnth_marc'],
                                  $this->Ini->Nm_lang['lang_mnth_apri'],
                                  $this->Ini->Nm_lang['lang_mnth_mayy'],
                                  $this->Ini->Nm_lang['lang_mnth_june'],
                                  $this->Ini->Nm_lang['lang_mnth_july'],
                                  $this->Ini->Nm_lang['lang_mnth_augu'],
                                  $this->Ini->Nm_lang['lang_mnth_sept'],
                                  $this->Ini->Nm_lang['lang_mnth_octo'],
                                  $this->Ini->Nm_lang['lang_mnth_nove'],
                                  $this->Ini->Nm_lang['lang_mnth_dece']);
      $_SESSION['scriptcase']['sc_tab_meses']['abr'] = array(
                                  $this->Ini->Nm_lang['lang_shrt_mnth_janu'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_febr'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_marc'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_apri'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_mayy'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_june'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_july'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_augu'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_sept'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_octo'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_nove'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_dece']);
      $_SESSION['scriptcase']['sc_tab_dias']['int'] = array(
                                  $this->Ini->Nm_lang['lang_days_sund'],
                                  $this->Ini->Nm_lang['lang_days_mond'],
                                  $this->Ini->Nm_lang['lang_days_tued'],
                                  $this->Ini->Nm_lang['lang_days_wend'],
                                  $this->Ini->Nm_lang['lang_days_thud'],
                                  $this->Ini->Nm_lang['lang_days_frid'],
                                  $this->Ini->Nm_lang['lang_days_satd']);
      $_SESSION['scriptcase']['sc_tab_dias']['abr'] = array(
                                  $this->Ini->Nm_lang['lang_shrt_days_sund'],
                                  $this->Ini->Nm_lang['lang_shrt_days_mond'],
                                  $this->Ini->Nm_lang['lang_shrt_days_tued'],
                                  $this->Ini->Nm_lang['lang_shrt_days_wend'],
                                  $this->Ini->Nm_lang['lang_shrt_days_thud'],
                                  $this->Ini->Nm_lang['lang_shrt_days_frid'],
                                  $this->Ini->Nm_lang['lang_shrt_days_satd']);
      $this->Db = $this->Ini->Db; 
      include_once($this->Ini->path_aplicacao . "module_erro.class.php"); 
      $this->Erro      = new module_erro();
      $this->Erro->Ini = $this->Ini;
//
      $_SESSION['scriptcase']['module']['contr_erro'] = 'on';
  
      $nm_select = "SELECT 
  b.scpCode AS scp_code,
  FORMAT(b.`GotAnnoCallAddict`,'###,###,###')AS aparty_attemp,
  FORMAT(b.`APressOne`,'###,###,###')AS aparty_success,
  FORMAT((b.`GotAnnoCallAddict` - b.`APressOne`),'###,###,###') AS aparty_failed, 
  ROUND(CAST(((b.`APressOne`/b.`GotAnnoCallAddict`)*100) AS DOUBLE),2) AS aparty,
  FORMAT(b.`BoAnswer`,'###,###,###')AS bparty_attemp,
  FORMAT(b.`BPressOne`,'###,###,###')AS bparty_success,
  FORMAT((b.`BoAnswer` - b.`BPressOne`),'###,###,###') AS bparty_failed, 
  ROUND(CAST(((b.`BPressOne`/b.`BoAnswer`)*100) AS DOUBLE),2) AS bparty
FROM 
  (SELECT 
    scpcode, 
    MAX(scpDate) AS LastScpDate 
  FROM tbl_scp GROUP BY scpcode) AS a
  LEFT JOIN 
    (SELECT * FROM tbl_scp) AS b 
  ON b.scpdate=a.LastScpDate AND b.scpcode = a.scpcode"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->part = array();
      if ($rx = $this->Db->Execute($nm_select)) 
      { 
          $y = 0; 
          $nm_count = $rx->FieldCount();
          while (!$rx->EOF)
          { 
                 for ($x = 0; $x < $nm_count; $x++)
                 { 
                        $this->part[$y] [$x] = $rx->fields[$x];
                 }
                 $y++; 
                 $rx->MoveNext();
          } 
          $rx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->part = false;
          $this->part_erro = $this->Db->ErrorMsg();
      } 
;
for($party=0;$party<count($this->part );$party++){
  $myObj_party[0][] = array(
    'scp_code' => $this->part[$party][0],
    'aparty_attemp' => $this->part[$party][1],
    'aparty_success' => $this->part[$party][2],
    'aparty_failed' => $this->part[$party][3],
    'aparty' => $this->part[$party][4],
  'bparty_attemp' => $this->part[$party][5],
    'bparty_success' => $this->part[$party][6],
    'bparty_failed' => $this->part[$party][7],
    'bparty' => $this->part[$party][8]
  );
}

if($this->part[0][4] < 50){
$btnap1 = "<div id='btnap1' class='blink'>".$this->part[0][4]."%</div>";
}else{
$btnap1 = "<div id='btnap1' style='padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;'>".$this->part[0][4]."%</div>";
}

if($this->part[0][8] < 6){
$btnbp1 = "<div id='btnbp1' class='blink'>".$this->part[0][8]."%</div>";
}else{
$btnbp1 = "<div id='btnbp1' style='padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;'>".$this->part[0][8]."%</div>";
}

if($this->part[1][4] < 50){
$btnap2 = "<div id='btnap2' class='blink'>".$this->part[1][4]."%</div>";
}else{
$btnap2 = "<div id='btnap2' style='padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;'>".$this->part[1][4]."%</div>";
}

if($this->part[1][8] < 6){
$btnbp2 = "<div id='btnbp2' class='blink'>".$this->part[1][8]."%</div>";
}else{
$btnbp2 = "<div id='btnbp2' style='padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;'>".$this->part[1][8]."%</div>";
}

if($this->part[2][4] < 50){
$btnap3 = "<div id='btnap3' class='blink'>".$this->part[2][4]."%</div>";
}else{
$btnap3 = "<div id='btnap3' style='padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;'>".$this->part[2][4]."%</div>";
}

if($this->part[2][8] < 6){
$btnbp3 = "<div id='btnbp3' class='blink'>".$this->part[2][8]."%</div>";
}else{
$btnbp3 = "<div id='btnbp3' style='padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;'>".$this->part[2][8]."%</div>";
}

if($this->part[3][4] < 50){
$btnap4 = "<div id='btnap4' class='blink'>".$this->part[3][4]."%</div>";
}else{
$btnap4 = "<div id='btnap4' style='padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;'>".$this->part[3][4]."%</div>";
}

if($this->part[3][8] < 6){
$btnbp4 = "<div id='btnbp4' class='blink'>".$this->part[3][8]."%</div>";
}else{
$btnbp4 = "<div id='btnbp4' style='padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;'>".$this->part[3][8]."%</div>";
}


 
      $nm_select = "SELECT b.idScp AS id_scp, b.scpCode AS scp_code, b.bussinessSuccessRate AS business_percentage, FORMAT(b.BussinessAttemp, '###,###,###') AS business_attemp, FORMAT(b.BussinessFailed, '###,###,###') AS business_failed,
FORMAT(b.BussinessSuccess, '###,###,###') AS business_success, CAST(b.DSPSuccessRate AS DOUBLE) AS dsp_persentage, FORMAT(b.DSPAttemp, '###,###,###') AS dsp_attemp, FORMAT(b.DSPFailed, '###,###,###') AS dsp_failed, FORMAT(b.DSPSuccess, '###,###,###') AS dsp_success, CAST(b.ATISuccessRate AS DOUBLE) AS ati_persentage,
FORMAT(b.ATIAttemp, '###,###,###') AS ati_attemp, FORMAT(b.ATIFailed, '###,###,###') AS ati_failed, FORMAT(b.ATISuccess, '###,###,###') AS ati_success, b.scpDate AS scp_date, b.scpDay AS scp_day, b.scpHour AS scp_hour FROM (SELECT scpcode, MAX(scpDate) AS LastScpDate FROM v_rates GROUP BY scpcode) AS a
      LEFT JOIN (SELECT * FROM v_rates) AS b ON b.scpdate=a.LastScpDate AND b.scpcode = a.scpcode"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->ds = array();
      if ($rx = $this->Db->Execute($nm_select)) 
      { 
          $y = 0; 
          $nm_count = $rx->FieldCount();
          while (!$rx->EOF)
          { 
                 for ($x = 0; $x < $nm_count; $x++)
                 { 
                        $this->ds[$y] [$x] = $rx->fields[$x];
                 }
                 $y++; 
                 $rx->MoveNext();
          } 
          $rx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->ds = false;
          $this->ds_erro = $this->Db->ErrorMsg();
      } 
;
for($i=0;$i<count($this->ds );$i++){
  $myObj_1[0][] = array(
    'id_scp' => $this->ds[$i][0],
    'scp_code' => $this->ds[$i][1],
    'business_percentage' => $this->ds[$i][2],
    'business_attemp' => $this->ds[$i][3],
    'business_failed' => $this->ds[$i][4],
    'business_success' => $this->ds[$i][5],
    'dsp_persentage' => $this->ds[$i][6],
    'dsp_attemp' => $this->ds[$i][7],
    'dsp_failed' => $this->ds[$i][8],
    'dsp_success' => $this->ds[$i][9],
    'ati_persentage' => $this->ds[$i][10],
    'ati_attemp' => $this->ds[$i][11],
    'ati_failed' => $this->ds[$i][12],
    'ati_success' => $this->ds[$i][13],
    'scp_date' => $this->ds[$i][14],   
    'scp_day' => $this->ds[$i][15],
    'scp_hour' => $this->ds[$i][16]
  );
}

$mks_bisnis = "linear-gradient(to right, #427a91 ".$this->ds[0][2]."%, #9b9b9b ".$this->ds[0][2]."%)";
$mks_ati = "linear-gradient(to right, #427a91 ".$this->ds[0][10]."%, #9b9b9b ".$this->ds[0][10]."%)";
$mks_dsp = "linear-gradient(to right, #427a91 ".$this->ds[0][6]."%, #9b9b9b ".$this->ds[0][6]."%)";
$mks_aparty = "linear-gradient(to right, #427a91 ".$this->part[0][4]."%, #9b9b9b ".$this->part[0][4]."%)";
$mks_bparty = "linear-gradient(to right, #427a91 ".$this->part[0][8]."%, #9b9b9b ".$this->part[0][8]."%)";

$pku_bisnis = "linear-gradient(to right, #427a91 ".$this->ds[1][2]."%, #9b9b9b ".$this->ds[1][2]."%)";
$pku_ati = "linear-gradient(to right, #427a91 ".$this->ds[1][10]."%, #9b9b9b ".$this->ds[1][10]."%)";
$pku_dsp = "linear-gradient(to right, #427a91 ".$this->ds[1][6]."%, #9b9b9b ".$this->ds[1][6]."%)";
$pku_aparty = "linear-gradient(to right, #427a91 ".$this->part[1][4]."%, #9b9b9b ".$this->part[1][4]."%)";
$pku_bparty = "linear-gradient(to right, #427a91 ".$this->part[1][8]."%, #9b9b9b ".$this->part[1][8]."%)";


$sby_bisnis = "linear-gradient(to right, #427a91 ".$this->ds[2][2]."%, #9b9b9b ".$this->ds[2][2]."%)";
$sby_ati = "linear-gradient(to right, #427a91 ".$this->ds[2][10]."%, #9b9b9b ".$this->ds[2][10]."%)";
$sby_dsp = "linear-gradient(to right, #427a91 ".$this->ds[2][6]."%, #9b9b9b ".$this->ds[2][6]."%)";
$sby_aparty = "linear-gradient(to right, #427a91 ".$this->part[2][4]."%, #9b9b9b ".$this->part[2][4]."%)";
$sby_bparty = "linear-gradient(to right, #427a91 ".$this->part[2][8]."%, #9b9b9b ".$this->part[2][8]."%)";


$tbs_bisnis = "linear-gradient(to right, #427a91 ".$this->ds[3][2]."%, #9b9b9b ".$this->ds[3][2]."%)";
$tbs_ati = "linear-gradient(to right, #427a91 ".$this->ds[3][10]."%, #9b9b9b ".$this->ds[3][10]."%)";
$tbs_dsp = "linear-gradient(to right, #427a91 ".$this->ds[3][6]."%, #9b9b9b ".$this->ds[3][6]."%)";
$tbs_aparty = "linear-gradient(to right, #427a91 ".$this->part[3][4]."%, #9b9b9b ".$this->part[3][4]."%)";
$tbs_bparty = "linear-gradient(to right, #427a91 ".$this->part[3][8]."%, #9b9b9b ".$this->part[3][8]."%)";



$this->mks = $this->ds[0][0];
 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nm_select = "SELECT IdScp, ScpCode, FORMAT(GotAnnoCallAddict, '###.###.###') AS GotAnnoCallAddict, FORMAT(MaxDailyThreshold,'###.###.###') AS MaxDailyThreshold, FORMAT(APressEndCall,'###.###.###') AS APressEndCall,  FORMAT(ApressNotOne,'###.###.###') AS ApressNotOne, FORMAT(APressOne,'###.###.###') AS APressOne, FORMAT(QueryDSPSuccess,'###.###.###') AS QueryDSPSuccess, FORMAT(QueryDSPFailed,'###.###.###') AS QueryDSPFailed, FORMAT(BInsufficientBalance,'###.###.###') AS BInsufficientBalance, FORMAT(NoInfoOfB,'###.###.###') AS NoInfoOfB, FORMAT(BRoaming,'###.###.###') AS BRoaming, FORMAT(BUnReachable,'###.###.###') AS BUnReachable,  FORMAT(BBusy,'###.###.###') AS BBusy, FORMAT(BNoAnswer,'###.###.###') AS BNoAnswer, FORMAT(BoAnswer,'###.###.###') AS BoAnswer, FORMAT(BError,'###.###.###') AS BError, FORMAT(BOabandon,'###.###.###') AS BOabandon, FORMAT(BSelectTimeOut,'###.###.###') AS BSelectTimeOut, FORMAT(BATIFailed,'###.###.###') AS BATIFailed, FORMAT(BPressOne,'###.###.###') AS BPressOne, FORMAT(BCallA,'###.###.###') AS BCallA, FORMAT(AUnReachable,'###.###.###') AS AUnReachable, FORMAT(ABusy,'###.###.###') AS ABusy, FORMAT(ANoAnswer,'###.###.###') AS ANoAnswer, FORMAT(AoAnswer,'###.###.###') AS AoAnswer, str_replace (convert(char(10),ScpDay,102), '.', '-') + ' ' + convert(char(8),ScpDay,20), ScpHour, str_replace (convert(char(10),ScpDate,102), '.', '-') + ' ' + convert(char(8),ScpDate,20), ScpTimestamp FROM tbl_scp WHERE IdScp = '$this->mks'"; 
      }
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
          $nm_select = "SELECT IdScp, ScpCode, FORMAT(GotAnnoCallAddict, '###.###.###') AS GotAnnoCallAddict, FORMAT(MaxDailyThreshold,'###.###.###') AS MaxDailyThreshold, FORMAT(APressEndCall,'###.###.###') AS APressEndCall,  FORMAT(ApressNotOne,'###.###.###') AS ApressNotOne, FORMAT(APressOne,'###.###.###') AS APressOne, FORMAT(QueryDSPSuccess,'###.###.###') AS QueryDSPSuccess, FORMAT(QueryDSPFailed,'###.###.###') AS QueryDSPFailed, FORMAT(BInsufficientBalance,'###.###.###') AS BInsufficientBalance, FORMAT(NoInfoOfB,'###.###.###') AS NoInfoOfB, FORMAT(BRoaming,'###.###.###') AS BRoaming, FORMAT(BUnReachable,'###.###.###') AS BUnReachable,  FORMAT(BBusy,'###.###.###') AS BBusy, FORMAT(BNoAnswer,'###.###.###') AS BNoAnswer, FORMAT(BoAnswer,'###.###.###') AS BoAnswer, FORMAT(BError,'###.###.###') AS BError, FORMAT(BOabandon,'###.###.###') AS BOabandon, FORMAT(BSelectTimeOut,'###.###.###') AS BSelectTimeOut, FORMAT(BATIFailed,'###.###.###') AS BATIFailed, FORMAT(BPressOne,'###.###.###') AS BPressOne, FORMAT(BCallA,'###.###.###') AS BCallA, FORMAT(AUnReachable,'###.###.###') AS AUnReachable, FORMAT(ABusy,'###.###.###') AS ABusy, FORMAT(ANoAnswer,'###.###.###') AS ANoAnswer, FORMAT(AoAnswer,'###.###.###') AS AoAnswer, convert(char(23),ScpDay,121), ScpHour, convert(char(23),ScpDate,121), ScpTimestamp FROM tbl_scp WHERE IdScp = '$this->mks'"; 
      }
      else
      { 
          $nm_select = "SELECT IdScp, ScpCode, FORMAT(GotAnnoCallAddict, '###.###.###') AS GotAnnoCallAddict, FORMAT(MaxDailyThreshold,'###.###.###') AS MaxDailyThreshold, FORMAT(APressEndCall,'###.###.###') AS APressEndCall,
FORMAT(ApressNotOne,'###.###.###') AS ApressNotOne, FORMAT(APressOne,'###.###.###') AS APressOne, FORMAT(QueryDSPSuccess,'###.###.###') AS QueryDSPSuccess, FORMAT(QueryDSPFailed,'###.###.###') AS QueryDSPFailed, FORMAT(BInsufficientBalance,'###.###.###') AS BInsufficientBalance, FORMAT(NoInfoOfB,'###.###.###') AS NoInfoOfB, FORMAT(BRoaming,'###.###.###') AS BRoaming, FORMAT(BUnReachable,'###.###.###') AS BUnReachable,
FORMAT(BBusy,'###.###.###') AS BBusy, FORMAT(BNoAnswer,'###.###.###') AS BNoAnswer, FORMAT(BoAnswer,'###.###.###') AS BoAnswer, FORMAT(BError,'###.###.###') AS BError, FORMAT(BOabandon,'###.###.###') AS BOabandon, FORMAT(BSelectTimeOut,'###.###.###') AS BSelectTimeOut, FORMAT(BATIFailed,'###.###.###') AS BATIFailed, FORMAT(BPressOne,'###.###.###') AS BPressOne, FORMAT(BCallA,'###.###.###') AS BCallA, FORMAT(AUnReachable,'###.###.###') AS AUnReachable, FORMAT(ABusy,'###.###.###') AS ABusy, FORMAT(ANoAnswer,'###.###.###') AS ANoAnswer, FORMAT(AoAnswer,'###.###.###') AS AoAnswer, ScpDay, ScpHour, ScpDate, ScpTimestamp FROM tbl_scp WHERE IdScp = '$this->mks'"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->mks = array();
      if ($rx = $this->Db->Execute($nm_select)) 
      { 
          $y = 0; 
          $nm_count = $rx->FieldCount();
          while (!$rx->EOF)
          { 
                 $rx->fields[0] = str_replace(',', '.', $rx->fields[0]);
                 $rx->fields[27] = str_replace(',', '.', $rx->fields[27]);
                 $rx->fields[0] = (strpos(strtolower($rx->fields[0]), "e")) ? (float)$rx->fields[0] : $rx->fields[0];
                 $rx->fields[0] = (string)$rx->fields[0];
                 $rx->fields[27] = (strpos(strtolower($rx->fields[27]), "e")) ? (float)$rx->fields[27] : $rx->fields[27];
                 $rx->fields[27] = (string)$rx->fields[27];
                 for ($x = 0; $x < $nm_count; $x++)
                 { 
                        $this->mks[$y] [$x] = $rx->fields[$x];
                 }
                 $y++; 
                 $rx->MoveNext();
          } 
          $rx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->mks = false;
          $this->mks_erro = $this->Db->ErrorMsg();
      } 
;
for($a=0;$a<count($this->mks );$a++){
  $myObj_2[0][] = array(
    'IdScp' => $this->mks[$a][0],
    'ScpCode' => $this->mks[$a][1],
    'GotAnnoCallAddict' => $this->mks[$a][2],
    'MaxDailyThreshold' => $this->mks[$a][3],
    'APressEndCall' => $this->mks[$a][4],
    'ApressNotOne' => $this->mks[$a][5],
    'APressOne' => $this->mks[$a][6],
    'QueryDSPSuccess' => $this->mks[$a][7],
    'QueryDSPFailed' => $this->mks[$a][8],
    'BInsufficientBalance' => $this->mks[$a][9],
    'NoInfoOfB' => $this->mks[$a][10],
    'BRoaming' => $this->mks[$a][11],
    'BUnReachable' => $this->mks[$a][12],
    'BBusy' => $this->mks[$a][13],
    'BNoAnswer' => $this->mks[$a][14],   
    'BoAnswer' => $this->mks[$a][15],
    'BError' => $this->mks[$a][16],  
  'BOabandon' => $this->mks[$a][17],
    'BSelectTimeOut' => $this->mks[$a][18],
    'BATIFailed' => $this->mks[$a][19],
    'BPressOne' => $this->mks[$a][20],
    'BCallA' => $this->mks[$a][21],
    'AUnReachable' => $this->mks[$a][22],
    'ABusy' => $this->mks[$a][23],
    'ANoAnswer' => $this->mks[$a][24],
    'AoAnswer' => $this->mks[$a][25],
    'ScpDay' => $this->mks[$a][26],
    'ScpHour' => $this->mks[$a][27],
    'ScpDate' => $this->mks[$a][28],
    'ScpTimestamp' => $this->mks[$a][29]
  );
}

$this->pku = $this->ds[1][0];
 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nm_select = "SELECT IdScp, ScpCode, FORMAT(GotAnnoCallAddict, '###.###.###') AS GotAnnoCallAddict, FORMAT(MaxDailyThreshold,'###.###.###') AS MaxDailyThreshold, FORMAT(APressEndCall,'###.###.###') AS APressEndCall,  FORMAT(ApressNotOne,'###.###.###') AS ApressNotOne, FORMAT(APressOne,'###.###.###') AS APressOne, FORMAT(QueryDSPSuccess,'###.###.###') AS QueryDSPSuccess, FORMAT(QueryDSPFailed,'###.###.###') AS QueryDSPFailed, FORMAT(BInsufficientBalance,'###.###.###') AS BInsufficientBalance, FORMAT(NoInfoOfB,'###.###.###') AS NoInfoOfB, FORMAT(BRoaming,'###.###.###') AS BRoaming, FORMAT(BUnReachable,'###.###.###') AS BUnReachable,  FORMAT(BBusy,'###.###.###') AS BBusy, FORMAT(BNoAnswer,'###.###.###') AS BNoAnswer, FORMAT(BoAnswer,'###.###.###') AS BoAnswer, FORMAT(BError,'###.###.###') AS BError, FORMAT(BOabandon,'###.###.###') AS BOabandon, FORMAT(BSelectTimeOut,'###.###.###') AS BSelectTimeOut, FORMAT(BATIFailed,'###.###.###') AS BATIFailed, FORMAT(BPressOne,'###.###.###') AS BPressOne, FORMAT(BCallA,'###.###.###') AS BCallA, FORMAT(AUnReachable,'###.###.###') AS AUnReachable, FORMAT(ABusy,'###.###.###') AS ABusy, FORMAT(ANoAnswer,'###.###.###') AS ANoAnswer, FORMAT(AoAnswer,'###.###.###') AS AoAnswer, str_replace (convert(char(10),ScpDay,102), '.', '-') + ' ' + convert(char(8),ScpDay,20), ScpHour, str_replace (convert(char(10),ScpDate,102), '.', '-') + ' ' + convert(char(8),ScpDate,20), ScpTimestamp FROM tbl_scp WHERE IdScp = '$this->pku'"; 
      }
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
          $nm_select = "SELECT IdScp, ScpCode, FORMAT(GotAnnoCallAddict, '###.###.###') AS GotAnnoCallAddict, FORMAT(MaxDailyThreshold,'###.###.###') AS MaxDailyThreshold, FORMAT(APressEndCall,'###.###.###') AS APressEndCall,  FORMAT(ApressNotOne,'###.###.###') AS ApressNotOne, FORMAT(APressOne,'###.###.###') AS APressOne, FORMAT(QueryDSPSuccess,'###.###.###') AS QueryDSPSuccess, FORMAT(QueryDSPFailed,'###.###.###') AS QueryDSPFailed, FORMAT(BInsufficientBalance,'###.###.###') AS BInsufficientBalance, FORMAT(NoInfoOfB,'###.###.###') AS NoInfoOfB, FORMAT(BRoaming,'###.###.###') AS BRoaming, FORMAT(BUnReachable,'###.###.###') AS BUnReachable,  FORMAT(BBusy,'###.###.###') AS BBusy, FORMAT(BNoAnswer,'###.###.###') AS BNoAnswer, FORMAT(BoAnswer,'###.###.###') AS BoAnswer, FORMAT(BError,'###.###.###') AS BError, FORMAT(BOabandon,'###.###.###') AS BOabandon, FORMAT(BSelectTimeOut,'###.###.###') AS BSelectTimeOut, FORMAT(BATIFailed,'###.###.###') AS BATIFailed, FORMAT(BPressOne,'###.###.###') AS BPressOne, FORMAT(BCallA,'###.###.###') AS BCallA, FORMAT(AUnReachable,'###.###.###') AS AUnReachable, FORMAT(ABusy,'###.###.###') AS ABusy, FORMAT(ANoAnswer,'###.###.###') AS ANoAnswer, FORMAT(AoAnswer,'###.###.###') AS AoAnswer, convert(char(23),ScpDay,121), ScpHour, convert(char(23),ScpDate,121), ScpTimestamp FROM tbl_scp WHERE IdScp = '$this->pku'"; 
      }
      else
      { 
          $nm_select = "SELECT IdScp, ScpCode, FORMAT(GotAnnoCallAddict, '###.###.###') AS GotAnnoCallAddict, FORMAT(MaxDailyThreshold,'###.###.###') AS MaxDailyThreshold, FORMAT(APressEndCall,'###.###.###') AS APressEndCall,
FORMAT(ApressNotOne,'###.###.###') AS ApressNotOne, FORMAT(APressOne,'###.###.###') AS APressOne, FORMAT(QueryDSPSuccess,'###.###.###') AS QueryDSPSuccess, FORMAT(QueryDSPFailed,'###.###.###') AS QueryDSPFailed, FORMAT(BInsufficientBalance,'###.###.###') AS BInsufficientBalance, FORMAT(NoInfoOfB,'###.###.###') AS NoInfoOfB, FORMAT(BRoaming,'###.###.###') AS BRoaming, FORMAT(BUnReachable,'###.###.###') AS BUnReachable,
FORMAT(BBusy,'###.###.###') AS BBusy, FORMAT(BNoAnswer,'###.###.###') AS BNoAnswer, FORMAT(BoAnswer,'###.###.###') AS BoAnswer, FORMAT(BError,'###.###.###') AS BError, FORMAT(BOabandon,'###.###.###') AS BOabandon, FORMAT(BSelectTimeOut,'###.###.###') AS BSelectTimeOut, FORMAT(BATIFailed,'###.###.###') AS BATIFailed, FORMAT(BPressOne,'###.###.###') AS BPressOne, FORMAT(BCallA,'###.###.###') AS BCallA, FORMAT(AUnReachable,'###.###.###') AS AUnReachable, FORMAT(ABusy,'###.###.###') AS ABusy, FORMAT(ANoAnswer,'###.###.###') AS ANoAnswer, FORMAT(AoAnswer,'###.###.###') AS AoAnswer, ScpDay, ScpHour, ScpDate, ScpTimestamp FROM tbl_scp WHERE IdScp = '$this->pku'"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->pku = array();
      if ($rx = $this->Db->Execute($nm_select)) 
      { 
          $y = 0; 
          $nm_count = $rx->FieldCount();
          while (!$rx->EOF)
          { 
                 $rx->fields[0] = str_replace(',', '.', $rx->fields[0]);
                 $rx->fields[27] = str_replace(',', '.', $rx->fields[27]);
                 $rx->fields[0] = (strpos(strtolower($rx->fields[0]), "e")) ? (float)$rx->fields[0] : $rx->fields[0];
                 $rx->fields[0] = (string)$rx->fields[0];
                 $rx->fields[27] = (strpos(strtolower($rx->fields[27]), "e")) ? (float)$rx->fields[27] : $rx->fields[27];
                 $rx->fields[27] = (string)$rx->fields[27];
                 for ($x = 0; $x < $nm_count; $x++)
                 { 
                        $this->pku[$y] [$x] = $rx->fields[$x];
                 }
                 $y++; 
                 $rx->MoveNext();
          } 
          $rx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->pku = false;
          $this->pku_erro = $this->Db->ErrorMsg();
      } 
;
for($a=0;$a<count($this->pku );$a++){
  $myObj_3[0][] = array(
    'IdScp' => $this->pku[$a][0],
    'ScpCode' => $this->pku[$a][1],
    'GotAnnoCallAddict' => $this->pku[$a][2],
    'MaxDailyThreshold' => $this->pku[$a][3],
    'APressEndCall' => $this->pku[$a][4],
    'ApressNotOne' => $this->pku[$a][5],
    'APressOne' => $this->pku[$a][6],
    'QueryDSPSuccess' => $this->pku[$a][7],
    'QueryDSPFailed' => $this->pku[$a][8],
    'BInsufficientBalance' => $this->pku[$a][9],
    'NoInfoOfB' => $this->pku[$a][10],
    'BRoaming' => $this->pku[$a][11],
    'BUnReachable' => $this->pku[$a][12],
    'BBusy' => $this->pku[$a][13],
    'BNoAnswer' => $this->pku[$a][14],   
    'BoAnswer' => $this->pku[$a][15],
    'BError' => $this->pku[$a][16],  
    'BOabandon' => $this->pku[$a][17],
    'BSelectTimeOut' => $this->pku[$a][18],
    'BATIFailed' => $this->pku[$a][19],
    'BPressOne' => $this->pku[$a][20],
    'BCallA' => $this->pku[$a][21],
    'AUnReachable' => $this->pku[$a][22],
    'ABusy' => $this->pku[$a][23],
    'ANoAnswer' => $this->pku[$a][24],
    'AoAnswer' => $this->pku[$a][25],
    'ScpDay' => $this->pku[$a][26],
    'ScpHour' => $this->pku[$a][27],
    'ScpDate' => $this->pku[$a][28],
    'ScpTimestamp' => $this->pku[$a][29]
  );
}

$this->sby = $this->ds[2][0];
 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nm_select = "SELECT IdScp, ScpCode, FORMAT(GotAnnoCallAddict, '###.###.###') AS GotAnnoCallAddict, FORMAT(MaxDailyThreshold,'###.###.###') AS MaxDailyThreshold, FORMAT(APressEndCall,'###.###.###') AS APressEndCall,  FORMAT(ApressNotOne,'###.###.###') AS ApressNotOne, FORMAT(APressOne,'###.###.###') AS APressOne, FORMAT(QueryDSPSuccess,'###.###.###') AS QueryDSPSuccess, FORMAT(QueryDSPFailed,'###.###.###') AS QueryDSPFailed, FORMAT(BInsufficientBalance,'###.###.###') AS BInsufficientBalance, FORMAT(NoInfoOfB,'###.###.###') AS NoInfoOfB, FORMAT(BRoaming,'###.###.###') AS BRoaming, FORMAT(BUnReachable,'###.###.###') AS BUnReachable,  FORMAT(BBusy,'###.###.###') AS BBusy, FORMAT(BNoAnswer,'###.###.###') AS BNoAnswer, FORMAT(BoAnswer,'###.###.###') AS BoAnswer, FORMAT(BError,'###.###.###') AS BError, FORMAT(BOabandon,'###.###.###') AS BOabandon, FORMAT(BSelectTimeOut,'###.###.###') AS BSelectTimeOut, FORMAT(BATIFailed,'###.###.###') AS BATIFailed, FORMAT(BPressOne,'###.###.###') AS BPressOne, FORMAT(BCallA,'###.###.###') AS BCallA, FORMAT(AUnReachable,'###.###.###') AS AUnReachable, FORMAT(ABusy,'###.###.###') AS ABusy, FORMAT(ANoAnswer,'###.###.###') AS ANoAnswer, FORMAT(AoAnswer,'###.###.###') AS AoAnswer, str_replace (convert(char(10),ScpDay,102), '.', '-') + ' ' + convert(char(8),ScpDay,20), ScpHour, str_replace (convert(char(10),ScpDate,102), '.', '-') + ' ' + convert(char(8),ScpDate,20), ScpTimestamp FROM tbl_scp WHERE IdScp = '$this->sby'"; 
      }
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
          $nm_select = "SELECT IdScp, ScpCode, FORMAT(GotAnnoCallAddict, '###.###.###') AS GotAnnoCallAddict, FORMAT(MaxDailyThreshold,'###.###.###') AS MaxDailyThreshold, FORMAT(APressEndCall,'###.###.###') AS APressEndCall,  FORMAT(ApressNotOne,'###.###.###') AS ApressNotOne, FORMAT(APressOne,'###.###.###') AS APressOne, FORMAT(QueryDSPSuccess,'###.###.###') AS QueryDSPSuccess, FORMAT(QueryDSPFailed,'###.###.###') AS QueryDSPFailed, FORMAT(BInsufficientBalance,'###.###.###') AS BInsufficientBalance, FORMAT(NoInfoOfB,'###.###.###') AS NoInfoOfB, FORMAT(BRoaming,'###.###.###') AS BRoaming, FORMAT(BUnReachable,'###.###.###') AS BUnReachable,  FORMAT(BBusy,'###.###.###') AS BBusy, FORMAT(BNoAnswer,'###.###.###') AS BNoAnswer, FORMAT(BoAnswer,'###.###.###') AS BoAnswer, FORMAT(BError,'###.###.###') AS BError, FORMAT(BOabandon,'###.###.###') AS BOabandon, FORMAT(BSelectTimeOut,'###.###.###') AS BSelectTimeOut, FORMAT(BATIFailed,'###.###.###') AS BATIFailed, FORMAT(BPressOne,'###.###.###') AS BPressOne, FORMAT(BCallA,'###.###.###') AS BCallA, FORMAT(AUnReachable,'###.###.###') AS AUnReachable, FORMAT(ABusy,'###.###.###') AS ABusy, FORMAT(ANoAnswer,'###.###.###') AS ANoAnswer, FORMAT(AoAnswer,'###.###.###') AS AoAnswer, convert(char(23),ScpDay,121), ScpHour, convert(char(23),ScpDate,121), ScpTimestamp FROM tbl_scp WHERE IdScp = '$this->sby'"; 
      }
      else
      { 
          $nm_select = "SELECT IdScp, ScpCode, FORMAT(GotAnnoCallAddict, '###.###.###') AS GotAnnoCallAddict, FORMAT(MaxDailyThreshold,'###.###.###') AS MaxDailyThreshold, FORMAT(APressEndCall,'###.###.###') AS APressEndCall,
FORMAT(ApressNotOne,'###.###.###') AS ApressNotOne, FORMAT(APressOne,'###.###.###') AS APressOne, FORMAT(QueryDSPSuccess,'###.###.###') AS QueryDSPSuccess, FORMAT(QueryDSPFailed,'###.###.###') AS QueryDSPFailed, FORMAT(BInsufficientBalance,'###.###.###') AS BInsufficientBalance, FORMAT(NoInfoOfB,'###.###.###') AS NoInfoOfB, FORMAT(BRoaming,'###.###.###') AS BRoaming, FORMAT(BUnReachable,'###.###.###') AS BUnReachable,
FORMAT(BBusy,'###.###.###') AS BBusy, FORMAT(BNoAnswer,'###.###.###') AS BNoAnswer, FORMAT(BoAnswer,'###.###.###') AS BoAnswer, FORMAT(BError,'###.###.###') AS BError, FORMAT(BOabandon,'###.###.###') AS BOabandon, FORMAT(BSelectTimeOut,'###.###.###') AS BSelectTimeOut, FORMAT(BATIFailed,'###.###.###') AS BATIFailed, FORMAT(BPressOne,'###.###.###') AS BPressOne, FORMAT(BCallA,'###.###.###') AS BCallA, FORMAT(AUnReachable,'###.###.###') AS AUnReachable, FORMAT(ABusy,'###.###.###') AS ABusy, FORMAT(ANoAnswer,'###.###.###') AS ANoAnswer, FORMAT(AoAnswer,'###.###.###') AS AoAnswer, ScpDay, ScpHour, ScpDate, ScpTimestamp FROM tbl_scp WHERE IdScp = '$this->sby'"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->sby = array();
      if ($rx = $this->Db->Execute($nm_select)) 
      { 
          $y = 0; 
          $nm_count = $rx->FieldCount();
          while (!$rx->EOF)
          { 
                 $rx->fields[0] = str_replace(',', '.', $rx->fields[0]);
                 $rx->fields[27] = str_replace(',', '.', $rx->fields[27]);
                 $rx->fields[0] = (strpos(strtolower($rx->fields[0]), "e")) ? (float)$rx->fields[0] : $rx->fields[0];
                 $rx->fields[0] = (string)$rx->fields[0];
                 $rx->fields[27] = (strpos(strtolower($rx->fields[27]), "e")) ? (float)$rx->fields[27] : $rx->fields[27];
                 $rx->fields[27] = (string)$rx->fields[27];
                 for ($x = 0; $x < $nm_count; $x++)
                 { 
                        $this->sby[$y] [$x] = $rx->fields[$x];
                 }
                 $y++; 
                 $rx->MoveNext();
          } 
          $rx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->sby = false;
          $this->sby_erro = $this->Db->ErrorMsg();
      } 
;
for($a=0;$a<count($this->sby );$a++){
  $myObj_4[0][] = array(
    'IdScp' => $this->sby[$a][0],
    'ScpCode' => $this->sby[$a][1],
    'GotAnnoCallAddict' => $this->sby[$a][2],
    'MaxDailyThreshold' => $this->sby[$a][3],
    'APressEndCall' => $this->sby[$a][4],
    'ApressNotOne' => $this->sby[$a][5],
    'APressOne' => $this->sby[$a][6],
    'QueryDSPSuccess' => $this->sby[$a][7],
    'QueryDSPFailed' => $this->sby[$a][8],
    'BInsufficientBalance' => $this->sby[$a][9],
    'NoInfoOfB' => $this->sby[$a][10],
    'BRoaming' => $this->sby[$a][11],
    'BUnReachable' => $this->sby[$a][12],
    'BBusy' => $this->sby[$a][13],
    'BNoAnswer' => $this->sby[$a][14],   
    'BoAnswer' => $this->sby[$a][15],
    'BError' => $this->sby[$a][16],  
  'BOabandon' => $this->sby[$a][17],
    'BSelectTimeOut' => $this->sby[$a][18],
    'BATIFailed' => $this->sby[$a][19],
    'BPressOne' => $this->sby[$a][20],
    'BCallA' => $this->sby[$a][21],
    'AUnReachable' => $this->sby[$a][22],
    'ABusy' => $this->sby[$a][23],
    'ANoAnswer' => $this->sby[$a][24],
    'AoAnswer' => $this->sby[$a][25],
    'ScpDay' => $this->sby[$a][26],
    'ScpHour' => $this->sby[$a][27],
    'ScpDate' => $this->sby[$a][28],
    'ScpTimestamp' => $this->sby[$a][29]
  );
}

$this->tbs = $this->ds[3][0];
 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nm_select = "SELECT IdScp, ScpCode, FORMAT(GotAnnoCallAddict, '###.###.###') AS GotAnnoCallAddict, FORMAT(MaxDailyThreshold,'###.###.###') AS MaxDailyThreshold, FORMAT(APressEndCall,'###.###.###') AS APressEndCall,  FORMAT(ApressNotOne,'###.###.###') AS ApressNotOne, FORMAT(APressOne,'###.###.###') AS APressOne, FORMAT(QueryDSPSuccess,'###.###.###') AS QueryDSPSuccess, FORMAT(QueryDSPFailed,'###.###.###') AS QueryDSPFailed, FORMAT(BInsufficientBalance,'###.###.###') AS BInsufficientBalance, FORMAT(NoInfoOfB,'###.###.###') AS NoInfoOfB, FORMAT(BRoaming,'###.###.###') AS BRoaming, FORMAT(BUnReachable,'###.###.###') AS BUnReachable,  FORMAT(BBusy,'###.###.###') AS BBusy, FORMAT(BNoAnswer,'###.###.###') AS BNoAnswer, FORMAT(BoAnswer,'###.###.###') AS BoAnswer, FORMAT(BError,'###.###.###') AS BError, FORMAT(BOabandon,'###.###.###') AS BOabandon, FORMAT(BSelectTimeOut,'###.###.###') AS BSelectTimeOut, FORMAT(BATIFailed,'###.###.###') AS BATIFailed, FORMAT(BPressOne,'###.###.###') AS BPressOne, FORMAT(BCallA,'###.###.###') AS BCallA, FORMAT(AUnReachable,'###.###.###') AS AUnReachable, FORMAT(ABusy,'###.###.###') AS ABusy, FORMAT(ANoAnswer,'###.###.###') AS ANoAnswer, FORMAT(AoAnswer,'###.###.###') AS AoAnswer, str_replace (convert(char(10),ScpDay,102), '.', '-') + ' ' + convert(char(8),ScpDay,20), ScpHour, str_replace (convert(char(10),ScpDate,102), '.', '-') + ' ' + convert(char(8),ScpDate,20), ScpTimestamp FROM tbl_scp WHERE IdScp = '$this->tbs'"; 
      }
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
          $nm_select = "SELECT IdScp, ScpCode, FORMAT(GotAnnoCallAddict, '###.###.###') AS GotAnnoCallAddict, FORMAT(MaxDailyThreshold,'###.###.###') AS MaxDailyThreshold, FORMAT(APressEndCall,'###.###.###') AS APressEndCall,  FORMAT(ApressNotOne,'###.###.###') AS ApressNotOne, FORMAT(APressOne,'###.###.###') AS APressOne, FORMAT(QueryDSPSuccess,'###.###.###') AS QueryDSPSuccess, FORMAT(QueryDSPFailed,'###.###.###') AS QueryDSPFailed, FORMAT(BInsufficientBalance,'###.###.###') AS BInsufficientBalance, FORMAT(NoInfoOfB,'###.###.###') AS NoInfoOfB, FORMAT(BRoaming,'###.###.###') AS BRoaming, FORMAT(BUnReachable,'###.###.###') AS BUnReachable,  FORMAT(BBusy,'###.###.###') AS BBusy, FORMAT(BNoAnswer,'###.###.###') AS BNoAnswer, FORMAT(BoAnswer,'###.###.###') AS BoAnswer, FORMAT(BError,'###.###.###') AS BError, FORMAT(BOabandon,'###.###.###') AS BOabandon, FORMAT(BSelectTimeOut,'###.###.###') AS BSelectTimeOut, FORMAT(BATIFailed,'###.###.###') AS BATIFailed, FORMAT(BPressOne,'###.###.###') AS BPressOne, FORMAT(BCallA,'###.###.###') AS BCallA, FORMAT(AUnReachable,'###.###.###') AS AUnReachable, FORMAT(ABusy,'###.###.###') AS ABusy, FORMAT(ANoAnswer,'###.###.###') AS ANoAnswer, FORMAT(AoAnswer,'###.###.###') AS AoAnswer, convert(char(23),ScpDay,121), ScpHour, convert(char(23),ScpDate,121), ScpTimestamp FROM tbl_scp WHERE IdScp = '$this->tbs'"; 
      }
      else
      { 
          $nm_select = "SELECT IdScp, ScpCode, FORMAT(GotAnnoCallAddict, '###.###.###') AS GotAnnoCallAddict, FORMAT(MaxDailyThreshold,'###.###.###') AS MaxDailyThreshold, FORMAT(APressEndCall,'###.###.###') AS APressEndCall,
FORMAT(ApressNotOne,'###.###.###') AS ApressNotOne, FORMAT(APressOne,'###.###.###') AS APressOne, FORMAT(QueryDSPSuccess,'###.###.###') AS QueryDSPSuccess, FORMAT(QueryDSPFailed,'###.###.###') AS QueryDSPFailed, FORMAT(BInsufficientBalance,'###.###.###') AS BInsufficientBalance, FORMAT(NoInfoOfB,'###.###.###') AS NoInfoOfB, FORMAT(BRoaming,'###.###.###') AS BRoaming, FORMAT(BUnReachable,'###.###.###') AS BUnReachable,
FORMAT(BBusy,'###.###.###') AS BBusy, FORMAT(BNoAnswer,'###.###.###') AS BNoAnswer, FORMAT(BoAnswer,'###.###.###') AS BoAnswer, FORMAT(BError,'###.###.###') AS BError, FORMAT(BOabandon,'###.###.###') AS BOabandon, FORMAT(BSelectTimeOut,'###.###.###') AS BSelectTimeOut, FORMAT(BATIFailed,'###.###.###') AS BATIFailed, FORMAT(BPressOne,'###.###.###') AS BPressOne, FORMAT(BCallA,'###.###.###') AS BCallA, FORMAT(AUnReachable,'###.###.###') AS AUnReachable, FORMAT(ABusy,'###.###.###') AS ABusy, FORMAT(ANoAnswer,'###.###.###') AS ANoAnswer, FORMAT(AoAnswer,'###.###.###') AS AoAnswer, ScpDay, ScpHour, ScpDate, ScpTimestamp FROM tbl_scp WHERE IdScp = '$this->tbs'"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->tbs = array();
      if ($rx = $this->Db->Execute($nm_select)) 
      { 
          $y = 0; 
          $nm_count = $rx->FieldCount();
          while (!$rx->EOF)
          { 
                 $rx->fields[0] = str_replace(',', '.', $rx->fields[0]);
                 $rx->fields[27] = str_replace(',', '.', $rx->fields[27]);
                 $rx->fields[0] = (strpos(strtolower($rx->fields[0]), "e")) ? (float)$rx->fields[0] : $rx->fields[0];
                 $rx->fields[0] = (string)$rx->fields[0];
                 $rx->fields[27] = (strpos(strtolower($rx->fields[27]), "e")) ? (float)$rx->fields[27] : $rx->fields[27];
                 $rx->fields[27] = (string)$rx->fields[27];
                 for ($x = 0; $x < $nm_count; $x++)
                 { 
                        $this->tbs[$y] [$x] = $rx->fields[$x];
                 }
                 $y++; 
                 $rx->MoveNext();
          } 
          $rx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->tbs = false;
          $this->tbs_erro = $this->Db->ErrorMsg();
      } 
;
for($a=0;$a<count($this->tbs );$a++){
  $myObj_5[0][] = array(
    'IdScp' => $this->tbs[$a][0],
    'ScpCode' => $this->tbs[$a][1],
    'GotAnnoCallAddict' => $this->tbs[$a][2],
    'MaxDailyThreshold' => $this->tbs[$a][3],
    'APressEndCall' => $this->tbs[$a][4],
    'ApressNotOne' => $this->tbs[$a][5],
    'APressOne' => $this->tbs[$a][6],
    'QueryDSPSuccess' => $this->tbs[$a][7],
    'QueryDSPFailed' => $this->tbs[$a][8],
    'BInsufficientBalance' => $this->tbs[$a][9],
    'NoInfoOfB' => $this->tbs[$a][10],
    'BRoaming' => $this->tbs[$a][11],
    'BUnReachable' => $this->tbs[$a][12],
    'BBusy' => $this->tbs[$a][13],
    'BNoAnswer' => $this->tbs[$a][14],   
    'BoAnswer' => $this->tbs[$a][15],
    'BError' => $this->tbs[$a][16],  
    'BOabandon' => $this->tbs[$a][17],
    'BSelectTimeOut' => $this->tbs[$a][18],
    'BATIFailed' => $this->tbs[$a][19],
    'BPressOne' => $this->tbs[$a][20],
    'BCallA' => $this->tbs[$a][21],
    'AUnReachable' => $this->tbs[$a][22],
    'ABusy' => $this->tbs[$a][23],
    'ANoAnswer' => $this->tbs[$a][24],
    'AoAnswer' => $this->tbs[$a][25],
    'ScpDay' => $this->tbs[$a][26],
    'ScpHour' => $this->tbs[$a][27],
    'ScpDate' => $this->tbs[$a][28],
    'ScpTimestamp' => $this->tbs[$a][29]
  );
}

$mks1 = $this->ds[0][1];
$pku1 = $this->ds[1][1];
$sby1 = $this->ds[2][1];
$tbs1 = $this->ds[3][1];



 
      $nm_select = "SELECT * FROM (SELECT CAST(scpDate AS DATETIME) AS scpDate,DSPAttemp,DSPFailed,DSPSuccess FROM v_rates WHERE scpCode =  '$mks1' ORDER BY scpDate DESC LIMIT 30)a ORDER BY scpDate ASC"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->categoriesdsp1 = array();
      if ($rx = $this->Db->Execute($nm_select)) 
      { 
          $y = 0; 
          $nm_count = $rx->FieldCount();
          while (!$rx->EOF)
          { 
                 for ($x = 0; $x < $nm_count; $x++)
                 { 
                        $this->categoriesdsp1[$y] [$x] = $rx->fields[$x];
                 }
                 $y++; 
                 $rx->MoveNext();
          } 
          $rx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->categoriesdsp1 = false;
          $this->categoriesdsp1_erro = $this->Db->ErrorMsg();
      } 
;

for($b=0;$b<count($this->categoriesdsp1 );$b++){
  $mks_dsp_a[] = array($this->categoriesdsp1[$b][0]);
  $mks_dsp_b[] = array($this->categoriesdsp1[$b][1]);
  $mks_dsp_c[] = array($this->categoriesdsp1[$b][2]);
  $mks_dsp_d[] = array($this->categoriesdsp1[$b][3]);  
}

$mks_dsp_1 = str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($mks_dsp_a))));

$mks_dsp_2 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($mks_dsp_b)))));

$mks_dsp_3 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($mks_dsp_c)))));

$mks_dsp_4 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($mks_dsp_d)))));


 
      $nm_select = "SELECT * FROM (SELECT CAST(scpDate AS DATETIME) AS scpDate,DSPAttemp,DSPFailed,DSPSuccess FROM v_rates WHERE scpCode =  '$pku1' ORDER BY scpDate DESC LIMIT 30)a ORDER BY scpDate ASC"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->categoriesdsp2 = array();
      if ($rx = $this->Db->Execute($nm_select)) 
      { 
          $y = 0; 
          $nm_count = $rx->FieldCount();
          while (!$rx->EOF)
          { 
                 for ($x = 0; $x < $nm_count; $x++)
                 { 
                        $this->categoriesdsp2[$y] [$x] = $rx->fields[$x];
                 }
                 $y++; 
                 $rx->MoveNext();
          } 
          $rx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->categoriesdsp2 = false;
          $this->categoriesdsp2_erro = $this->Db->ErrorMsg();
      } 
;

for($c=0;$c<count($this->categoriesdsp2 );$c++){
  $pku_dsp_a[] = array($this->categoriesdsp2[$c][0]);
  $pku_dsp_b[] = array($this->categoriesdsp2[$c][1]);
  $pku_dsp_c[] = array($this->categoriesdsp2[$c][2]);
  $pku_dsp_d[] = array($this->categoriesdsp2[$c][3]);  
}

$pku_dsp_1 = str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($pku_dsp_a))));

$pku_dsp_2 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($pku_dsp_b)))));

$pku_dsp_3 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($pku_dsp_c)))));

$pku_dsp_4 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($pku_dsp_d)))));


 
      $nm_select = "SELECT * FROM (SELECT CAST(scpDate AS DATETIME) AS scpDate,DSPAttemp,DSPFailed,DSPSuccess FROM v_rates WHERE scpCode =  '$sby1' ORDER BY scpDate DESC LIMIT 30)a ORDER BY scpDate ASC"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->categoriesdsp3 = array();
      if ($rx = $this->Db->Execute($nm_select)) 
      { 
          $y = 0; 
          $nm_count = $rx->FieldCount();
          while (!$rx->EOF)
          { 
                 for ($x = 0; $x < $nm_count; $x++)
                 { 
                        $this->categoriesdsp3[$y] [$x] = $rx->fields[$x];
                 }
                 $y++; 
                 $rx->MoveNext();
          } 
          $rx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->categoriesdsp3 = false;
          $this->categoriesdsp3_erro = $this->Db->ErrorMsg();
      } 
;

for($d=0;$d<count($this->categoriesdsp3 );$d++){
  $sby_dsp_a[] = array($this->categoriesdsp3[$d][0]);
  $sby_dsp_b[] = array($this->categoriesdsp3[$d][1]);
  $sby_dsp_c[] = array($this->categoriesdsp3[$d][2]);
  $sby_dsp_d[] = array($this->categoriesdsp3[$d][3]);  
}

$sby_dsp_1 = str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($sby_dsp_a))));

$sby_dsp_2 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($sby_dsp_b)))));

$sby_dsp_3 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($sby_dsp_c)))));

$sby_dsp_4 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($sby_dsp_d)))));

 
      $nm_select = "SELECT * FROM (SELECT CAST(scpDate AS DATETIME) AS scpDate,DSPAttemp,DSPFailed,DSPSuccess FROM v_rates WHERE scpCode =  '$tbs1' ORDER BY scpDate DESC LIMIT 30)a ORDER BY scpDate ASC"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->categoriesdsp4 = array();
      if ($rx = $this->Db->Execute($nm_select)) 
      { 
          $y = 0; 
          $nm_count = $rx->FieldCount();
          while (!$rx->EOF)
          { 
                 for ($x = 0; $x < $nm_count; $x++)
                 { 
                        $this->categoriesdsp4[$y] [$x] = $rx->fields[$x];
                 }
                 $y++; 
                 $rx->MoveNext();
          } 
          $rx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->categoriesdsp4 = false;
          $this->categoriesdsp4_erro = $this->Db->ErrorMsg();
      } 
;

for($e=0;$e<count($this->categoriesdsp4 );$e++){
  $tbs_dsp_a[] = array($this->categoriesdsp4[$e][0]);
  $tbs_dsp_b[] = array($this->categoriesdsp4[$e][1]);
  $tbs_dsp_c[] = array($this->categoriesdsp4[$e][2]);
  $tbs_dsp_d[] = array($this->categoriesdsp4[$e][3]);  
}

$tbs_dsp_1 = str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($tbs_dsp_a))));

$tbs_dsp_2 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($tbs_dsp_b)))));

$tbs_dsp_3 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($tbs_dsp_c)))));

$tbs_dsp_4 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($tbs_dsp_d)))));

 
      $nm_select = "SELECT * FROM (SELECT ScpDate,ATIAttemp,ATIFailed,ATISuccess FROM v_rates WHERE scpCode =  '$mks1' order by scpDate DESC LIMIT 30)a ORDER BY scpDate ASC"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->categoriesati1 = array();
      if ($rx = $this->Db->Execute($nm_select)) 
      { 
          $y = 0; 
          $nm_count = $rx->FieldCount();
          while (!$rx->EOF)
          { 
                 for ($x = 0; $x < $nm_count; $x++)
                 { 
                        $this->categoriesati1[$y] [$x] = $rx->fields[$x];
                 }
                 $y++; 
                 $rx->MoveNext();
          } 
          $rx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->categoriesati1 = false;
          $this->categoriesati1_erro = $this->Db->ErrorMsg();
      } 
;

for($b1=0;$b1<count($this->categoriesati1 );$b1++){
  $mks_ati_a[] = array($this->categoriesati1[$b1][0]);
  $mks_ati_b[] = array($this->categoriesati1[$b1][1]);
  $mks_ati_c[] = array($this->categoriesati1[$b1][2]);
  $mks_ati_d[] = array($this->categoriesati1[$b1][3]); 
}

$mks_ati_1 = str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($mks_ati_a))));

$mks_ati_2 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($mks_ati_b)))));

$mks_ati_3 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($mks_ati_c)))));

$mks_ati_4 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($mks_ati_d)))));

 
      $nm_select = "SELECT * FROM (SELECT ScpDate,ATIAttemp,ATIFailed,ATISuccess FROM v_rates WHERE scpCode =  '$pku1' order by scpDate DESC LIMIT 30)a ORDER BY scpDate ASC"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->categoriesati2 = array();
      if ($rx = $this->Db->Execute($nm_select)) 
      { 
          $y = 0; 
          $nm_count = $rx->FieldCount();
          while (!$rx->EOF)
          { 
                 for ($x = 0; $x < $nm_count; $x++)
                 { 
                        $this->categoriesati2[$y] [$x] = $rx->fields[$x];
                 }
                 $y++; 
                 $rx->MoveNext();
          } 
          $rx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->categoriesati2 = false;
          $this->categoriesati2_erro = $this->Db->ErrorMsg();
      } 
;

for($c1=0;$c1<count($this->categoriesati2 );$c1++){
  $pku_ati_a[] = array($this->categoriesati2[$c1][0]);
  $pku_ati_b[] = array($this->categoriesati2[$c1][1]);
  $pku_ati_c[] = array($this->categoriesati2[$c1][2]);
  $pku_ati_d[] = array($this->categoriesati2[$c1][3]); 
}

$pku_ati_1 = str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($pku_ati_a))));

$pku_ati_2 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($pku_ati_b)))));

$pku_ati_3 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($pku_ati_c)))));

$pku_ati_4 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($pku_ati_d)))));

 
      $nm_select = "SELECT * FROM (SELECT ScpDate,ATIAttemp,ATIFailed,ATISuccess FROM v_rates WHERE scpCode =  '$sby1' order by scpDate DESC LIMIT 30)a ORDER BY scpDate ASC"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->categoriesati3 = array();
      if ($rx = $this->Db->Execute($nm_select)) 
      { 
          $y = 0; 
          $nm_count = $rx->FieldCount();
          while (!$rx->EOF)
          { 
                 for ($x = 0; $x < $nm_count; $x++)
                 { 
                        $this->categoriesati3[$y] [$x] = $rx->fields[$x];
                 }
                 $y++; 
                 $rx->MoveNext();
          } 
          $rx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->categoriesati3 = false;
          $this->categoriesati3_erro = $this->Db->ErrorMsg();
      } 
;

for($d1=0;$d1<count($this->categoriesati3 );$d1++){
  $sby_ati_a[] = array($this->categoriesati3[$d1][0]);
  $sby_ati_b[] = array($this->categoriesati3[$d1][1]);
  $sby_ati_c[] = array($this->categoriesati3[$d1][2]);
  $sby_ati_d[] = array($this->categoriesati3[$d1][3]); 
}

$sby_ati_1 = str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($sby_ati_a))));

$sby_ati_2 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($sby_ati_b)))));

$sby_ati_3 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($sby_ati_c)))));

$sby_ati_4 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($sby_ati_d)))));

 
      $nm_select = "SELECT * FROM (SELECT ScpDate,ATIAttemp,ATIFailed,ATISuccess FROM v_rates WHERE scpCode =  '$tbs1' order by scpDate DESC LIMIT 30)a ORDER BY scpDate ASC"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->categoriesati4 = array();
      if ($rx = $this->Db->Execute($nm_select)) 
      { 
          $y = 0; 
          $nm_count = $rx->FieldCount();
          while (!$rx->EOF)
          { 
                 for ($x = 0; $x < $nm_count; $x++)
                 { 
                        $this->categoriesati4[$y] [$x] = $rx->fields[$x];
                 }
                 $y++; 
                 $rx->MoveNext();
          } 
          $rx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->categoriesati4 = false;
          $this->categoriesati4_erro = $this->Db->ErrorMsg();
      } 
;

for($e1=0;$e1<count($this->categoriesati4 );$e1++){
  $tbs_ati_a[] = array($this->categoriesati4[$e1][0]);
  $tbs_ati_b[] = array($this->categoriesati4[$e1][1]);
  $tbs_ati_c[] = array($this->categoriesati4[$e1][2]);
  $tbs_ati_d[] = array($this->categoriesati4[$e1][3]); 
}

$tbs_ati_1 = str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($tbs_ati_a))));

$tbs_ati_2 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($tbs_ati_b)))));

$tbs_ati_3 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($tbs_ati_c)))));

$tbs_ati_4 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($tbs_ati_d)))));

 
      $nm_select = "SELECT * FROM (SELECT ScpDate,BussinessAttemp,BussinessFailed,BussinessSuccess FROM v_rates WHERE scpCode =  '$mks1' order by scpDate DESC LIMIT 30)a ORDER BY scpDate ASC"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->categoriesbisnis1 = array();
      if ($rx = $this->Db->Execute($nm_select)) 
      { 
          $y = 0; 
          $nm_count = $rx->FieldCount();
          while (!$rx->EOF)
          { 
                 for ($x = 0; $x < $nm_count; $x++)
                 { 
                        $this->categoriesbisnis1[$y] [$x] = $rx->fields[$x];
                 }
                 $y++; 
                 $rx->MoveNext();
          } 
          $rx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->categoriesbisnis1 = false;
          $this->categoriesbisnis1_erro = $this->Db->ErrorMsg();
      } 
;

for($b2=0;$b2<count($this->categoriesbisnis1 );$b2++){
  $mks_bisnis_a[] = array($this->categoriesbisnis1[$b2][0]);
  $mks_bisnis_b[] = array($this->categoriesbisnis1[$b2][1]);
  $mks_bisnis_c[] = array($this->categoriesbisnis1[$b2][2]);
  $mks_bisnis_d[] = array($this->categoriesbisnis1[$b2][3]); 
}

$mks_bisnis_1 = str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($mks_bisnis_a))));

$mks_bisnis_2 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($mks_bisnis_b)))));

$mks_bisnis_3 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($mks_bisnis_c)))));

$mks_bisnis_4 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($mks_bisnis_d)))));

 
      $nm_select = "SELECT * FROM (SELECT ScpDate,BussinessAttemp,BussinessFailed,BussinessSuccess FROM v_rates WHERE scpCode =  '$pku1' order by scpDate DESC LIMIT 30)a ORDER BY scpDate ASC"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->categoriesbisnis2 = array();
      if ($rx = $this->Db->Execute($nm_select)) 
      { 
          $y = 0; 
          $nm_count = $rx->FieldCount();
          while (!$rx->EOF)
          { 
                 for ($x = 0; $x < $nm_count; $x++)
                 { 
                        $this->categoriesbisnis2[$y] [$x] = $rx->fields[$x];
                 }
                 $y++; 
                 $rx->MoveNext();
          } 
          $rx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->categoriesbisnis2 = false;
          $this->categoriesbisnis2_erro = $this->Db->ErrorMsg();
      } 
;

for($c2=0;$c2<count($this->categoriesbisnis2 );$c2++){
  $pku_bisnis_a[] = array($this->categoriesbisnis2[$c2][0]);
  $pku_bisnis_b[] = array($this->categoriesbisnis2[$c2][1]);
  $pku_bisnis_c[] = array($this->categoriesbisnis2[$c2][2]);
  $pku_bisnis_d[] = array($this->categoriesbisnis2[$c2][3]); 
}

$pku_bisnis_1 = str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($pku_bisnis_a))));

$pku_bisnis_2 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($pku_bisnis_b)))));

$pku_bisnis_3 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($pku_bisnis_c)))));

$pku_bisnis_4 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($pku_bisnis_d)))));

 
      $nm_select = "SELECT * FROM (SELECT ScpDate,BussinessAttemp,BussinessFailed,BussinessSuccess FROM v_rates WHERE scpCode =  '$sby1' order by scpDate DESC LIMIT 30)a ORDER BY scpDate ASC"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->categoriesbisnis3 = array();
      if ($rx = $this->Db->Execute($nm_select)) 
      { 
          $y = 0; 
          $nm_count = $rx->FieldCount();
          while (!$rx->EOF)
          { 
                 for ($x = 0; $x < $nm_count; $x++)
                 { 
                        $this->categoriesbisnis3[$y] [$x] = $rx->fields[$x];
                 }
                 $y++; 
                 $rx->MoveNext();
          } 
          $rx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->categoriesbisnis3 = false;
          $this->categoriesbisnis3_erro = $this->Db->ErrorMsg();
      } 
;

for($d2=0;$d2<count($this->categoriesbisnis3 );$d2++){
  $sby_bisnis_a[] = array($this->categoriesbisnis3[$d2][0]);
  $sby_bisnis_b[] = array($this->categoriesbisnis3[$d2][1]);
  $sby_bisnis_c[] = array($this->categoriesbisnis3[$d2][2]);
  $sby_bisnis_d[] = array($this->categoriesbisnis3[$d2][3]); 
}

$sby_bisnis_1 = str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($sby_bisnis_a))));

$sby_bisnis_2 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($sby_bisnis_b)))));

$sby_bisnis_3 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($sby_bisnis_c)))));

$sby_bisnis_4 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($sby_bisnis_d)))));


 
      $nm_select = "SELECT * FROM (SELECT ScpDate,BussinessAttemp,BussinessFailed,BussinessSuccess FROM v_rates WHERE scpCode =  '$tbs1' order by scpDate DESC LIMIT 30)a ORDER BY scpDate ASC"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->categoriesbisnis4 = array();
      if ($rx = $this->Db->Execute($nm_select)) 
      { 
          $y = 0; 
          $nm_count = $rx->FieldCount();
          while (!$rx->EOF)
          { 
                 for ($x = 0; $x < $nm_count; $x++)
                 { 
                        $this->categoriesbisnis4[$y] [$x] = $rx->fields[$x];
                 }
                 $y++; 
                 $rx->MoveNext();
          } 
          $rx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->categoriesbisnis4 = false;
          $this->categoriesbisnis4_erro = $this->Db->ErrorMsg();
      } 
;

for($e2=0;$e2<count($this->categoriesbisnis4 );$e2++){
  $tbs_bisnis_a[] = array($this->categoriesbisnis4[$e2][0]);
  $tbs_bisnis_b[] = array($this->categoriesbisnis4[$e2][1]);
  $tbs_bisnis_c[] = array($this->categoriesbisnis4[$e2][2]);
  $tbs_bisnis_d[] = array($this->categoriesbisnis4[$e2][3]); 
}

$tbs_bisnis_1 = str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($tbs_bisnis_a))));

$tbs_bisnis_2 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($tbs_bisnis_b)))));

$tbs_bisnis_3 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($tbs_bisnis_c)))));

$tbs_bisnis_4 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($tbs_bisnis_d)))));

 
      $nm_select = "SELECT * FROM (SELECT CAST(scpDate AS DATETIME) AS scpDate, tbl_scp.`GotAnnoCallAddict` AS aparty_attemp, tbl_scp.`APressOne` AS aparty_Success, (tbl_scp.`GotAnnoCallAddict` - tbl_scp.`APressOne`) AS aparty_failed, tbl_scp.`BoAnswer` AS bparty_attemp, tbl_scp.`BPressOne` AS bparty_Success, (tbl_scp.`BoAnswer` - tbl_scp.`BPressOne`) AS bparty_failed FROM tbl_scp 
WHERE scpCode =  '$mks1' ORDER BY scpDate DESC LIMIT 30)a 
ORDER BY scpDate ASC"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->categoriesparty_mks = array();
      if ($rx = $this->Db->Execute($nm_select)) 
      { 
          $y = 0; 
          $nm_count = $rx->FieldCount();
          while (!$rx->EOF)
          { 
                 for ($x = 0; $x < $nm_count; $x++)
                 { 
                        $this->categoriesparty_mks[$y] [$x] = $rx->fields[$x];
                 }
                 $y++; 
                 $rx->MoveNext();
          } 
          $rx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->categoriesparty_mks = false;
          $this->categoriesparty_mks_erro = $this->Db->ErrorMsg();
      } 
;

for($equi_mks=0;$equi_mks<count($this->categoriesparty_mks );$equi_mks++){
  $mks_party_a[] = array($this->categoriesparty_mks[$equi_mks][0]);
  $mks_party_b[] = array($this->categoriesparty_mks[$equi_mks][1]);
  $mks_party_c[] = array($this->categoriesparty_mks[$equi_mks][2]);
  $mks_party_d[] = array($this->categoriesparty_mks[$equi_mks][3]);
  $mks_party_e[] = array($this->categoriesparty_mks[$equi_mks][4]);
  $mks_party_f[] = array($this->categoriesparty_mks[$equi_mks][5]);
  $mks_party_g[] = array($this->categoriesparty_mks[$equi_mks][6]);  
}
$mks_party_1 = str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($mks_party_a))));
$mks_party_2 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($mks_party_b)))));
$mks_party_3 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($mks_party_c)))));
$mks_party_4 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($mks_party_d)))));
$mks_party_5 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($mks_party_e)))));
$mks_party_6 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($mks_party_f)))));
$mks_party_7 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($mks_party_g)))));


 
      $nm_select = "SELECT * FROM (SELECT CAST(scpDate AS DATETIME) AS scpDate, tbl_scp.`GotAnnoCallAddict` AS aparty_attemp, tbl_scp.`APressOne` AS aparty_Success, (tbl_scp.`GotAnnoCallAddict` - tbl_scp.`APressOne`) AS aparty_failed, tbl_scp.`BoAnswer` AS bparty_attemp, tbl_scp.`BPressOne` AS bparty_Success, (tbl_scp.`BoAnswer` - tbl_scp.`BPressOne`) AS bparty_failed FROM tbl_scp WHERE scpCode =  '$pku1' ORDER BY scpDate DESC LIMIT 30)a ORDER BY scpDate ASC"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->categoriesparty_pku = array();
      if ($rx = $this->Db->Execute($nm_select)) 
      { 
          $y = 0; 
          $nm_count = $rx->FieldCount();
          while (!$rx->EOF)
          { 
                 for ($x = 0; $x < $nm_count; $x++)
                 { 
                        $this->categoriesparty_pku[$y] [$x] = $rx->fields[$x];
                 }
                 $y++; 
                 $rx->MoveNext();
          } 
          $rx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->categoriesparty_pku = false;
          $this->categoriesparty_pku_erro = $this->Db->ErrorMsg();
      } 
;

for($equi_pku=0;$equi_pku<count($this->categoriesparty_pku );$equi_pku++){
  $pku_party_a[] = array($this->categoriesparty_pku[$equi_pku][0]);
  $pku_party_b[] = array($this->categoriesparty_pku[$equi_pku][1]);
  $pku_party_c[] = array($this->categoriesparty_pku[$equi_pku][2]);
  $pku_party_d[] = array($this->categoriesparty_pku[$equi_pku][3]);
  $pku_party_e[] = array($this->categoriesparty_pku[$equi_pku][4]);
  $pku_party_f[] = array($this->categoriesparty_pku[$equi_pku][5]);
  $pku_party_g[] = array($this->categoriesparty_pku[$equi_pku][6]);  
}

$pku_party_1 = str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($pku_party_a))));
$pku_party_2 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($pku_party_b)))));
$pku_party_3 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($pku_party_c)))));
$pku_party_4 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($pku_party_d)))));
$pku_party_5 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($pku_party_e)))));
$pku_party_6 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($pku_party_f)))));
$pku_party_7 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($pku_party_g)))));


 
      $nm_select = "SELECT * FROM (SELECT CAST(scpDate AS DATETIME) AS scpDate, tbl_scp.`GotAnnoCallAddict` AS aparty_attemp, tbl_scp.`APressOne` AS aparty_Success, (tbl_scp.`GotAnnoCallAddict` - tbl_scp.`APressOne`) AS aparty_failed, tbl_scp.`BoAnswer` AS bparty_attemp, tbl_scp.`BPressOne` AS bparty_Success, (tbl_scp.`BoAnswer` - tbl_scp.`BPressOne`) AS bparty_failed FROM tbl_scp WHERE scpCode =  '$sby1' ORDER BY scpDate DESC LIMIT 30)a ORDER BY scpDate ASC"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->categoriesparty_sby = array();
      if ($rx = $this->Db->Execute($nm_select)) 
      { 
          $y = 0; 
          $nm_count = $rx->FieldCount();
          while (!$rx->EOF)
          { 
                 for ($x = 0; $x < $nm_count; $x++)
                 { 
                        $this->categoriesparty_sby[$y] [$x] = $rx->fields[$x];
                 }
                 $y++; 
                 $rx->MoveNext();
          } 
          $rx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->categoriesparty_sby = false;
          $this->categoriesparty_sby_erro = $this->Db->ErrorMsg();
      } 
;

for($equi_sby=0;$equi_sby<count($this->categoriesparty_sby );$equi_sby++){
  $sby_party_a[] = array($this->categoriesparty_sby[$equi_sby][0]);
  $sby_party_b[] = array($this->categoriesparty_sby[$equi_sby][1]);
  $sby_party_c[] = array($this->categoriesparty_sby[$equi_sby][2]);
  $sby_party_d[] = array($this->categoriesparty_sby[$equi_sby][3]);
  $sby_party_e[] = array($this->categoriesparty_sby[$equi_sby][4]);
  $sby_party_f[] = array($this->categoriesparty_sby[$equi_sby][5]);
  $sby_party_g[] = array($this->categoriesparty_sby[$equi_sby][6]);  
}

$sby_party_1 = str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($sby_party_a))));
$sby_party_2 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($sby_party_b)))));
$sby_party_3 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($sby_party_c)))));
$sby_party_4 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($sby_party_d)))));
$sby_party_5 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($sby_party_e)))));
$sby_party_6 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($sby_party_f)))));
$sby_party_7 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($sby_party_g)))));


 
      $nm_select = "SELECT * FROM 
(SELECT CAST(scpDate AS DATETIME) AS scpDate, 
tbl_scp.`GotAnnoCallAddict` AS aparty_attemp, 
tbl_scp.`APressOne` AS aparty_Success, 
(tbl_scp.`GotAnnoCallAddict` - tbl_scp.`APressOne`) AS aparty_failed, 
tbl_scp.`BoAnswer` AS bparty_attemp, 
tbl_scp.`BPressOne` AS bparty_Success, 
(tbl_scp.`BoAnswer` - tbl_scp.`BPressOne`) AS bparty_failed 
FROM tbl_scp WHERE scpCode =  '$tbs1' ORDER BY scpDate DESC LIMIT 30)a ORDER BY scpDate ASC"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->categoriesparty_tbs = array();
      if ($rx = $this->Db->Execute($nm_select)) 
      { 
          $y = 0; 
          $nm_count = $rx->FieldCount();
          while (!$rx->EOF)
          { 
                 for ($x = 0; $x < $nm_count; $x++)
                 { 
                        $this->categoriesparty_tbs[$y] [$x] = $rx->fields[$x];
                 }
                 $y++; 
                 $rx->MoveNext();
          } 
          $rx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->categoriesparty_tbs = false;
          $this->categoriesparty_tbs_erro = $this->Db->ErrorMsg();
      } 
;

for($equi_tbs=0;$equi_tbs<count($this->categoriesparty_tbs );$equi_tbs++){
  $tbs_party_a[] = array($this->categoriesparty_tbs[$equi_tbs][0]);
  $tbs_party_b[] = array($this->categoriesparty_tbs[$equi_tbs][1]);
  $tbs_party_c[] = array($this->categoriesparty_tbs[$equi_tbs][2]);
  $tbs_party_d[] = array($this->categoriesparty_tbs[$equi_tbs][3]);
  $tbs_party_e[] = array($this->categoriesparty_tbs[$equi_tbs][4]);
  $tbs_party_f[] = array($this->categoriesparty_tbs[$equi_tbs][5]);
  $tbs_party_g[] = array($this->categoriesparty_tbs[$equi_tbs][6]);  
}

$tbs_party_1 = str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($tbs_party_a))));
$tbs_party_2 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($tbs_party_b)))));
$tbs_party_3 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($tbs_party_c)))));
$tbs_party_4 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($tbs_party_d)))));
$tbs_party_5 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($tbs_party_e)))));
$tbs_party_6 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($tbs_party_f)))));
$tbs_party_7 = str_replace('"','',str_replace("],[",",",str_replace("]]","",str_replace("[[","",json_encode($tbs_party_g)))));


?>

<meta charset="UTF-8">
<meta http-equiv="refresh" content="1000" >
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<style type="text/css">
  @font-face {
      font-family: "clock";
      src: url("<?php echo sc_url_library('public','font','digital-7.ttf');?>");
    }
</style>
<style>
body{
  margin:auto;
  background-image:url('http://10.251.166.161:8090/scriptcase/devel/conf/sys/img/img/telkom2.png?1185716997');
  background-repeat: no-repeat;
  background-size: 100% 100%;
  text-align:center;
  margin:6px;
}

.grid-container {
  display: grid;
  grid-template-columns: 450px 450px 450px;
  grid-template-rows: 340px 340px;
  grid-template-areas: "area-1 area-2 area-3" "area-4 area-5 aeaa-6";
}

.area-1 {
  display: grid;
  margin:2px;
  grid-template-columns: 250px 194px;
  grid-template-rows: 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px;
  grid-template-areas: "header-area-1 header-area-1" "ati-area-1 success-ati-area-1" "ati-area-1 failed-ati-area-1" "ati-area-1 attemp-ati-area-1" "dsp-area-1 success-dsp-area-1" "dsp-area-1 failed-dsp-area-1" "dsp-area-1 attemp-dsp-area-1" "aparty-area-1 success-aparty-area-1" "aparty-area-1 failed-aparty-area-1" "aparty-area-1 attemp-aparty-area-1" "bparty-area-1 success-bparty-area-1" "bparty-area-1 failed-bparty-area-1" "bparty-area-1 attemp-bparty-area-1" "business-area-1 succes-buisness-area-1" "business-area-1 failed-business-area-1" "business-area-1 attemp-business-area-1" "footer-area-1 footer-area-1";
  grid-area: area-1;
  background-color: #444444;
  border-top:3px solid #a5a5a5;
  border:0.5px solid #a5a5a5;
  border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px;
}

.header-area-1 { 
  grid-area: header-area-1;
  color: #dbdcdd;
  background:linear-gradient(to right, #427a91 90%, black 100%);
  margin-bottom: 1px;
  width:100px;
  margin-left: 342px;
  border-bottom-left-radius: 110px;
  border: 1px solid #a5a5a5;
  border-top: 0;
  font-size:12pt;
  font-family:fantasy; }

.footer-area-1 { 
  grid-area: footer-area-1;
  font-family:arial;
  color:#d8d8d8;
  font-size:7.8pt;
  padding:2px;
  cursor:pointer; }

.attemp-business-area-1 { 
  grid-area: attemp-business-area-1;
  background:#6aa7c1; }

.failed-business-area-1 { 
  grid-area: failed-business-area-1;
  background:#5d9eba; }

.succes-buisness-area-1 { 
  grid-area: succes-buisness-area-1;
  background-color: #5290aa; }

.business-area-1 { 
  grid-area: business-area-1;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background: <?php echo $mks_bisnis;?>;
  }

.attemp-bparty-area-1 { 
  grid-area: attemp-bparty-area-1;
  background:#6aa7c1; }

.failed-bparty-area-1 { 
  grid-area: failed-bparty-area-1;
  background:#5d9eba; }

.success-bparty-area-1 { 
  grid-area: success-bparty-area-1;
  background-color: #5290aa; }

.bparty-area-1 { 
  grid-area: bparty-area-1;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background:<?php echo $mks_bparty;?>; }

.attemp-aparty-area-1 { 
  grid-area: attemp-aparty-area-1;
  background:#6aa7c1; }

.failed-aparty-area-1 { 
  grid-area: failed-aparty-area-1;
  background:#5d9eba; }

.success-aparty-area-1 { 
  grid-area: success-aparty-area-1;
  background-color: #5290aa; }

.aparty-area-1 { 
  grid-area: aparty-area-1;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background:<?php echo $mks_aparty;?>; }

.attemp-dsp-area-1 { 
  grid-area: attemp-dsp-area-1;
  background:#6aa7c1; }

.failed-dsp-area-1 { 
  grid-area: failed-dsp-area-1;
  background:#5d9eba; }

.success-dsp-area-1 { 
  grid-area: success-dsp-area-1;
  background-color: #5290aa; }

.dsp-area-1 { 
  grid-area: dsp-area-1;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background: <?php echo $mks_dsp;?>; }

.attemp-ati-area-1 { 
  grid-area: attemp-ati-area-1;
  background:#6aa7c1; }

.failed-ati-area-1 { 
  grid-area: failed-ati-area-1;
  background:#5d9eba; }

.success-ati-area-1 { 
  grid-area: success-ati-area-1;
  background-color: #5290aa; }

.ati-area-1 { 
  grid-area: ati-area-1;
  background: <?php echo $mks_ati;?>;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
}

.area-2 {
  display: grid;
  margin:2px;
  grid-template-columns: 250px 194px;
  grid-template-rows: 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px;
  grid-template-areas: "header-area-2 header-area-2" "ati-area-2 success-ati-area-2" "ati-area-2 failed-ati-area-2" "ati-area-2 attemp-ati-area-2" "dsp-area-2 success-dsp-area-2" "dsp-area-2 failed-dsp-area-2" "dsp-area-2 attemp-dsp-area-2" "aparty-area-2 success-aparty-area-2" "aparty-area-2 failed-aparty-area-2" "aparty-area-2 attemp-aparty-area-2" "bparty-area-2 success-bparty-area-2" "bparty-area-2 failed-bparty-area-2" "bparty-area-2 attemp-bparty-area-2" "business-area-2 success-business-area-2" "business-area-2 failed-business-area-2" "business-area-2 attemp-business-area-2" "footer-area-2 footer-area-2";
  grid-area: area-2;
  background-color: #444444;
  border-top:3px solid #a5a5a5;
  border:0.5px solid #a5a5a5;
  border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px;
}

.header-area-2 { 
  grid-area: header-area-2;
  color: #dbdcdd;
  background:linear-gradient(to right, #427a91 90%, black 100%);
  margin-bottom: 1px;
  width:100px;
  margin-left: 342px;
  border-bottom-left-radius: 110px;
  border: 1px solid #a5a5a5;
  border-top: 0;
  font-size:12pt;
  font-family:fantasy; }

.footer-area-2 { 
  grid-area: footer-area-2;
  font-family:arial;
  color:#d8d8d8;
  font-size:7.8pt;
  padding:2px;
  cursor:pointer; }

.attemp-business-area-2 { 
  grid-area: attemp-business-area-2;
  background:#76b2cc; }

.failed-business-area-2 { 
  grid-area: failed-business-area-2;
  background:#5d9eba; }

.success-business-area-2 { 
  grid-area: success-business-area-2;
  background-color: #5290aa; }

.business-area-2 { 
  grid-area: business-area-2;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background: <?php echo $pku_bisnis;?>; }

.attemp-bparty-area-2 { 
  grid-area: attemp-bparty-area-2;
  background:#6aa7c1; }

.failed-bparty-area-2 { 
  grid-area: failed-bparty-area-2;
  background:#5d9eba; }

.success-bparty-area-2 { 
  grid-area: success-bparty-area-2;
  background-color: #5290aa; }

.bparty-area-2 { 
  grid-area: bparty-area-2;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background:<?php echo $pku_bparty;?>; }

.attemp-aparty-area-2 { 
  grid-area: attemp-aparty-area-2;
  background:#6aa7c1; }

.failed-aparty-area-2 { 
  grid-area: failed-aparty-area-2;
  background:#5d9eba; }

.success-aparty-area-2 { 
  grid-area: success-aparty-area-2;
  background-color: #5290aa; }

.aparty-area-2 { 
  grid-area: aparty-area-2;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background:<?php echo $pku_aparty;?>; }

.attemp-dsp-area-2 { 
  grid-area: attemp-dsp-area-2;
  background:#6aa7c1; }

.failed-dsp-area-2 { 
  grid-area: failed-dsp-area-2;
  background:#5d9eba; }

.success-dsp-area-2 { 
  grid-area: success-dsp-area-2;
  background-color: #5290aa; }

.dsp-area-2 { 
  grid-area: dsp-area-2;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background: <?php echo $pku_dsp;?>; }

.attemp-ati-area-2 { 
  grid-area: attemp-ati-area-2;
  background:#6aa7c1; }

.failed-ati-area-2 { 
  grid-area: failed-ati-area-2;
  background:#5d9eba; }

.success-ati-area-2 { 
  grid-area: success-ati-area-2;
  background-color: #5290aa; }

.ati-area-2 { 
  grid-area: ati-area-2;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background: <?php echo $pku_ati;?>; }

.area-3 {
  display: grid;
  margin:2px;
  grid-template-columns: 250px 194px;
  grid-template-rows: 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px;
  grid-template-areas: "header-area-3 header-area-3" "ati-area-3 success-ati-area-3" "ati-area-3 failed-ati-area-3" "ati-area-3 attemp-ati-area-3" "dsp-area-3 success-dsp-area-3" "dsp-area-3 failed-dsp-area-3" "dsp-area-3 attemp-dsp-area-3" "aparty-area-3 success-aparty-area-3" "aparty-area-3 failed-aparty-area-3" "aparty-area-3 attemp-aparty-area-3" "bparty-area-3 success-bparty-area-3" "bparty-area-3 failed-bparty-area-3" "bparty-area-3 attemp-bparty-area-3" "business-area-3 success-business-area-3" "business-area-3 failed-business-area-3" "business-area-3 attemp-business-area-3" "footer-area-3 footer-area-3";
  grid-area: area-3;
  background-color: #444444;
  border-top:3px solid #a5a5a5;
  border:0.5px solid #a5a5a5;
  border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px;
}

.header-area-3 { 
  grid-area: header-area-3;
  color: #dbdcdd;
  background:linear-gradient(to right, #427a91 90%, black 100%);
  margin-bottom: 1px;
  width:100px;
  margin-left: 342px;
  border-bottom-left-radius: 110px;
  border: 1px solid #a5a5a5;
  border-top: 0;
  font-size:12pt;
  font-family:fantasy; }

.footer-area-3 { 
  grid-area: footer-area-3;
  font-family:arial;
  color:#d8d8d8;
  font-size:7.8pt;
  padding:2px;
  cursor:pointer; }

.attemp-business-area-3 { 
  grid-area: attemp-business-area-3;
  background:#6aa7c1; }

.failed-business-area-3 { 
  grid-area: failed-business-area-3;
  background:#5d9eba; }

.success-business-area-3 { 
  grid-area: success-business-area-3;
  background-color: #5290aa; }

.business-area-3 { 
  grid-area: business-area-3;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background: <?php echo $sby_bisnis;?>; }

.attemp-bparty-area-3 { 
  grid-area: attemp-bparty-area-3;
  background:#6aa7c1; }

.failed-bparty-area-3 { 
  grid-area: failed-bparty-area-3;
  background:#5d9eba; }

.success-bparty-area-3 { 
  grid-area: success-bparty-area-3;
  background-color: #5290aa; }

.bparty-area-3 { 
  grid-area: bparty-area-3;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background:<?php echo $sby_bparty;?>; }

.attemp-aparty-area-3 { 
  grid-area: attemp-aparty-area-3;
  background:#6aa7c1; }

.failed-aparty-area-3 { 
  grid-area: failed-aparty-area-3;
  background:#5d9eba; }

.success-aparty-area-3 { 
  grid-area: success-aparty-area-3;
  background-color: #5290aa; }

.aparty-area-3 { 
  grid-area: aparty-area-3;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background:<?php echo $sby_aparty;?>; }

.attemp-dsp-area-3 { 
  grid-area: attemp-dsp-area-3;
  background:#6aa7c1; }

.failed-dsp-area-3 { 
  grid-area: failed-dsp-area-3;
  background:#5d9eba; }

.success-dsp-area-3 { 
  grid-area: success-dsp-area-3;
  background-color: #5290aa; }

.dsp-area-3 { 
  grid-area: dsp-area-3;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background: <?php echo $sby_dsp;?>; }

.attemp-ati-area-3 { 
  grid-area: attemp-ati-area-3;
  background:#6aa7c1; }

.failed-ati-area-3 { 
  grid-area: failed-ati-area-3;
  background:#5d9eba; }

.success-ati-area-3 { 
  grid-area: success-ati-area-3;
  background-color: #5290aa; }

.ati-area-3 { 
  grid-area: ati-area-3;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background: <?php echo $sby_ati;?>; }

.area-4 {
  display: grid;
  margin:2px;
  grid-template-columns: 250px 194px;
  grid-template-rows: 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px;
  grid-template-areas: "header-area-4 header-area-4" "ati-area-4 success-ati-area-4" "ati-area-4 failed-ati-area-4" "ati-area-4 attemp-ati-area-4" "dsp-area-4 success-dsp-area-4" "dsp-area-4 failed-dsp-area-4" "dsp-area-4 attemp-dsp-area-4" "aparty-area-4 success-aparty-area-4" "aparty-area-4 failed-aparty-area-4" "aparty-area-4 attemp-aparty-area-4" "bparty-area-4 success-bparty-area-4" "bparty-area-4 failed-bparty-area-4" "bparty-area-4 attemp-bparty-area-4" "business-area-4 success-business-area-4" "business-area-4 failed-business-area-5" "business-area-4 attemp-business-area-4" "footer-area-4 footer-area-4";
  grid-area: area-4;
  background-color: #444444;
  border-top:3px solid #a5a5a5;
  border:0.5px solid #a5a5a5;
  border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px;
}

.footer-area-4 { 
  grid-area: footer-area-4;
  font-family:arial;
  color:#d8d8d8;
  font-size:7.8pt;
  padding:2px;
  cursor:pointer;
 }

.header-area-4 { 
  grid-area: header-area-4;
  width:100px;
  margin-left: 342px;
  border-bottom-left-radius: 110px;
  border: 1px solid #a5a5a5;
  border-top: 0;
  font-size:12pt;
  color: #dbdcdd;
  background:linear-gradient(to right, #427a91 90%, black 100%);
  margin-bottom: 1px;
  font-family:fantasy; }

.success-ati-area-4 {
  grid-area: success-ati-area-4;
  background-color: #5290aa;  }

.failed-ati-area-4 { 
  grid-area: failed-ati-area-4;
  background:#5d9eba; }

.attemp-ati-area-4 { 
  grid-area: attemp-ati-area-4;
  background:#6aa7c1; }

.ati-area-4 { 
  grid-area: ati-area-4;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background: <?php echo $tbs_ati;?>; }

.success-dsp-area-4 { 
  grid-area: success-dsp-area-4;
  background-color: #5290aa; }

.failed-dsp-area-4 { 
  grid-area: failed-dsp-area-4;
  background:#5d9eba; }

.attemp-dsp-area-4 { 
  grid-area: attemp-dsp-area-4;
  background:#6aa7c1; }

.dsp-area-4 { 
  grid-area: dsp-area-4;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background-color: #427a91; }

.success-aparty-area-4 { 
  grid-area: success-aparty-area-4;
  background-color: #5290aa; }

.failed-aparty-area-4 { 
  grid-area: failed-aparty-area-4;
  background:#5d9eba; }

.attemp-aparty-area-4 { 
  grid-area: attemp-aparty-area-4;
  background:#6aa7c1; }

.aparty-area-4 { 
  grid-area: aparty-area-4;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background:<?php echo $tbs_aparty;?>; }

.success-bparty-area-4 { 
  grid-area: success-bparty-area-4;
  background-color: #5290aa; }

.failed-bparty-area-4 { 
  grid-area: failed-bparty-area-4;
  background:#5d9eba; }

.attemp-bparty-area-4 { 
  grid-area: attemp-bparty-area-4;
  background:#6aa7c1; }

.bparty-area-4 { 
  grid-area: bparty-area-4;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background:<?php echo $tbs_bparty;?>; }

.success-business-area-4 { 
  grid-area: success-business-area-4;
  background-color: #5290aa; }

.failed-business-area-5 { 
  grid-area: failed-business-area-5;
   background:#5d9eba;}

.attemp-business-area-4 { 
  grid-area: attemp-business-area-4;
  background:#6aa7c1; }

.business-area-4 { 
  grid-area: business-area-4;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background: <?php echo $tbs_bisnis;?>; }

.area-5 {
  display: grid;
  margin:2px;
  grid-template-columns: 250px 194px;
  grid-template-rows: 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px;
  grid-template-areas: "header-area-5 header-area-5" "ati-area-5 success-ati-area-5" "ati-area-5 failed-ati-area-5" "ati-area-5 attemp-ati-area-5" "dsp-area-5 success-dsp-area-5" "dsp-area-5 failed-area-5" "dsp-area-5 attemp-area-5" "aparty-area-5 success-aparty-area-5" "aparty-area-5 failed-aparty-area-5" "aparty-area-5 attemp-aparty-area-5" "bparty-area-5 success-bparty-area-5" "bparty-area-5 failed-bparty-area-5" "bparty-area-5 attemp-bparty-area-5" "business-area-5 success-business-area-5" "business-area-5 failed-business-area-5" "business-area-5 attemp-business-area-5" "footer-area-5 footer-area-5";
  grid-area: area-5;
  background-color: #444444;
  border-top:3px solid #a5a5a5;
  border:0.5px solid #a5a5a5;
  border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px;
}

.header-area-5 { 
  grid-area: header-area-5;
  color: #dbdcdd;
  background:linear-gradient(to right, #427a91 90%, black 100%);
  margin-bottom: 1px;
  width:100px;
  margin-left: 342px;
  border-bottom-left-radius: 110px;
  border: 1px solid #a5a5a5;
  border-top: 0;
  font-size:12pt;
  font-family:fantasy; }

.success-ati-area-5 { 
  grid-area: success-ati-area-5;
  background-color: #5290aa; }

.failed-ati-area-5 { 
  grid-area: failed-ati-area-5;
  background:#5d9eba; }

.attemp-ati-area-5 { 
  grid-area: attemp-ati-area-5;
  background:#6aa7c1; }

.ati-area-5 { 
  grid-area: ati-area-5;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background-color: #427a91; }

.footer-area-5 { 
  grid-area: footer-area-5;
  font-family:arial;
  color:#d8d8d8;
  font-size:7.8pt;
  padding:2px;
  cursor:pointer; }

.success-dsp-area-5 { 
  grid-area: success-dsp-area-5;
  background-color: #5290aa; }

.failed-area-5 { 
  grid-area: failed-area-5;
  background:#5d9eba; }

.attemp-area-5 { 
  grid-area: attemp-area-5;
  background:#6aa7c1; }

.dsp-area-5 { 
  grid-area: dsp-area-5;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background-color: #427a91; }

.success-aparty-area-5 { 
  grid-area: success-aparty-area-5;
  background-color: #5290aa; }

.failed-aparty-area-5 { 
  grid-area: failed-aparty-area-5;
  background:#5d9eba; }

.attemp-aparty-area-5 { 
  grid-area: attemp-aparty-area-5;
  background:#6aa7c1; }

.aparty-area-5 { 
  grid-area: aparty-area-5;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background-color: #427a91; }

.success-bparty-area-5 { 
  grid-area: success-bparty-area-5;
  background-color: #5290aa; }

.failed-bparty-area-5 { 
  grid-area: failed-bparty-area-5;
  background:#5d9eba; }

.attemp-bparty-area-5 { 
  grid-area: attemp-bparty-area-5;
  background:#6aa7c1; }

.bparty-area-5 { 
  grid-area: bparty-area-5;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background-color: #427a91; }

.success-business-area-5 { 
  grid-area: success-business-area-5;
  background-color: #5290aa; }

.failed-business-area-5 { 
  grid-area: failed-business-area-5;
  background:#5d9eba; }

.attemp-business-area-5 { 
  grid-area: attemp-business-area-5;
  background:#6aa7c1; }

.business-area-5 { 
  grid-area: business-area-5;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background-color: #427a91; }

.aeaa-6 {
  display: grid;
  margin:2px;
  grid-template-columns: 250px 194px;
  grid-template-rows: 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px 20px;
  grid-template-areas: "header-area-6 header-area-6" "ati-area-6 success-ati-area-6" "ati-area-6 failed-ati-area-6" "ati-area-6 attempt-ati-area-6" "dsp-area-6 success-dsp-area-6" "dsp-area-6 failed-dsp-area-6" "dsp-area-6 attemp-dsp-area-6" "aparty-area-6 success-aparty-area-6" "aparty-area-6 failed-aparty-area-6" "aparty-area-6 attempt-aparty-area-6" "bparty-area-6 success-bparty-area-6" "bparty-area-6 failed-bparty-area-6" "bparty-area-6 attempt-bparty-area-6" "business-area-6 success-business-area-6" "business-area-6 failed-business-area-6" "business-area-6 attempt-business-area-6" "footer-area-6 footer-area-6";
  grid-area: aeaa-6;
  background-color: #444444;
  border-top:3px solid #a5a5a5;
  border:0.5px solid #a5a5a5;
  border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px;
}

.header-area-6 { 
  grid-area: header-area-6;
  width:100px;
  margin-left: 342px;
  border-bottom-left-radius: 110px;
  border: 1px solid #a5a5a5;
  border-top: 0;
  font-size:12pt;
  color: #dbdcdd;
  background:linear-gradient(to right, #427a91 90%, black 100%);
  margin-bottom: 1px;
  font-family:fantasy; }

.success-ati-area-6 { 
  grid-area: success-ati-area-6;
  background-color: #5290aa; }

.failed-ati-area-6 { 
  grid-area: failed-ati-area-6;
  background:#5d9eba; }

.attempt-ati-area-6 { 
  grid-area: attempt-ati-area-6;
  background:#6aa7c1; }

.ati-area-6 { 
  grid-area: ati-area-6;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background-color: #427a91; }

.success-dsp-area-6 { 
  grid-area: success-dsp-area-6;
  background-color: #5290aa; }

.failed-dsp-area-6 { 
  grid-area: failed-dsp-area-6;
  background:#5d9eba; }

.attemp-dsp-area-6 {
 grid-area: attemp-dsp-area-6;
 background:#6aa7c1; }

.dsp-area-6 { 
  grid-area: dsp-area-6;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background-color: #427a91; }

.success-aparty-area-6 { 
  grid-area: success-aparty-area-6;
  background-color: #5290aa; }

.failed-aparty-area-6 { 
  grid-area: failed-aparty-area-6;
  background:#5d9eba; }

.attempt-aparty-area-6 { 
  grid-area: attempt-aparty-area-6;
   background:#6aa7c1;}

.aparty-area-6 { 
  grid-area: aparty-area-6;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background-color: #427a91; }

.success-bparty-area-6 { 
  grid-area: success-bparty-area-6;
  background-color: #5290aa; }

.failed-bparty-area-6 { 
  grid-area: failed-bparty-area-6;
  background:#5d9eba; }

.attempt-bparty-area-6 { 
  grid-area: attempt-bparty-area-6;
  background:#6aa7c1; }

.success-business-area-6 { 
  grid-area: success-business-area-6;
  background-color: #5290aa; }

.failed-business-area-6 { 
  grid-area: failed-business-area-6;
  background:#5d9eba; }

.attempt-business-area-6 { 
  grid-area: attempt-business-area-6;
  background:#6aa7c1; }

.footer-area-6 { 
  grid-area: footer-area-6;
  font-family:arial;
  color:#d8d8d8;
  font-size:7.8pt;
  padding:2px;
  cursor:pointer; }

.bparty-area-6 { 
  grid-area: bparty-area-6;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background-color: #427a91; }

.business-area-6 { 
  grid-area: business-area-6;
  border-right: 2px solid #a5a5a5;
  border-bottom: 2px solid #a5a5a5;
  background-color: #427a91; }  

.modalupd1,
.modalupd2,
.modalupd3,
.modalupd4,
.modalupd5,
.modalupd6 {
  display: none; 
  position: fixed; 
  z-index: 1; 
  padding-top: 10px; 
  left: 0;
  top: 0;
  width: 100%; 
  height: 100%; 
  overflow: auto; 
  background-color: rgb(0,0,0); 
  background-color: rgba(0,0,0,0.4); 
}
  
.modal-content-upd1,
.modal-content-upd2,
.modal-content-upd3,
.modal-content-upd4,
.modal-content-upd5,
.modal-content-upd6 {
  background-color: #262626;
  margin: auto;
  padding: 10px;
  border: 1px solid #888;
  width: 40%;
}

.close-upd1,
.close-upd2,
.close-upd3,
.close-upd4,
.close-upd5,
.close-upd6 {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close-upd1:hover,
.close-upd1:focus,
.close-upd2:hover,
.close-upd2:focus,
.close-upd3:hover,
.close-upd3:focus,
.close-upd4:hover,
.close-upd4:focus,
.close-upd5:hover,
.close-upd5:focus,
.close-upd6:hover,
.close-upd6:focus {
  color: #4d8ea8;
  text-decoration: none;
  cursor: pointer;
}
  
.modaldsp1,
.modaldsp2,
.modaldsp3,
.modaldsp4,
.modaldsp5,
.modaldsp6,
.modalbp1,
.modalbp2,
.modalbp3,
.modalbp4,
.modalbp5,
.modalbp6,
.modalap1,
.modalap2,
.modalap3,
.modalap4,
.modalap5,
.modalap6,
.modalbisnis1,
.modalbisnis2,
.modalbisnis3,
.modalbisnis4,
.modalbisnis5,
.modalbisnis6,
.modalati1,
.modalati2,
.modalati3,
.modalati4,
.modalati5,
.modalati6 {
  display: none; 
  position: fixed; 
  z-index: 1; 
  padding-top: 100px; 
  left: 0;
  top: 0;
  width: 100%; 
  height: 100%; 
  overflow: auto; 
  background-color: rgb(0,0,0); 
  background-color: rgba(0,0,0,0.4); 
}
  

.modal-content-dsp1,
.modal-content-dsp2,
.modal-content-dsp3,
.modal-content-dsp4,
.modal-content-dsp5, 
.modal-content-dsp6,
.modal-content-bp1,
.modal-content-bp2,
.modal-content-bp3,
.modal-content-bp4,
.modal-content-bp5, 
.modal-content-bp6,
.modal-content-ap1,
.modal-content-ap2,
.modal-content-ap3,
.modal-content-ap4,
.modal-content-ap5, 
.modal-content-ap6,
.modal-content-bisnis1,
.modal-content-bisnis2,
.modal-content-bisnis3,
.modal-content-bisnis4,
.modal-content-bisnis5,
.modal-content-bisnis6,  
.modal-content-ati1,
.modal-content-ati2,
.modal-content-ati3,
.modal-content-ati4,
.modal-content-ati5,
.modal-content-ati6 {
  background-color: #202020;
  margin: auto;
  padding: 20px;
  border: 1px solid #888;
  width: 80%;
}

.close-dsp1,
.close-dsp2,
.close-dsp3,
.close-dsp4,
.close-dsp5,
.close-dsp6,
.close-bp1,
.close-bp2,
.close-bp3,
.close-bp4,
.close-bp5,
.close-bp6,
.close-ap1,
.close-ap2,
.close-ap3,
.close-ap4,
.close-ap5,
.close-ap6,
.close-bisnis1,
.close-bisnis2,
.close-bisnis3,
.close-bisnis4,
.close-bisnis5,
.close-bisnis6,
.close-ati1,
.close-ati2,
.close-ati3,
.close-ati4,
.close-ati5,
.close-ati6 {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close-dsp1:hover,
.close-dsp1:focus,
.close-dsp2:hover,
.close-dsp2:focus,
.close-dsp3:hover,
.close-dsp3:focus,
.close-dsp4:hover,
.close-dsp4:focus,
.close-dsp5:hover,
.close-dsp5:focus,
.close-dsp6:hover,
.close-dsp6:focus,
.close-bp1:hover,
.close-bp1:focus,
.close-bp2:hover,
.close-bp2:focus,
.close-bp3:hover,
.close-bp3:focus,
.close-bp4:hover,
.close-bp4:focus,
.close-bp5:hover,
.close-bp5:focus,
.close-bp6:hover,
.close-bp6:focus,
.close-ap1:hover,
.close-ap1:focus,
.close-ap2:hover,
.close-ap2:focus,
.close-ap3:hover,
.close-ap3:focus,
.close-ap4:hover,
.close-ap4:focus,
.close-ap5:hover,
.close-ap5:focus,
.close-ap6:hover,
.close-ap6:focus,
.close-bisnis1:hover,
.close-bisnis1:focus,
.close-bisnis2:hover,
.close-bisnis2:focus,
.close-bisnis3:hover,
.close-bisnis3:focus,
.close-bisnis4:hover,
.close-bisnis4:focus,
.close-bisnis5:hover,
.close-bisnis5:focus,
.close-bisnis6:hover,
.close-bisnis6:focus,
.close-ati1:hover,
.close-ati1:focus,
.close-ati2:hover,
.close-ati2:focus,
.close-ati3:hover,
.close-ati3:focus,
.close-ati4:hover,
.close-ati4:focus,
.close-ati5:hover,
.close-ati5:focus,
.close-ati6:hover,
.close-ati6:focus {
  color: #4d8ea8;
  text-decoration: none;
  cursor: pointer;
}


table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 0px solid #ddd;
  font-family:arial;
  color:#f2f2f2;
}

th, td {
  text-align: left;

}

tr:nth-child(even) {
  background-color: #565656;
}
    
.blink {
    -webkit-animation: blink 1000ms step-end infinite;
            animation: blink 1000ms step-end infinite;
    padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;
}

    
@-webkit-keyframes blink { 50% { padding: 10px;margin-right:35px; color:red; font-size: 28pt; font-family:clock;cursor:pointer; }}
        @keyframes blink { 50% { padding: 10px;margin-right:35px; color:red; font-size: 28pt; font-family:clock;cursor:pointer; }}

</style>

<div class="grid-container">
  
    <script>
    Highcharts.setOptions({
        lang: {
            numericSymbols: null
        }
    });
    </script>  

  <div class="area-1">
    <div class="header-area-1">
      <?php echo $this->ds[0][1];?>
    </div>
    <div class="footer-area-1" id="btnupdate1">Last Update <?php echo $this->ds[0][14];?> &#8505;</div>
    
      <div id="modalupdate1" class="modalupd1">
          <div class="modal-content-upd1">
            <span class="close-upd1">&times;</span>
            <p style="text-align:left; color:#f2f2f2; font-family:arial;"><?php echo $this->ds[0][1];?> Call Addict Data Detail</p>
          <table>
            <tr>
              <td>ID SCP</td>
              <td><?php echo $this->mks[0][0];?></td>
            </tr>
            <tr>
              <td>SCP Code</td>
              <td><?php echo $this->mks[0][1];?></td>
            </tr>
            <tr>
              <td>Got Anno Call Addict</td>
              <td><?php echo $this->mks[0][2];?></td>
            </tr>
            <tr>
              <td>Max Daily Threshold</td>
              <td><?php echo $this->mks[0][3];?></td>
            </tr>
            <tr>
              <td>A Press End Call</td>
              <td><?php echo $this->mks[0][4];?></td>
            </tr>
            <tr>
              <td>A Press Not One</td>
              <td><?php echo $this->mks[0][5];?></td>
            </tr>
            <tr>
              <td>A Press One</td>
              <td><?php echo $this->mks[0][6];?></td>
            </tr>
            <tr>
              <td>Query DSP Success</td>
              <td><?php echo $this->mks[0][7];?></td>
            </tr>
            <tr>
              <td>Query DSP Failed</td>
              <td><?php echo $this->mks[0][8];?></td>
            </tr>
            <tr>
              <td>BInsufficient Balance</td>
              <td><?php echo $this->mks[0][9];?></td>
            </tr>
            <tr>
              <td>No Info Of B</td>
              <td><?php echo $this->mks[0][10];?></td>
            </tr>
            <tr>
              <td>B Roaming</td>
              <td><?php echo $this->mks[0][11];?></td>
            </tr>
            <tr>
              <td>B Unreachable</td>
              <td><?php echo $this->mks[0][12];?></td>
            </tr>
            <tr>
              <td>B Busy</td>
              <td><?php echo $this->mks[0][13];?></td>
            </tr>
            <tr>
              <td>B No Answer</td>
              <td><?php echo $this->mks[0][14];?></td>
            </tr>
            <tr>
              <td>Bo Answer</td>
              <td><?php echo $this->mks[0][15];?></td>
            </tr>
            <tr>
              <td>B Error</td>
              <td><?php echo $this->mks[0][16];?></td>
            </tr>
            <tr>
              <td>BO Abandon</td>
              <td><?php echo $this->mks[0][17];?></td>
            </tr>
            <tr>
              <td>B Selection Time Out</td>
              <td><?php echo $this->mks[0][18];?></td>
            </tr>
            <tr>
              <td>B ATI Failed</td>
              <td><?php echo $this->mks[0][19];?></td>
            </tr>
            <tr>
              <td>B Press One</td>
              <td><?php echo $this->mks[0][20];?></td>
            </tr>
            <tr>
              <td>B Call A</td>
              <td><?php echo $this->mks[0][21];?></td>
            </tr>
            <tr>
              <td>A Unreachable</td>
              <td><?php echo $this->mks[0][22];?></td>
            </tr>
            <tr>
              <td>A Busy</td>
              <td><?php echo $this->mks[0][23];?></td>
            </tr>
            <tr>
              <td>A No Answer</td>
              <td><?php echo $this->mks[0][24];?></td>
            </tr>
            <tr>
              <td>Ao Answer</td>
              <td><?php echo $this->mks[0][25];?></td>
            </tr>
            <tr>
              <td>SCP Day</td>
              <td><?php echo $this->mks[0][26];?></td>
            </tr>
            <tr>
              <td>SCP Hour</td>
              <td><?php echo $this->mks[0][27];?></td>
            </tr>
            <tr>
              <td>SCP Date</td>
              <td><?php echo $this->mks[0][28];?></td>
            </tr>
            <tr>
              <td>SCP Timestamp</td>
              <td><?php echo $this->mks[0][29];?></td>
            </tr> 
          </table>
          </div>
        </div>


        <script>
        var modal_upd1 = document.getElementById('modalupdate1');
        var btn_upd1 = document.getElementById("btnupdate1");
        var span_upd1 = document.getElementsByClassName("close-upd1")[0];
        btn_upd1.onclick = function() {
          modal_upd1.style.display = "block";
        }
        span_upd1.onclick = function() {
          modal_upd1.style.display = "none";
        }
        window.onclick = function(event) {
          if (event.target == modal_upd1) {
            modal_upd1.style.display = "none";
          }
        }
        </script>

      
    <div class="attemp-business-area-1">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        <?php echo $this->ds[0][3];?>
      </span>
    </div>
    <div class="failed-business-area-1">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        <?php echo $this->ds[0][4];?>
      </span>
    </div>
    <div class="succes-buisness-area-1">
      <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        <?php echo $this->ds[0][5];?>
      </span>
    </div>
    <div class="business-area-1">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        Business
      </div>
      <div id="btnbisnis12" style="padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;">
        <?php echo $this->ds[0][2]; ?>%
      </div>
        <div id="modalbisnis-1" class="modalbisnis1">
          <div class="modal-content-bisnis1">
            <span class="close-bisnis1">&times;</span>
          <br>
          <div id="c-chart1-bisnis"></div>
          </div>
        </div>

        <script>
        var modal_bisnis1 = document.getElementById('modalbisnis-1');
        var btn_bisnis1 = document.getElementById("btnbisnis12");
        var span_bisnis1 = document.getElementsByClassName("close-bisnis1")[0];
        btn_bisnis1.onclick = function() {
          modal_bisnis1.style.display = "block";
        }
        span_bisnis1.onclick = function() {
          modal_bisnis1.style.display = "none";
        }
        window.onclick = function(event) {
          if (event.target == modal_bisnis1) {
            modal_bisnis1.style.display = "none";
          }
        }

        Highcharts.chart('c-chart1-bisnis', {
          chart: {
            zoomType:'xy',
                backgroundColor: '#202020',
              width:1080
            },  
            title: {
                text: "BUSINESS <?php echo $this->ds[0][1];?>",
                style: { 
                    color: '#4aafce',
                    fontFamily: 'clock',
                    fontSize: '24px',
                    fontWeight: 'bold'
                }
            },
          xAxis: [{
                categories:[<?php echo $mks_bisnis_1;?>],
            labels: {
                    rotation: -45,
              style: {
                          color: '#ffffff'
                  }
                }
            }],
          yAxis: [{ 
                  labels: {
                      style: {
                          color: '#d1db62'
                      }
                  }
              }, { 
                  labels: {
                      style: {
                          color: '#42ce35'
                      }
                  },
                  opposite: true
              }],
              tooltip: {
                  shared: true
              },      
            legend: {
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
            itemStyle: {
                        font: '9pt Trebuchet MS, Verdana, sans-serif',
                        color: '#A0A0A0'
              },
              itemHoverStyle: { color: '#FFF'},
              itemHiddenStyle: { color: '#444'}
            },
            series: [{
                name: 'BUSINESS Attemp',
                data: [<?php echo $mks_bisnis_2;?>],
                type: 'spline',
                color: '#d1db62'
            }, {
                name: 'BUSINESS Failed',
                data: [<?php echo $mks_bisnis_3;?>],
                type: 'spline',
                color: '#ed8474'
            }, {
                name: 'BUSINESS Success',
                data: [<?php echo $mks_bisnis_4;?>],
                type: 'spline',
                yAxis: 1,
                color: '#42ce35'
            }],
        }); 
        </script>      
    </div>
    <div class="attemp-bparty-area-1">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        <?php echo $this->part[0][5];?>
      </span>
    </div>
    <div class="failed-bparty-area-1">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        <?php echo $this->part[0][7];?>
      </span>
    </div>
    <div class="success-bparty-area-1">
      <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        <?php echo $this->part[0][6];?>
      </span>
    </div>
    <div class="bparty-area-1">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        B#Party
      </div>
      <?php echo $btnbp1; ?>
    </div>

        <div id="modalbp-1" class="modalbp1">
          <div class="modal-content-bp1">
            <span class="close-bp1">&times;</span>
            <br>
          <div id="c-chart1-bp"></div>
          </div>
        </div>

        <script>
        var modal_bp1 = document.getElementById('modalbp-1');
        var btn_bp1 = document.getElementById("btnbp1");
        var span_bp1 = document.getElementsByClassName("close-bp1")[0];
        btn_bp1.onclick = function() {
          modal_bp1.style.display = "block";
        }
        span_bp1.onclick = function() {
          modal_bp1.style.display = "none";
        }
        window.onclick = function(event) {
          if (event.target == modal_bp1) {
            modal_bp1.style.display = "none";
          }
        }

        Highcharts.chart('c-chart1-bp', {
          chart: {
            zoomType:'xy',
            backgroundColor: '#202020',
              width:1080
          
            },  
            title: {
                text: "B#PARTY <?php echo $this->ds[0][1];?>",
            style: { 
                    color: '#4aafce',
                    fontFamily: 'clock',
                    fontSize: '24px',
                    fontWeight: 'bold'
                }
            },
          xAxis: [{
                categories:[<?php echo $mks_party_1;?>],
            labels: {
                    rotation: -45,
              style: {
                          color: '#ffffff'
                  }
                }
            }],
          yAxis: [{ 
                  labels: {
                      style: {
                          color: '#d1db62'
                      }
                  }
              }, { 
              labels: {
                      style: {
                          color: '#42ce35'
                      }
                  },
                  opposite: true
              }],
              tooltip: {
                  shared: true
              },      
            legend: {
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
            itemStyle: {
                        font: '9pt Trebuchet MS, Verdana, sans-serif',
                        color: '#A0A0A0'
              },
              itemHoverStyle: { color: '#FFF'},
              itemHiddenStyle: { color: '#444'}
            },
            series: [{
                name: 'B#Party Attemp',
                data: [<?php echo $mks_party_5;?>],
                type: 'spline',
                color: '#d1db62'
            }, {
                name: 'B#Party Failed',
                data: [<?php echo $mks_party_7;?>],
                type: 'spline',
                color: '#ed8474'
            }, {
                name: 'B#Party Success',
                data: [<?php echo $mks_party_6;?>],
                type: 'spline',
                yAxis: 1, 
                color: '#42ce35'
            }],
        }); 
        </script>    

    <div class="attemp-aparty-area-1">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        <?php echo $this->part[0][1];?>
      </span>
    </div>
    <div class="failed-aparty-area-1">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        <?php echo $this->part[0][3];?>
      </span>
    </div>
    <div class="success-aparty-area-1">
      <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        <?php echo $this->part[0][2];?>
      </span>
    </div>
    <div class="aparty-area-1">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        A#Party
      </div>
      <?php echo $btnap1; ?>
    </div>

        <div id="modalap-1" class="modalap1">
          <div class="modal-content-ap1">
            <span class="close-ap1">&times;</span>
            <br>
          <div id="c-chart1-ap"></div>
          </div>
        </div>

        <script>
        var modal_ap1 = document.getElementById('modalap-1');
        var btn_ap1 = document.getElementById("btnap1");
        var span_ap1 = document.getElementsByClassName("close-ap1")[0];
        btn_ap1.onclick = function() {
          modal_ap1.style.display = "block";
        }
        span_ap1.onclick = function() {
          modal_ap1.style.display = "none";
        }
        window.onclick = function(event) {
          if (event.target == modal_ap1) {
            modal_ap1.style.display = "none";
          }
        }

        Highcharts.chart('c-chart1-ap', {
          chart: {
              zoomType:'xy',
                backgroundColor: '#202020',
              width:1080
            },  
            title: {
                text: "A#PARTY <?php echo $this->ds[0][1];?>",
            style: { 
                    color: '#4aafce',
                    fontFamily: 'clock',
                    fontSize: '24px',
                    fontWeight: 'bold'
                }
            },
          xAxis: [{
                categories:[<?php echo $mks_party_1;?>],
            labels: {
                    rotation: -45,
              style: {
                          color: '#ffffff'
                  }
                }
            }],
          yAxis: [{ 
                  labels: {
                      style: {
                          color: '#d1db62'
                      }
                  }
              }, { 
                  labels: {
                      style: {
                          color: '#ed8474'
                      }
                  },
                  opposite: true
              }],
              tooltip: {
                  shared: true
              },      
            legend: {
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
            itemStyle: {
                        font: '9pt Trebuchet MS, Verdana, sans-serif',
                        color: '#A0A0A0'
              },
              itemHoverStyle: { color: '#FFF'},
              itemHiddenStyle: { color: '#444'}
            },
            series: [{
                name: 'A#Party Attemp',
                data: [<?php echo $mks_party_2;?>],
                type: 'spline',
                color: '#d1db62'
            }, {
                name: 'A#Party Failed',
                data: [<?php echo $mks_party_4;?>],
                type: 'spline',
                color: '#ed8474',
                yAxis: 1
            }, {
                name: 'A#Party Success',
                data: [<?php echo $mks_party_3;?>],
                type: 'spline', 
                color: '#42ce35'
            }],
        });   
        </script>

    <div class="attemp-dsp-area-1">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        <?php echo $this->ds[0][7];?>
      </span>
    </div>
    <div class="failed-dsp-area-1">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        <?php echo $this->ds[0][8];?>
      </span>
    </div>
    <div class="success-dsp-area-1">
      <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        <?php echo $this->ds[0][9];?>
      </span>
    </div>
    <div class="dsp-area-1">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        DSP
      </div>
      <div id="btndsp1" style="padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;">
        <?php echo $this->ds[0][6]; ?>%
      </div>
        <div id="modaldsp-1" class="modaldsp1">
          <div class="modal-content-dsp1">
            <span class="close-dsp1">&times;</span>
          <br>  
          <div id="c-chart1-dsp"></div>
          </div>
        </div>

        <script>
        var modal_dsp1 = document.getElementById('modaldsp-1');
        var btn_dsp1 = document.getElementById("btndsp1");
        var span_dsp1 = document.getElementsByClassName("close-dsp1")[0];
        btn_dsp1.onclick = function() {
          modal_dsp1.style.display = "block";
        }
        span_dsp1.onclick = function() {
          modal_dsp1.style.display = "none";
        }
        window.onclick = function(event) {
          if (event.target == modal_dsp1) {
            modal_dsp1.style.display = "none";
          }
        }

        Highcharts.chart('c-chart1-dsp', {
          chart: {
            zoomType:'xy',
            backgroundColor: '#202020',
              width:1080
            },  
            title: {
                text: "DSP <?php echo $this->ds[0][1];?>",
            style: { 
                    color: '#4aafce',
                    fontFamily: 'clock',
                    fontSize: '24px',
                    fontWeight: 'bold'
                }
            },
          xAxis: [{
                categories:[<?php echo $mks_dsp_1;?>],
            labels: {
                    rotation: -45,
              style: {
                          color: '#ffffff'
                  }
                }
            }],
          yAxis: [{ 
                  labels: {
                      style: {
                          color: '#d1db62'
                      }
                  }
              }, { 
              labels: {
                      style: {
                          color: '#ed8474'
                      }
                  },
                  opposite: true
              }],
              tooltip: {
                  shared: true
              },      
            legend: {
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
            itemStyle: {
                        font: '9pt Trebuchet MS, Verdana, sans-serif',
                        color: '#A0A0A0'
              },
              itemHoverStyle: { color: '#FFF'},
              itemHiddenStyle: { color: '#444'}
            },
            series: [{
                name: 'DSP Attemp',
                data: [<?php echo $mks_dsp_2;?>],
                type: 'spline',
                color: '#d1db62'
            }, {
                name: 'DSP Failed',
                data: [<?php echo $mks_dsp_3;?>],
                yAxis: 1,
                type: 'spline',
                color: '#ed8474'
            }, {
                name: 'DSP Success',
                data: [<?php echo $mks_dsp_4;?>],
                type: 'spline',
                color: '#42ce35'
            }],
        });

        </script>      
    </div>
    <div class="attemp-ati-area-1">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        <?php echo $this->ds[0][11];?>
      </span>
    </div>
    <div class="failed-ati-area-1">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        <?php echo $this->ds[0][12];?>
      </span>
    </div>
    <div class="success-ati-area-1">
      <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        <?php echo $this->ds[0][13];?>
      </span> 
    </div>
    <div class="ati-area-1">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        ATI
      </div>
      <div id="btnati1" style="padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;">
        <?php echo $this->ds[0][10]; ?>%
      </div>
        <div id="modalati-1" class="modalati1">
          <div class="modal-content-ati1">
            <span class="close-ati1">&times;</span>
          <br>  
          <div id="c-chart1-ati"></div>
          </div>
        </div>

        <script>
        var modal_ati1 = document.getElementById('modalati-1');
        var btn_ati1 = document.getElementById("btnati1");
        var span_ati1 = document.getElementsByClassName("close-ati1")[0];
        btn_ati1.onclick = function() {
          modal_ati1.style.display = "block";
        }
        span_ati1.onclick = function() {
          modal_ati1.style.display = "none";
        }
        window.onclick = function(event) {
          if (event.target == modal_ati1) {
            modal_ati1.style.display = "none";
          }
        }

        Highcharts.chart('c-chart1-ati', {
          chart: {
              zoomType:'xy',
                backgroundColor: '#202020',
              width:1080
            },  
            title: {
                text: "ATI <?php echo $this->ds[0][1];?>",
            style: { 
                    color: '#4aafce',
                    fontFamily: 'clock',
                    fontSize: '24px',
                    fontWeight: 'bold'
                }
            },
          xAxis: [{
                categories:[<?php echo $mks_ati_1;?>],
            labels: {
                    rotation: -45,
              style: {
                          color: '#ffffff'
                  }
                }
            }],

          yAxis: [{ 
                  labels: {
                      style: {
                          color: '#d1db62'
                      }
                  }
              }, { 
                  labels: {
                      style: {
                          color: '#ed8474'
                      }
                  },
                  opposite: true
              }],
              tooltip: {
                  shared: true
              },      
            legend: {
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
            itemStyle: {
                        font: '9pt Trebuchet MS, Verdana, sans-serif',
                        color: '#A0A0A0'
              },
              itemHoverStyle: { color: '#FFF'},
              itemHiddenStyle: { color: '#444'}
            },
            series: [{
                name: 'ATI Attemp',
                data: [<?php echo $mks_ati_2;?>],
                type: 'spline',
                color: '#d1db62'
            }, {
                name: 'ATI Failed',
                data: [<?php echo $mks_ati_3;?>],
                yAxis: 1,
                type: 'spline',
                color: '#ed8474'
            }, {
                name: 'ATI Success',
                data: [<?php echo $mks_ati_4;?>],
                type: 'spline',
                color: '#42ce35'
            }],
        });   
        </script>
    </div>
  </div>
    
    
    
  <div class="area-2">
    <div class="header-area-2"><?php echo $this->ds[1][1];?></div>
    <div class="footer-area-2" id="btnupdate2">Last Update <?php echo $this->ds[1][14]?> &#8505;</div>
      
      
        <div id="modalupdate2" class="modalupd2">
          <div class="modal-content-upd2">
            <span class="close-upd2">&times;</span>
            <p style="text-align:left; color:#f2f2f2; font-family:arial;"><?php echo $this->ds[1][1];?> Call Addict Data Detail</p>
          <table>
            <tr>
              <td>ID SCP</td>
              <td><?php echo $this->pku[0][0];?></td>
            </tr>
            <tr>
              <td>SCP Code</td>
              <td><?php echo $this->pku[0][1];?></td>
            </tr>
            <tr>
              <td>Got Anno Call Addict</td>
              <td><?php echo $this->pku[0][2];?></td>
            </tr>
            <tr>
              <td>Max Daily Threshold</td>
              <td><?php echo $this->pku[0][3];?></td>
            </tr>
            <tr>
              <td>A Press End Call</td>
              <td><?php echo $this->pku[0][4];?></td>
            </tr>
            <tr>
              <td>A Press Not One</td>
              <td><?php echo $this->pku[0][5];?></td>
            </tr>
            <tr>
              <td>A Press One</td>
              <td><?php echo $this->pku[0][6];?></td>
            </tr>
            <tr>
              <td>Query DSP Success</td>
              <td><?php echo $this->pku[0][7];?></td>
            </tr>
            <tr>
              <td>Query DSP Failed</td>
              <td><?php echo $this->pku[0][8];?></td>
            </tr>
            <tr>
              <td>BInsufficient Balance</td>
              <td><?php echo $this->pku[0][9];?></td>
            </tr>
            <tr>
              <td>No Info Of B</td>
              <td><?php echo $this->pku[0][10];?></td>
            </tr>
            <tr>
              <td>B Roaming</td>
              <td><?php echo $this->pku[0][11];?></td>
            </tr>
            <tr>
              <td>B Unreachable</td>
              <td><?php echo $this->pku[0][12];?></td>
            </tr>
            <tr>
              <td>B Busy</td>
              <td><?php echo $this->pku[0][13];?></td>
            </tr>
            <tr>
              <td>B No Answer</td>
              <td><?php echo $this->pku[0][14];?></td>
            </tr>
            <tr>
              <td>Bo Answer</td>
              <td><?php echo $this->pku[0][15];?></td>
            </tr>
            <tr>
              <td>B Error</td>
              <td><?php echo $this->pku[0][16];?></td>
            </tr>
            <tr>
              <td>BO Abandon</td>
              <td><?php echo $this->pku[0][17];?></td>
            </tr>
            <tr>
              <td>B Selection Time Out</td>
              <td><?php echo $this->pku[0][18];?></td>
            </tr>
            <tr>
              <td>B ATI Failed</td>
              <td><?php echo $this->pku[0][19];?></td>
            </tr>
            <tr>
              <td>B Press One</td>
              <td><?php echo $this->pku[0][20];?></td>
            </tr>
            <tr>
              <td>B Call A</td>
              <td><?php echo $this->pku[0][21];?></td>
            </tr>
            <tr>
              <td>A Unreachable</td>
              <td><?php echo $this->pku[0][22];?></td>
            </tr>
            <tr>
              <td>A Busy</td>
              <td><?php echo $this->pku[0][23];?></td>
            </tr>
            <tr>
              <td>A No Answer</td>
              <td><?php echo $this->pku[0][24];?></td>
            </tr>
            <tr>
              <td>Ao Answer</td>
              <td><?php echo $this->pku[0][25];?></td>
            </tr>
            <tr>
              <td>SCP Day</td>
              <td><?php echo $this->pku[0][26];?></td>
            </tr>
            <tr>
              <td>SCP Hour</td>
              <td><?php echo $this->pku[0][27];?></td>
            </tr>
            <tr>
              <td>SCP Date</td>
              <td><?php echo $this->pku[0][28];?></td>
            </tr>
            <tr>
              <td>SCP Timestamp</td>
              <td><?php echo $this->pku[0][29];?></td>
            </tr> 
          </table>
          </div>
        </div>


        <script>
        var modal_upd2 = document.getElementById('modalupdate2');
        var btn_upd2 = document.getElementById("btnupdate2");
        var span_upd2 = document.getElementsByClassName("close-upd2")[0];
        btn_upd2.onclick = function() {
          modal_upd2.style.display = "block";
        }
        span_upd2.onclick = function() {
          modal_upd2.style.display = "none";
        }
        window.onclick = function(event) {
          if (event.target == modal_upd2) {
            modal_upd2.style.display = "none";
          }
        }
        </script>       
      
      
    <div class="attemp-business-area-2">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        <?php echo $this->ds[1][3];?>
      </span>
    </div>
    <div class="failed-business-area-2">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        <?php echo $this->ds[1][4];?>
      </span>
    </div>
    <div class="success-business-area-2">
      <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        <?php echo $this->ds[1][5];?>
      </span>
    </div>
    <div class="business-area-2">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        Business
      </div>
      <div id="btnbisnis22" style="padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;">
        <?php echo $this->ds[1][2]; ?>%
      </div>

        <div id="modalbisnis-2" class="modalbisnis2">
          <div class="modal-content-bisnis2">
            <span class="close-bisnis2">&times;</span>
          <br>
          <div id="c-chart2-bisnis"></div>
          </div>
        </div>


        <script>
        var modal_bisnis2 = document.getElementById('modalbisnis-2');
        var btn_bisnis2 = document.getElementById("btnbisnis22");
        var span_bisnis2 = document.getElementsByClassName("close-bisnis2")[0];
        btn_bisnis2.onclick = function() {
          modal_bisnis2.style.display = "block";
        }
        span_bisnis2.onclick = function() {
          modal_bisnis2.style.display = "none";
        }
        window.onclick = function(event) {
          if (event.target == modal_bisnis2) {
            modal_bisnis2.style.display = "none";
          }
        }

        Highcharts.chart('c-chart2-bisnis', {
          chart: {
            zoomType:'xy',
                backgroundColor: '#202020',
              width:1080
            },  
            title: {
                text: "BUSINESS <?php echo $this->ds[1][1];?>",
            style: { 
                    color: '#4aafce',
                    fontFamily: 'clock',
                    fontSize: '24px',
                    fontWeight: 'bold'
                }
            },
          xAxis: [{
                categories:[<?php echo $pku_bisnis_1;?>],
            labels: {
                    rotation: -45,
              style: {
                          color: '#ffffff'
                  }
                }
            }],
          yAxis: [{ 
                  labels: {
                      style: {
                          color: '#d1db62'
                      }
                  }
              }, { 
                  labels: {
                      style: {
                          color: '#42ce35'
                      }
                  },
                  opposite: true
              }],
              tooltip: {
                  shared: true
              },      
            legend: {
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
            itemStyle: {
                        font: '9pt Trebuchet MS, Verdana, sans-serif',
                        color: '#A0A0A0'
              },
              itemHoverStyle: { color: '#FFF'},
              itemHiddenStyle: { color: '#444'}
            },
            series: [{
                name: 'BUSINESS Attemp',
                data: [<?php echo $pku_bisnis_2;?>],
                type: 'spline',
                color: '#d1db62'
            }, {
                name: 'BUSINESS Failed',
                data: [<?php echo $pku_bisnis_3;?>],
                type: 'spline',
                color: '#ed8474'
            }, {
                name: 'BUSINESS Success',
                data: [<?php echo $pku_bisnis_4;?>],
                type: 'spline',
                yAxis: 1,
                color: '#42ce35'
            }],
        });
        </script>

    </div>
    <div class="attemp-bparty-area-2">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        <?php echo $this->part[1][5];?>
      </span>      
    </div>
    <div class="failed-bparty-area-2">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        <?php echo $this->part[1][7];?>
      </span>
    </div>
    <div class="success-bparty-area-2">
      <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        <?php echo $this->part[1][6];?>
      </span>
    </div>
    <div class="bparty-area-2">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        B#Party
      </div>
      <?php echo $btnbp2; ?>
    </div>

        <div id="modalbp-2" class="modalbp2">
          <div class="modal-content-bp2">
            <span class="close-bp2">&times;</span>
            <br>
          <div id="c-chart2-bp"></div>
          </div>
        </div>

        <script>
        var modal_bp2 = document.getElementById('modalbp-2');
        var btn_bp2 = document.getElementById("btnbp2");
        var span_bp2 = document.getElementsByClassName("close-bp2")[0];
        btn_bp2.onclick = function() {
          modal_bp2.style.display = "block";
        }
        span_bp2.onclick = function() {
          modal_bp2.style.display = "none";
        }
        window.onclick = function(event) {
          if (event.target == modal_bp2) {
            modal_bp2.style.display = "none";
          }
        }

        Highcharts.chart('c-chart2-bp', {
          chart: {
              zoomType:'xy',
                backgroundColor: '#202020',
              width:1080
            },  
            title: {
                text: "B#PARTY <?php echo $this->ds[1][1];?>",
            style: { 
                    color: '#4aafce',
                    fontFamily: 'clock',
                    fontSize: '24px',
                    fontWeight: 'bold'
                }
            },
          xAxis: [{
                categories:[<?php echo $pku_party_1;?>],
            labels: {
                    rotation: -45,
              style: {
                          color: '#ffffff'
                  }
                }
            }],
          yAxis: [{ 
                  labels: {
                      style: {
                          color: '#d1db62'
                      }
                  }
              }, { 
                  labels: {
                      style: {
                          color: '#42ce35'
                      }
                  },
                  opposite: true
              }],
              tooltip: {
                  shared: true
              },      
            legend: {
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
            itemStyle: {
                        font: '9pt Trebuchet MS, Verdana, sans-serif',
                        color: '#A0A0A0'
              },
              itemHoverStyle: { color: '#FFF'},
              itemHiddenStyle: { color: '#444'}
            },
            series: [{
                name: 'B#Party Attemp',
                data: [<?php echo $pku_party_5;?>],
                type: 'spline',
                color: '#d1db62'
            }, {
                name: 'B#Party Failed',
                data: [<?php echo $pku_party_7;?>],
                type: 'spline',
                color: '#ed8474'
            }, {
                name: 'B#Party Success',
                data: [<?php echo $pku_party_6;?>],
                type: 'spline',
                yAxis: 1, 
                color: '#42ce35'
            }],
        });
        </script>

    <div class="attemp-aparty-area-2">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        <?php echo $this->part[1][1];?>
      </span>
    </div>
    <div class="failed-aparty-area-2">
    <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        <?php echo $this->part[1][3];?>
      </span>
    </div>
    <div class="success-aparty-area-2">
    <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        <?php echo $this->part[1][2];?>
      </span>
    </div>
    <div class="aparty-area-2">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        A#Party
      </div>
      <?php echo $btnap2; ?>
    </div>

        <div id="modalap-2" class="modalap2">
          <div class="modal-content-ap2">
            <span class="close-ap2">&times;</span>
            <br>
          <div id="c-chart2-ap"></div>
          </div>
        </div>

        <script>
        var modal_ap2 = document.getElementById('modalap-2');
        var btn_ap2 = document.getElementById("btnap2");
        var span_ap2 = document.getElementsByClassName("close-ap2")[0];
        btn_ap2.onclick = function() {
          modal_ap2.style.display = "block";
        }
        span_ap2.onclick = function() {
          modal_ap2.style.display = "none";
        }
        window.onclick = function(event) {
          if (event.target == modal_ap2) {
            modal_ap2.style.display = "none";
          }
        }

        Highcharts.chart('c-chart2-ap', {
          chart: {
              zoomType:'xy',
                backgroundColor: '#202020',
              width:1080
            },  
            title: {
                text: "A#PARTY <?php echo $this->ds[1][1];?>",
            style: { 
                    color: '#4aafce',
                    fontFamily: 'clock',
                    fontSize: '24px',
                    fontWeight: 'bold'
                }
            },
          xAxis: [{
                categories:[<?php echo $pku_party_1;?>],
            labels: {
                    rotation: -45,
              style: {
                          color: '#ffffff'
                  }
                }
            }],
          yAxis: [{ 
                  labels: {
                      style: {
                          color: '#d1db62'
                      }
                  }
              }, { 
                  labels: {
                      style: {
                          color: '#ed8474'
                      }     
                  },
                  opposite: true
              }],
              tooltip: {
                  shared: true
              },      
            legend: {
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
            itemStyle: {
                        font: '9pt Trebuchet MS, Verdana, sans-serif',
                        color: '#A0A0A0'
              },
              itemHoverStyle: { color: '#FFF'},
              itemHiddenStyle: { color: '#444'}
            },
            series: [{
                name: 'A#Party Attemp',
                data: [<?php echo $pku_party_2;?>],
                type: 'spline',
                color: '#d1db62'
            }, {
                name: 'A#Party Failed',
                data: [<?php echo $pku_party_4;?>],
                type: 'spline',
                color: '#ed8474',
                yAxis:1
            }, {
                name: 'A#Party Success',
                data: [<?php echo $pku_party_3;?>],
                type: 'spline', 
                color: '#42ce35'
            }],
        }); 
        </script>

    <div class="attemp-dsp-area-2">
     <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        <?php echo $this->ds[1][7];?>
      </span> 
    </div>
    <div class="failed-dsp-area-2">
    <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        <?php echo $this->ds[1][8];?>
      </span>     
    </div>
    <div class="success-dsp-area-2">
    <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        <?php echo $this->ds[1][9];?>
      </span>
    </div>
    <div class="dsp-area-2">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        DSP
      </div>
      <div id="btndsp2" style="padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;">
        <?php echo $this->ds[1][6]; ?>%
      </div>

            <div id="modaldsp-2" class="modaldsp2">
              <div class="modal-content-dsp2">
                <span class="close-dsp2">&times;</span>
              <br>  
              <div id="c-chart2-dsp"></div>
              </div>
            </div>

            <script>
            var modal_dsp2 = document.getElementById('modaldsp-2');
            var btn_dsp2 = document.getElementById("btndsp2");
            var span_dsp2 = document.getElementsByClassName("close-dsp2")[0];
            btn_dsp2.onclick = function() {
              modal_dsp2.style.display = "block";
            }
            span_dsp2.onclick = function() {
              modal_dsp2.style.display = "none";
            }
            window.onclick = function(event) {
              if (event.target == modal_dsp2) {
                modal_dsp2.style.display = "none";
              }
            }

            Highcharts.chart('c-chart2-dsp', {
              chart: {
                  zoomType:'xy',
                    backgroundColor: '#202020',
                  width:1080
                },  
                title: {
                    text: "DSP <?php echo $this->ds[1][1];?>",
                style: { 
                    color: '#4aafce',
                    fontFamily: 'clock',
                    fontSize: '24px',
                    fontWeight: 'bold'
                }
                },
              xAxis: [{
                    categories:[<?php echo $pku_dsp_1;?>],
                labels: {
                        rotation: -45,
                  style: {
                              color: '#ffffff'
                      }
                    }
                }],
              yAxis: [{ 
                      labels: {
                          style: {
                              color: '#d1db62'
                          }
                      }
                  }, { 
                      labels: {
                          style: {
                              color: '#ed8474'
                          }
                      },
                      opposite: true
                  }],
                  tooltip: {
                      shared: true
                  },      
                legend: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
                itemStyle: {
                            font: '9pt Trebuchet MS, Verdana, sans-serif',
                            color: '#A0A0A0'
                  },
                  itemHoverStyle: { color: '#FFF'},
                  itemHiddenStyle: { color: '#444'}
                },
                series: [{
                    name: 'DSP Attemp',
                    data: [<?php echo $pku_dsp_2;?>],
                    type: 'spline',
                    color: '#d1db62'
                }, {
                    name: 'DSP Failed',
                    data: [<?php echo $pku_dsp_3;?>],
                    yAxis: 1,
                    type: 'spline',
                    color: '#ed8474'
                }, {
                    name: 'DSP Success',
                    data: [<?php echo $pku_dsp_4;?>],
                    type: 'spline',
                    color: '#42ce35'
                }],
            });

            </script>

    </div>
    <div class="attemp-ati-area-2">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        <?php echo $this->ds[1][11];?>
      </span>
    </div>
    <div class="failed-ati-area-2">
    <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        <?php echo $this->ds[1][12];?>
      </span>
    </div>
    <div class="success-ati-area-2">
      <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        <?php echo $this->ds[1][13];?>
      </span>
    </div>
    <div class="ati-area-2">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        ATI
      </div>
      <div id="btnati2" style="padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;">
        <?php echo $this->ds[1][10]; ?>%
      </div>

            <div id="modalati-2" class="modalati2">
              <div class="modal-content-ati2">
                <span class="close-ati2">&times;</span>
              <br>  
              <div id="c-chart2-ati"></div>
              </div>
            </div>

            <script>
            var modal_ati2 = document.getElementById('modalati-2');
            var btn_ati2 = document.getElementById("btnati2");
            var span_ati2 = document.getElementsByClassName("close-ati2")[0];
            btn_ati2.onclick = function() {
              modal_ati2.style.display = "block";
            }
            span_ati2.onclick = function() {
              modal_ati2.style.display = "none";
            }
            window.onclick = function(event) {
              if (event.target == modal_ati2) {
                modal_ati2.style.display = "none";
              }
            }

            Highcharts.chart('c-chart2-ati', {
              chart: {
                  zoomType:'xy',
                    backgroundColor: '#202020',
                  width:1080
                },  
                title: {
                    text: "ATI <?php echo $this->ds[1][1];?>",
                style: { 
                    color: '#4aafce',
                    fontFamily: 'clock',
                    fontSize: '24px',
                    fontWeight: 'bold'
                }
                },
              xAxis: [{
                    categories:[<?php echo $pku_ati_1;?>],
                labels: {
                        rotation: -45,
                  style: {
                              color: '#ffffff'
                      }
                    }
                }],
              yAxis: [{ 
                      labels: {
                          style: {
                              color: '#d1db62'
                          }
                      }
                  }, { 
                      labels: {
                          style: {
                              color: '#ed8474'
                          }     
                      },
                      opposite: true
                  }],
                  tooltip: {
                      shared: true
                  },      
                legend: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
                itemStyle: {
                            font: '9pt Trebuchet MS, Verdana, sans-serif',
                            color: '#A0A0A0'
                  },
                  itemHoverStyle: { color: '#FFF'},
                  itemHiddenStyle: { color: '#444'}
                },
                series: [{
                    name: 'ATI Attemp',
                    data: [<?php echo $pku_ati_2;?>],
                    type: 'spline',
                    color: '#d1db62'
                }, {
                    name: 'ATI Failed',
                    data: [<?php echo $pku_ati_3;?>],
                    yAxis: 1,
                    type: 'spline',
                    color: '#ed8474'
                }, {
                    name: 'ATI Success',
                    data: [<?php echo $pku_ati_4;?>],
                    type: 'spline',
                    color: '#42ce35'
                }],
            }); 
            </script>

    </div>
  </div>
    
    
    
  <div class="area-3">
    <div class="header-area-3"><?php echo $this->ds[2][1];?></div>
    <div class="footer-area-3" id="btnupdate3">Last Update <?php echo $this->ds[2][14]?> &#8505;</div>
      
      <div id="modalupdate3" class="modalupd3">
          <div class="modal-content-upd3">
            <span class="close-upd3">&times;</span>
            <p style="text-align:left; color:#f2f2f2; font-family:arial;"><?php echo $this->ds[2][1];?> Call Addict Data Detail</p>
          <table>
            <tr>
              <td>ID SCP</td>
              <td><?php echo $this->sby[0][0];?></td>
            </tr>
            <tr>
              <td>SCP Code</td>
              <td><?php echo $this->sby[0][1];?></td>
            </tr>
            <tr>
              <td>Got Anno Call Addict</td>
              <td><?php echo $this->sby[0][2];?></td>
            </tr>
            <tr>
              <td>Max Daily Threshold</td>
              <td><?php echo $this->sby[0][3];?></td>
            </tr>
            <tr>
              <td>A Press End Call</td>
              <td><?php echo $this->sby[0][4];?></td>
            </tr>
            <tr>
              <td>A Press Not One</td>
              <td><?php echo $this->sby[0][5];?></td>
            </tr>
            <tr>
              <td>A Press One</td>
              <td><?php echo $this->sby[0][6];?></td>
            </tr>
            <tr>
              <td>Query DSP Success</td>
              <td><?php echo $this->sby[0][7];?></td>
            </tr>
            <tr>
              <td>Query DSP Failed</td>
              <td><?php echo $this->sby[0][8];?></td>
            </tr>
            <tr>
              <td>BInsufficient Balance</td>
              <td><?php echo $this->sby[0][9];?></td>
            </tr>
            <tr>
              <td>No Info Of B</td>
              <td><?php echo $this->sby[0][10];?></td>
            </tr>
            <tr>
              <td>B Roaming</td>
              <td><?php echo $this->sby[0][11];?></td>
            </tr>
            <tr>
              <td>B Unreachable</td>
              <td><?php echo $this->sby[0][12];?></td>
            </tr>
            <tr>
              <td>B Busy</td>
              <td><?php echo $this->sby[0][13];?></td>
            </tr>
            <tr>
              <td>B No Answer</td>
              <td><?php echo $this->sby[0][14];?></td>
            </tr>
            <tr>
              <td>Bo Answer</td>
              <td><?php echo $this->sby[0][15];?></td>
            </tr>
            <tr>
              <td>B Error</td>
              <td><?php echo $this->sby[0][16];?></td>
            </tr>
            <tr>
              <td>BO Abandon</td>
              <td><?php echo $this->sby[0][17];?></td>
            </tr>
            <tr>
              <td>B Selection Time Out</td>
              <td><?php echo $this->sby[0][18];?></td>
            </tr>
            <tr>
              <td>B ATI Failed</td>
              <td><?php echo $this->sby[0][19];?></td>
            </tr>
            <tr>
              <td>B Press One</td>
              <td><?php echo $this->sby[0][20];?></td>
            </tr>
            <tr>
              <td>B Call A</td>
              <td><?php echo $this->sby[0][21];?></td>
            </tr>
            <tr>
              <td>A Unreachable</td>
              <td><?php echo $this->sby[0][22];?></td>
            </tr>
            <tr>
              <td>A Busy</td>
              <td><?php echo $this->sby[0][23];?></td>
            </tr>
            <tr>
              <td>A No Answer</td>
              <td><?php echo $this->sby[0][24];?></td>
            </tr>
            <tr>
              <td>Ao Answer</td>
              <td><?php echo $this->sby[0][25];?></td>
            </tr>
            <tr>
              <td>SCP Day</td>
              <td><?php echo $this->sby[0][26];?></td>
            </tr>
            <tr>
              <td>SCP Hour</td>
              <td><?php echo $this->sby[0][27];?></td>
            </tr>
            <tr>
              <td>SCP Date</td>
              <td><?php echo $this->sby[0][28];?></td>
            </tr>
            <tr>
              <td>SCP Timestamp</td>
              <td><?php echo $this->sby[0][29];?></td>
            </tr> 
          </table>
          </div>
        </div>


        <script>
        var modal_upd3 = document.getElementById('modalupdate3');
        var btn_upd3 = document.getElementById("btnupdate3");
        var span_upd3 = document.getElementsByClassName("close-upd3")[0];
        btn_upd3.onclick = function() {
          modal_upd3.style.display = "block";
        }
        span_upd3.onclick = function() {
          modal_upd3.style.display = "none";
        }
        window.onclick = function(event) {
          if (event.target == modal_upd3) {
            modal_upd3.style.display = "none";
          }
        }
        </script>
      
    <div class="attemp-business-area-3">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        <?php echo $this->ds[2][3];?>
      </span>
    </div>
    <div class="failed-business-area-3">
    <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        <?php echo $this->ds[2][4];?>
      </span>
    </div>
    <div class="success-business-area-3">
    <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        <?php echo $this->ds[2][5];?>
      </span>
    </div>
    <div class="business-area-3">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        Business
      </div>
      <div id="btnbisnis32" style="padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;">
        <?php echo $this->ds[2][2]; ?>%
      </div>

            <div id="modalbisnis-3" class="modalbisnis3">
              <div class="modal-content-bisnis3">
                <span class="close-bisnis3">&times;</span>
              <br>
              <div id="c-chart3-bisnis"></div>
              </div>
            </div>


            <script>
            var modal_bisnis3 = document.getElementById('modalbisnis-3');
            var btn_bisnis3 = document.getElementById("btnbisnis32");
            var span_bisnis3 = document.getElementsByClassName("close-bisnis3")[0];
            btn_bisnis3.onclick = function() {
              modal_bisnis3.style.display = "block";
            }
            span_bisnis3.onclick = function() {
              modal_bisnis3.style.display = "none";
            }
            window.onclick = function(event) {
              if (event.target == modal_bisnis3) {
                modal_bisnis3.style.display = "none";
              }
            }

            Highcharts.chart('c-chart3-bisnis', {
              chart: {
                zoomType:'xy',
                    backgroundColor: '#202020',
                  width:1080
                },  
                title: {
                    text: "BUSINESS <?php echo $this->ds[2][1];?>",
                style: { 
                    color: '#4aafce',
                    fontFamily: 'clock',
                    fontSize: '24px',
                    fontWeight: 'bold'
                }
                },
              xAxis: [{
                    categories:[<?php echo $sby_bisnis_1;?>],
                labels: {
                        rotation: -45,
                  style: {
                              color: '#ffffff'
                      }
                    }
                }],
              yAxis: [{ 
                      labels: {
                          style: {
                              color: '#d1db62'
                          }
                      }
                  }, { 
                      
                      labels: {
                          style: {
                              color: '#42ce35'
                          }
                      },
                      opposite: true
                  }],
                  tooltip: {
                      shared: true
                  },      
                legend: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
                itemStyle: {
                            font: '9pt Trebuchet MS, Verdana, sans-serif',
                            color: '#A0A0A0'
                  },
                  itemHoverStyle: { color: '#FFF'},
                  itemHiddenStyle: { color: '#444'}
                },
                series: [{
                    name: 'BUSINESS Attemp',
                    data: [<?php echo $sby_bisnis_2;?>],
                    type: 'spline',
                    color: '#d1db62'
                }, {
                    name: 'BUSINESS Failed',
                    data: [<?php echo $sby_bisnis_3;?>],
                    type: 'spline',
                    color: '#ed8474'
                }, {
                    name: 'BUSINESS Success',
                    data: [<?php echo $sby_bisnis_4;?>],
                    type: 'spline',
                    yAxis: 1,
                    color: '#42ce35'
                }],
            });
            </script>

    </div>
    <div class="attemp-bparty-area-3">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        <?php echo $this->part[2][5];?>
      </span>
    </div>
    <div class="failed-bparty-area-3">
    <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        <?php echo $this->part[2][7];?>
      </span>
    </div>
    <div class="success-bparty-area-3">
    <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        <?php echo $this->part[2][6];?>
      </span>
    </div>
    <div class="bparty-area-3">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        B#Party
      </div>
      <?php echo $btnbp3; ?>
    </div>

        <div id="modalbp-3" class="modalbp3">
          <div class="modal-content-bp3">
            <span class="close-bp3">&times;</span>
            <br>
          <div id="c-chart3-bp"></div>
          </div>
        </div>

        <script>
        var modal_bp3 = document.getElementById('modalbp-3');
        var btn_bp3 = document.getElementById("btnbp3");
        var span_bp3 = document.getElementsByClassName("close-bp3")[0];
        btn_bp3.onclick = function() {
          modal_bp3.style.display = "block";
        }
        span_bp3.onclick = function() {
          modal_bp3.style.display = "none";
        }
        window.onclick = function(event) {
          if (event.target == modal_bp3) {
            modal_bp3.style.display = "none";
          }
        }

        Highcharts.chart('c-chart3-bp', {
          chart: {
              zoomType:'xy',
                backgroundColor: '#202020',
              width:1080
            },  
            title: {
                text: "B#PARTY <?php echo $this->ds[2][1];?>",
            style: { 
                    color: '#4aafce',
                    fontFamily: 'clock',
                    fontSize: '24px',
                    fontWeight: 'bold'
                }
            },
          xAxis: [{
                categories:[<?php echo $sby_party_1;?>],
            labels: {
                    rotation: -45,
              style: {
                          color: '#ffffff'
                  }
                }
            }],
          yAxis: [{ 
                  labels: {
                      style: {
                          color: '#d1db62'
                      }
                  }
              }, { 
                  labels: {
                      style: {
                          color: '#42ce35'
                      }
                  },
                  opposite: true
              }],
              tooltip: {
                  shared: true
              },      
            legend: {
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
            itemStyle: {
                        font: '9pt Trebuchet MS, Verdana, sans-serif',
                        color: '#A0A0A0'
              },
              itemHoverStyle: { color: '#FFF'},
              itemHiddenStyle: { color: '#444'}
            },
            series: [{
                name: 'B#Party Attemp',
                data: [<?php echo $sby_party_5;?>],
                type: 'spline',
                color: '#d1db62'
            }, {
                name: 'B#Party Failed',
                data: [<?php echo $sby_party_7;?>],
                type: 'spline',
                color: '#ed8474'
            }, {
                name: 'B#Party Success',
                data: [<?php echo $sby_party_6;?>],
                type: 'spline',
                yAxis: 1, 
                color: '#42ce35'
            }],
        });
        </script>

    <div class="attemp-aparty-area-3">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        <?php echo $this->part[2][1];?>
      </span>
    </div>
    <div class="failed-aparty-area-3">
    <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        <?php echo $this->part[2][3];?>
      </span>
    </div>
    <div class="success-aparty-area-3">
    <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        <?php echo $this->part[2][2];?>
      </span>      
    </div>
    <div class="aparty-area-3">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        A#Party
      </div>
      <?php echo $btnap3; ?>
    </div>

            <div id="modalap-3" class="modalap3">
              <div class="modal-content-ap3">
                <span class="close-ap3">&times;</span>
                <br>
              <div id="c-chart3-ap"></div>
              </div>
            </div>

            <script>
            var modal_ap3 = document.getElementById('modalap-3');
            var btn_ap3 = document.getElementById("btnap3");
            var span_ap3 = document.getElementsByClassName("close-ap3")[0];
            btn_ap3.onclick = function() {
              modal_ap3.style.display = "block";
            }
            span_ap3.onclick = function() {
              modal_ap3.style.display = "none";
            }
            window.onclick = function(event) {
              if (event.target == modal_ap3) {
                modal_ap3.style.display = "none";
              }
            }

            Highcharts.chart('c-chart3-ap', {
              chart: {
                  zoomType:'xy',
                    backgroundColor: '#202020',
                  width:1080
                },  
                title: {
                    text: "A#PARTY <?php echo $this->ds[2][1];?>",
                style: { 
                    color: '#4aafce',
                    fontFamily: 'clock',
                    fontSize: '24px',
                    fontWeight: 'bold'
                }
                },
              xAxis: [{
                    categories:[<?php echo $sby_party_1;?>],
                labels: {
                        rotation: -45,
                  style: {
                              color: '#ffffff'
                      }
                    }
                }],
              yAxis: [{ 
                      labels: {
                          style: {
                              color: '#d1db62'
                          }
                      }
                  }, { 
                      labels: {
                          style: {
                              color: '#ed8474'
                          }
                      },
                      opposite: true
                  }],
                  tooltip: {
                      shared: true
                  },      
                legend: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
                itemStyle: {
                            font: '9pt Trebuchet MS, Verdana, sans-serif',
                            color: '#A0A0A0'
                  },
                  itemHoverStyle: { color: '#FFF'},
                  itemHiddenStyle: { color: '#444'}
                },
                series: [{
                    name: 'A#Party Attemp',
                    data: [<?php echo $sby_party_2;?>],
                    type: 'spline',
                    color: '#d1db62'
                }, {
                    name: 'A#Party Failed',
                    data: [<?php echo $sby_party_4;?>],
                    type: 'spline',
                    yAxis: 1,
                    color: '#ed8474'
                }, {
                    name: 'A#Party Success',
                    data: [<?php echo $sby_party_3;?>],
                    type: 'spline', 
                    color: '#42ce35'
                }],
            }); 
            </script>

    <div class="attemp-dsp-area-3">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        <?php echo $this->ds[2][7];?>
      </span>
    </div>
    <div class="failed-dsp-area-3">
    <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        <?php echo $this->ds[2][8];?>
      </span>
    </div>
    <div class="success-dsp-area-3">
    <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        <?php echo $this->ds[2][9];?>
      </span>
    </div>
    <div class="dsp-area-3">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        DSP
      </div>
      <div id="btndsp3" style="padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;">
        <?php echo $this->ds[2][6]; ?>%
      </div>

            <div id="modaldsp-3" class="modaldsp3">
              <div class="modal-content-dsp3">
                <span class="close-dsp3">&times;</span>
              <br>
              <div id="c-chart3-dsp"></div>
              </div>
            </div>

            <script>
            var modal_dsp3 = document.getElementById('modaldsp-3');
            var btn_dsp3 = document.getElementById("btndsp3");
            var span_dsp3 = document.getElementsByClassName("close-dsp3")[0];
            btn_dsp3.onclick = function() {
              modal_dsp3.style.display = "block";
            }
            span_dsp3.onclick = function() {
              modal_dsp3.style.display = "none";
            }
            window.onclick = function(event) {
              if (event.target == modal_dsp3) {
                modal_dsp3.style.display = "none";
              }
            }

            Highcharts.chart('c-chart3-dsp', {
              chart: {
                  zoomType:'xy',
                    backgroundColor: '#202020',
                  width:1080
                },  
                title: {
                    text: "DSP <?php echo $this->ds[2][1];?>",
                style: { 
                    color: '#4aafce',
                    fontFamily: 'clock',
                    fontSize: '24px',
                    fontWeight: 'bold'
                }
                },
              xAxis: [{
                    categories:[<?php echo $sby_dsp_1;?>],
                labels: {
                        rotation: -45,
                  style: {
                              color: '#ffffff'
                      }
                    }
                }],
              yAxis: [{ 
                      labels: {
                          style: {
                              color: '#d1db62'
                          }
                      }
                  }, { 
                      labels: {
                          style: {
                              color: '#ed8474'
                          }
                      },
                      opposite: true
                  }],
                  tooltip: {
                      shared: true
                  },      
                legend: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
                itemStyle: {
                            font: '9pt Trebuchet MS, Verdana, sans-serif',
                            color: '#A0A0A0'
                  },
                  itemHoverStyle: { color: '#FFF'},
                  itemHiddenStyle: { color: '#444'}
                },
                series: [{
                    name: 'DSP Attemp',
                    data: [<?php echo $sby_dsp_2;?>],
                    type: 'spline',
                    color: '#d1db62'
                }, {
                    name: 'DSP Failed',
                    data: [<?php echo $sby_dsp_3;?>],
                    yAxis: 1,
                    type: 'spline',
                    color: '#ed8474'
                }, {
                    name: 'DSP Success',
                    data: [<?php echo $sby_dsp_4;?>],
                    type: 'spline',
                    color: '#42ce35'
                }],
            });
            </script>

    </div>
    <div class="attemp-ati-area-3">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        <?php echo $this->ds[2][11];?>
      </span>
    </div>
    <div class="failed-ati-area-3">
    <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        <?php echo $this->ds[2][12];?>
      </span>
    </div>
    <div class="success-ati-area-3">
      <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        <?php echo $this->ds[2][13];?>
      </span>
    </div>
    <div class="ati-area-3">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        ATI
      </div>
      <div id="btnati3" style="padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;">
        <?php echo $this->ds[2][10]; ?>%
      </div>

            <div id="modalati-3" class="modalati3">
              <div class="modal-content-ati3">
                <span class="close-ati3">&times;</span>
              <br>  
              <div id="c-chart3-ati"></div>
              </div>
            </div>

            <script>
            var modal_ati3 = document.getElementById('modalati-3');
            var btn_ati3 = document.getElementById("btnati3");
            var span_ati3 = document.getElementsByClassName("close-ati3")[0];
            btn_ati3.onclick = function() {
              modal_ati3.style.display = "block";
            }
            span_ati3.onclick = function() {
              modal_ati3.style.display = "none";
            }
            window.onclick = function(event) {
              if (event.target == modal_ati3) {
                modal_ati3.style.display = "none";
              }
            }

            Highcharts.chart('c-chart3-ati', {
              chart: {
                  zoomType:'xy',
                    backgroundColor: '#202020',
                  width:1080
                },  
                title: {
                    text: "ATI <?php echo $this->ds[2][1];?>",
                style: { 
                    color: '#4aafce',
                    fontFamily: 'clock',
                    fontSize: '24px',
                    fontWeight: 'bold'
                }
                },
              xAxis: [{
                    categories:[<?php echo $sby_ati_1;?>],
                labels: {
                        rotation: -45,
                  style: {
                              color: '#ffffff'
                      }
                    }
                }],
              yAxis: [{ 
                      labels: {
                          style: {
                              color: '#d1db62'
                          }
                      }
                  }, { 
                      labels: {
                          style: {
                              color: '#ed8474'
                          }
                      },
                      opposite: true
                  }],
                  tooltip: {
                      shared: true
                  },      
                legend: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
                itemStyle: {
                            font: '9pt Trebuchet MS, Verdana, sans-serif',
                            color: '#A0A0A0'
                  },
                  itemHoverStyle: { color: '#FFF'},
                  itemHiddenStyle: { color: '#444'}
                },
                series: [{
                    name: 'ATI Attemp',
                    data: [<?php echo $sby_ati_2;?>],
                    type: 'spline',
                    color: '#d1db62'
                }, {
                    name: 'ATI Failed',
                    data: [<?php echo $sby_ati_3;?>],
                    yAxis: 1,
                    type: 'spline',
                    color: '#ed8474'
                }, {
                    name: 'ATI Success',
                    data: [<?php echo $sby_ati_4;?>],
                    type: 'spline',
                    color: '#42ce35'
                }],
            }); 

            </script>

    </div>
  </div>
    
    
  <div class="area-4">
    <div class="footer-area-4" id="btnupdate4">Last Update <?php echo $this->ds[3][14];?> &#8505;</div>

      <div id="modalupdate4" class="modalupd4">
          <div class="modal-content-upd4">
            <span class="close-upd4">&times;</span>
            <p style="text-align:left; color:#f2f2f2; font-family:arial;"><?php echo $this->ds[3][1];?> Call Addict Data Detail</p>
          <table>
            <tr>
              <td>ID SCP</td>
              <td><?php echo $this->tbs[0][0];?></td>
            </tr>
            <tr>
              <td>SCP Code</td>
              <td><?php echo $this->tbs[0][1];?></td>
            </tr>
            <tr>
              <td>Got Anno Call Addict</td>
              <td><?php echo $this->tbs[0][2];?></td>
            </tr>
            <tr>
              <td>Max Daily Threshold</td>
              <td><?php echo $this->tbs[0][3];?></td>
            </tr>
            <tr>
              <td>A Press End Call</td>
              <td><?php echo $this->tbs[0][4];?></td>
            </tr>
            <tr>
              <td>A Press Not One</td>
              <td><?php echo $this->tbs[0][5];?></td>
            </tr>
            <tr>
              <td>A Press One</td>
              <td><?php echo $this->tbs[0][6];?></td>
            </tr>
            <tr>
              <td>Query DSP Success</td>
              <td><?php echo $this->tbs[0][7];?></td>
            </tr>
            <tr>
              <td>Query DSP Failed</td>
              <td><?php echo $this->tbs[0][8];?></td>
            </tr>
            <tr>
              <td>BInsufficient Balance</td>
              <td><?php echo $this->tbs[0][9];?></td>
            </tr>
            <tr>
              <td>No Info Of B</td>
              <td><?php echo $this->tbs[0][10];?></td>
            </tr>
            <tr>
              <td>B Roaming</td>
              <td><?php echo $this->tbs[0][11];?></td>
            </tr>
            <tr>
              <td>B Unreachable</td>
              <td><?php echo $this->tbs[0][12];?></td>
            </tr>
            <tr>
              <td>B Busy</td>
              <td><?php echo $this->tbs[0][13];?></td>
            </tr>
            <tr>
              <td>B No Answer</td>
              <td><?php echo $this->tbs[0][14];?></td>
            </tr>
            <tr>
              <td>Bo Answer</td>
              <td><?php echo $this->tbs[0][15];?></td>
            </tr>
            <tr>
              <td>B Error</td>
              <td><?php echo $this->tbs[0][16];?></td>
            </tr>
            <tr>
              <td>BO Abandon</td>
              <td><?php echo $this->tbs[0][17];?></td>
            </tr>
            <tr>
              <td>B Selection Time Out</td>
              <td><?php echo $this->tbs[0][18];?></td>
            </tr>
            <tr>
              <td>B ATI Failed</td>
              <td><?php echo $this->tbs[0][19];?></td>
            </tr>
            <tr>
              <td>B Press One</td>
              <td><?php echo $this->tbs[0][20];?></td>
            </tr>
            <tr>
              <td>B Call A</td>
              <td><?php echo $this->tbs[0][21];?></td>
            </tr>
            <tr>
              <td>A Unreachable</td>
              <td><?php echo $this->tbs[0][22];?></td>
            </tr>
            <tr>
              <td>A Busy</td>
              <td><?php echo $this->tbs[0][23];?></td>
            </tr>
            <tr>
              <td>A No Answer</td>
              <td><?php echo $this->tbs[0][24];?></td>
            </tr>
            <tr>
              <td>Ao Answer</td>
              <td><?php echo $this->tbs[0][25];?></td>
            </tr>
            <tr>
              <td>SCP Day</td>
              <td><?php echo $this->tbs[0][26];?></td>
            </tr>
            <tr>
              <td>SCP Hour</td>
              <td><?php echo $this->tbs[0][27];?></td>
            </tr>
            <tr>
              <td>SCP Date</td>
              <td><?php echo $this->tbs[0][28];?></td>
            </tr>
            <tr>
              <td>SCP Timestamp</td>
              <td><?php echo $this->tbs[0][29];?></td>
            </tr> 
          </table>

          </div>
        </div>


        <script>
        var modal_upd4 = document.getElementById('modalupdate4');
        var btn_upd4 = document.getElementById("btnupdate4");
        var span_upd4 = document.getElementsByClassName("close-upd4")[0];
        btn_upd4.onclick = function() {
          modal_upd4.style.display = "block";
        }
        span_upd4.onclick = function() {
          modal_upd4.style.display = "none";
        }
        window.onclick = function(event) {
          if (event.target == modal_upd4) {
            modal_upd4.style.display = "none";
          }
        }
        </script>
      
    <div class="header-area-4"><?php echo $this->ds[3][1];?></div>
    <div class="success-ati-area-4">
    <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        <?php echo $this->ds[3][13];?>
      </span>
    </div>
    <div class="failed-ati-area-4">
    <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        <?php echo $this->ds[3][12];?>
      </span>
    </div>
    <div class="attemp-ati-area-4">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        <?php echo $this->ds[3][11];?>
      </span>
    </div>
    <div class="ati-area-4">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        ATI
      </div>
      <div id="btnati4" style="padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;">
        <?php echo $this->ds[3][10]; ?>%
      </div>

            <div id="modalati-4" class="modalati4">
              <div class="modal-content-ati4">
                <span class="close-ati4">&times;</span>
              <br>
              <div id="c-chart4-ati"></div>
              </div>
            </div>


            <script>
            var modal_ati4 = document.getElementById('modalati-4');
            var btn_ati4 = document.getElementById("btnati4");
            var span_ati4 = document.getElementsByClassName("close-ati4")[0];
            btn_ati4.onclick = function() {
              modal_ati4.style.display = "block";
            }
            span_ati4.onclick = function() {
              modal_ati4.style.display = "none";
            }
            window.onclick = function(event) {
              if (event.target == modal_ati4) {
                modal_ati4.style.display = "none";
              }
            }

            Highcharts.chart('c-chart4-ati', {
              chart: {
                  zoomType:'xy',
                    backgroundColor: '#202020',
                  width:1080
                },  
                title: {
                    text: "ATI <?php echo $this->ds[3][1];?>",
                style: { 
                    color: '#4aafce',
                    fontFamily: 'clock',
                    fontSize: '24px',
                    fontWeight: 'bold'
                }
                },
              xAxis: [{
                    categories:[<?php echo $tbs_ati_1;?>],
                labels: {
                        rotation: -45,
                  style: {
                              color: '#ffffff'
                      }
                    }
                }],
              yAxis: [{ 
                      labels: {
                          style: {
                              color: '#d1db62'
                          }
                      }
                  }, { 
                      labels: {
                          style: {
                              color: '#ed8474'
                          }
                      },
                      opposite: true
                  }],
                  tooltip: {
                      shared: true
                  },      
                legend: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
                itemStyle: {
                            font: '9pt Trebuchet MS, Verdana, sans-serif',
                            color: '#A0A0A0'
                  },
                  itemHoverStyle: { color: '#FFF'},
                  itemHiddenStyle: { color: '#444'}
                },
                series: [{
                    name: 'ATI Attemp',
                    data: [<?php echo $tbs_ati_2;?>],
                    type: 'spline',
                    color: '#d1db62'
                }, {
                    name: 'ATI Failed',
                    data: [<?php echo $tbs_ati_3;?>],
                    yAxis: 1,
                    type: 'spline',
                    color: '#ed8474'
                }, {
                    name: 'ATI Success',
                    data: [<?php echo $tbs_ati_4;?>],
                    type: 'spline',
                    color: '#42ce35'
                }],
            });
            </script>

    </div>
    <div class="success-dsp-area-4">
    <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        <?php echo $this->ds[3][9];?>
      </span>
    </div>
    <div class="failed-dsp-area-4">
    <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        <?php echo $this->ds[3][8];?>
      </span>
    </div>
    <div class="attemp-dsp-area-4">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        <?php echo $this->ds[3][7];?>
      </span>
    </div>
    <div class="dsp-area-4">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        DSP
      </div>
      <div id="btndsp4" style="padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;">
        <?php echo $this->ds[3][6]; ?>%
      </div>

            <div id="modaldsp-4" class="modaldsp4">
              <div class="modal-content-dsp4">
                <span class="close-dsp4">&times;</span>
              <br> 
              <div id="c-chart4-dsp"></div>
              </div>
            </div>


            <script>
            var modal_dsp4 = document.getElementById('modaldsp-4');
            var btn_dsp4 = document.getElementById("btndsp4");
            var span_dsp4 = document.getElementsByClassName("close-dsp4")[0];
            btn_dsp4.onclick = function() {
              modal_dsp4.style.display = "block";
            }
            span_dsp4.onclick = function() {
              modal_dsp4.style.display = "none";
            }
            window.onclick = function(event) {
              if (event.target == modal_dsp4) {
                modal_dsp4.style.display = "none";
              }
            }

            Highcharts.chart('c-chart4-dsp', {
              chart: {
                  zoomType:'xy',
                    backgroundColor: '#202020',
                  width:1080
                },  
                title: {
                    text: "DSP <?php echo $this->ds[3][1];?>",
                style: { 
                    color: '#4aafce',
                    fontFamily: 'clock',
                    fontSize: '24px',
                    fontWeight: 'bold'
                }
                },
              xAxis: [{
                    categories:[<?php echo $tbs_dsp_1;?>],
                labels: {
                        rotation: -45,
                  style: {
                              color: '#ffffff'
                      }
                    }
                }],
              yAxis: [{ 
                      labels: {
                          style: {
                              color: '#d1db62'
                          }
                      }
                  }, { 
                      labels: {
                          style: {
                              color: '#ed8474'
                          }
                      },
                      opposite: true
                  }],
                  tooltip: {
                      shared: true
                  },      
                legend: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
                itemStyle: {
                            font: '9pt Trebuchet MS, Verdana, sans-serif',
                            color: '#A0A0A0'
                  },
                  itemHoverStyle: { color: '#FFF'},
                  itemHiddenStyle: { color: '#444'}
                },
                series: [{
                    name: 'DSP Attemp',
                    data: [<?php echo $tbs_dsp_2;?>],
                    type: 'spline',
                    color: '#d1db62'
                }, {
                    name: 'DSP Failed',
                    data: [<?php echo $tbs_dsp_3;?>],
                    yAxis: 1,
                    type: 'spline',
                    color: '#ed8474'
                }, {
                    name: 'DSP Success',
                    data: [<?php echo $tbs_dsp_4;?>],
                    type: 'spline',
                    color: '#42ce35'
                }],
            });
            </script>

    </div>
    <div class="success-aparty-area-4">
    <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        <?php echo $this->part[3][2];?>
      </span>
    </div>
    <div class="failed-aparty-area-4">
    <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        <?php echo $this->part[3][3];?>
      </span>
    </div>
    <div class="attemp-aparty-area-4">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        <?php echo $this->part[3][1];?>
      </span>
    </div>
    <div class="aparty-area-4">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        A#Party
      </div>
      <?php echo $btnap4; ?>
    </div>

        <div id="modalap-4" class="modalap4">
          <div class="modal-content-ap4">
            <span class="close-ap4">&times;</span>
            <br>
          <div id="c-chart4-ap"></div>
          </div>
        </div>

        <script>
        var modal_ap4 = document.getElementById('modalap-4');
        var btn_ap4 = document.getElementById("btnap4");
        var span_ap4 = document.getElementsByClassName("close-ap4")[0];
        btn_ap4.onclick = function() {
          modal_ap4.style.display = "block";
        }
        span_ap4.onclick = function() {
          modal_ap4.style.display = "none";
        }
        window.onclick = function(event) {
          if (event.target == modal_ap4) {
            modal_ap4.style.display = "none";
          }
        }

        Highcharts.chart('c-chart4-ap', {
          chart: {
              zoomType:'xy',
                backgroundColor: '#202020',
              width:1080
            },  
            title: {
                text: "A#PARTY <?php echo $this->ds[3][1];?>",
            style: { 
                    color: '#4aafce',
                    fontFamily: 'clock',
                    fontSize: '24px',
                    fontWeight: 'bold'
                }
            },
          xAxis: [{
                categories:[<?php echo $tbs_party_1;?>],
            labels: {
                    rotation: -45,
              style: {
                          color: '#ffffff'
                  }
                }
            }],
          yAxis: [{ 
                  labels: {
                      style: {
                          color: '#d1db62'
                      }
                  }
              }, { 
                  labels: {
                      style: {
                          color: '#ed8474'
                      }
                  },
                  opposite: true
              }],
              tooltip: {
                  shared: true
              },      
            legend: {
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
            itemStyle: {
                        font: '9pt Trebuchet MS, Verdana, sans-serif',
                        color: '#A0A0A0'
              },
              itemHoverStyle: { color: '#FFF'},
              itemHiddenStyle: { color: '#444'}
            },
            series: [{
                name: 'A#Party Attemp',
                data: [<?php echo $tbs_party_2;?>],
                type: 'spline',
                color: '#d1db62'
            }, {
                name: 'A#Party Failed',
                data: [<?php echo $tbs_party_4;?>],
                type: 'spline',
                color: '#ed8474'
            }, {
                name: 'A#Party Success',
                data: [<?php echo $tbs_party_3;?>],
                type: 'spline',
                yAxis: 1, 
                color: '#42ce35'
            }],
        }); 
        </script>

    <div class="success-bparty-area-4">
    <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        <?php echo $this->part[3][6];?>
      </span>      
    </div>
    <div class="failed-bparty-area-4">
    <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        <?php echo $this->part[3][7];?>
      </span>
    </div>
    <div class="attemp-bparty-area-4">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        <?php echo $this->part[3][5];?>
      </span>
    </div>
    <div class="bparty-area-4">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        B#Party
      </div>
      <?php echo $btnbp4; ?>
    </div>
   
        <div id="modalbp-4" class="modalbp4">
          <div class="modal-content-bp4">
            <span class="close-bp4">&times;</span>
            <br>
          <div id="c-chart4-bp"></div>
          </div>
        </div>

        <script>
        var modal_bp4 = document.getElementById('modalbp-4');
        var btn_bp4 = document.getElementById("btnbp4");
        var span_bp4 = document.getElementsByClassName("close-bp4")[0];
        btn_bp4.onclick = function() {
          modal_bp4.style.display = "block";
        }
        span_bp4.onclick = function() {
          modal_bp4.style.display = "none";
        }
        window.onclick = function(event) {
          if (event.target == modal_bp4) {
            modal_bp4.style.display = "none";
          }
        }

        Highcharts.chart('c-chart4-bp', {
          chart: {
              zoomType:'xy',
                backgroundColor: '#202020',
              width:1080
            },  
            title: {
                text: "B#PARTY <?php echo $this->ds[3][1];?>",
            style: { 
                    color: '#4aafce',
                    fontFamily: 'clock',
                    fontSize: '24px',
                    fontWeight: 'bold'
                }
            },
          xAxis: [{
                categories:[<?php echo $tbs_party_1;?>],
            labels: {
                    rotation: -45,
              style: {
                          color: '#ffffff'
                  }
                }
            }],
          yAxis: [{ 
                  labels: {
                      style: {
                          color: '#d1db62'
                      }
                  }
              }, { 
                  labels: {
                      style: {
                          color: '#42ce35'
                      }
                  },
                  opposite: true
              }],
              tooltip: {
                  shared: true
              },      
            legend: {
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
            itemStyle: {
                        font: '9pt Trebuchet MS, Verdana, sans-serif',
                        color: '#A0A0A0'
              },
              itemHoverStyle: { color: '#FFF'},
              itemHiddenStyle: { color: '#444'}
            },
            series: [{
                name: 'B#Party Attemp',
                data: [<?php echo $tbs_party_5;?>],
                type: 'spline',
                color: '#d1db62'
            }, {
                name: 'B#Party Failed',
                data: [<?php echo $tbs_party_7;?>],
                type: 'spline',
                color: '#ed8474'
            }, {
                name: 'B#Party Success',
                data: [<?php echo $tbs_party_6;?>],
                type: 'spline',
                yAxis: 1, 
                color: '#42ce35'
            }],
        });
        </script>    

    <div class="success-business-area-4">
    <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        <?php echo $this->ds[3][5];?>
      </span>
    </div>
    <div class="failed-business-area-5">
    <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        <?php echo $this->ds[3][4];?>
      </span>
    </div>
    <div class="attemp-business-area-4">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        <?php echo $this->ds[3][3];?>
      </span>
    </div>
    <div class="business-area-4">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        Business
      </div>
      <div id="btnbisnis42" style="padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;">
        <?php echo $this->ds[3][2]; ?>%
      </div>

            <div id="modalbisnis-4" class="modalbisnis4">
              <div class="modal-content-bisnis4">
                <span class="close-bisnis4">&times;</span>
              <br>
              <div id="c-chart4-bisnis"></div>
              </div>
            </div>


            <script>
            var modal_bisnis4 = document.getElementById('modalbisnis-4');
            var btn_bisnis4 = document.getElementById("btnbisnis42");
            var span_bisnis4 = document.getElementsByClassName("close-bisnis4")[0];
            btn_bisnis4.onclick = function() {
              modal_bisnis4.style.display = "block";
            }
            span_bisnis4.onclick = function() {
              modal_bisnis4.style.display = "none";
            }
            window.onclick = function(event) {
              if (event.target == modal_bisnis4) {
                modal_bisnis4.style.display = "none";
              }
            }

            Highcharts.chart('c-chart4-bisnis', {
              chart: {
                zoomType:'xy',
                    backgroundColor: '#202020',
                  width:1080
                },  
                title: {
                    text: "BUSINESS <?php echo $this->ds[3][1];?>",
                style: { 
                    color: '#4aafce',
                    fontFamily: 'clock',
                    fontSize: '24px',
                    fontWeight: 'bold'
                }
                },
              xAxis: [{
                    categories:[<?php echo $tbs_bisnis_1;?>],
                labels: {
                        rotation: -45,
                  style: {
                              color: '#ffffff'
                      }
                    }
                }],
              yAxis: [{ 
                      labels: {
                          style: {
                              color: '#d1db62'
                          }
                      }
                  }, { 
                      labels: {
                          style: {
                              color: '#42ce35'
                          }
                      },
                      opposite: true
                  }],
                  tooltip: {
                      shared: true
                  },      
                legend: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
                itemStyle: {
                            font: '9pt Trebuchet MS, Verdana, sans-serif',
                            color: '#A0A0A0'
                  },
                  itemHoverStyle: { color: '#FFF'},
                  itemHiddenStyle: { color: '#444'}
                },
                series: [{
                    name: 'BUSINESS Attemp',
                    data: [<?php echo $tbs_bisnis_2;?>],
                    type: 'spline',
                    color: '#d1db62'
                }, {
                    name: 'BUSINESS Failed',
                    data: [<?php echo $tbs_bisnis_3;?>],
                    type: 'spline',
                    color: '#ed8474'
                }, {
                    name: 'BUSINESS Success',
                    data: [<?php echo $tbs_bisnis_4;?>],
                    type: 'spline',
                    yAxis: 1,
                    color: '#42ce35'
                }],
            }); 
            </script>

    </div>
  </div>
    
    
  <div class="area-5">
    <div class="header-area-5">---</div>
    <div class="success-ati-area-5">
    <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        ---
      </span>
    </div>
    <div class="failed-ati-area-5">
    <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        ---
      </span>
    </div>
    <div class="attemp-ati-area-5">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        ---
      </span>
    </div>
    <div class="ati-area-5">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        ATI
      </div>
      <div id="label21" style="padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;">
        100%
      </div>

            <div id="modalati-5" class="modalati5">
              <div class="modal-content-ati5">
                <span class="close-ati5">&times;</span>
              <br>
              <div id="c-chart5-ati"></div>
              </div>
            </div>


            <script>
            var modal_ati5 = document.getElementById('modalati-5');
            var btn_ati5 = document.getElementById("btnati5");
            var span_ati5 = document.getElementsByClassName("close-ati5")[0];
            btn_ati5.onclick = function() {
              modal_ati5.style.display = "block";
            }
            span_ati5.onclick = function() {
              modal_ati5.style.display = "none";
            }
            window.onclick = function(event) {
              if (event.target == modal_ati5) {
                modal_ati5.style.display = "none";
              }
            }

            Highcharts.chart('c-chart5-ati', {
              chart: {
                    backgroundColor: '#202020'
                },  
                title: {
                    text: '----',
                style: { color: '#FFFFFF'}
                },
                yAxis: {
                    title: {
                        text: '----',
                  style: { color: '#FFFFFF'}
                    }
                },
                legend: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
                itemStyle: {
                            font: '9pt Trebuchet MS, Verdana, sans-serif',
                            color: '#A0A0A0'
                  },
                  itemHoverStyle: { color: '#FFF'},
                  itemHiddenStyle: { color: '#444'}
                },
                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                        pointStart: 2010
                    }
                },
                series: [{
                    name: 'a',
                    data: [null, null, null, null, null, null, null, null]
                }, {
                    name: 'b',
                    data: [null, null, null, null, null, null, null, null]
                }, {
                    name: 'c',
                    data: [null, null, null, null, null, null, null, null]
                }],
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }
            }); 
            </script>

    </div>
    <div class="footer-area-5" id="btnupdate5">Last Update --- &#8505;</div>
        <div id="modalupdate5" class="modalupd5">
          <div class="modal-content-upd5">
            <span class="close-upd5">&times;</span>
            <p>Some text in the Modal..</p>
          </div>
        </div>

        <script>
        var modal_upd5 = document.getElementById('modalupdate5');
        var btn_upd5 = document.getElementById("btnupdate5");
        var span_upd5 = document.getElementsByClassName("close-upd5")[0];
        btn_upd5.onclick = function() {
          modal_upd5.style.display = "block";
        }
        span_upd5.onclick = function() {
          modal_upd5.style.display = "none";
        }
        window.onclick = function(event) {
          if (event.target == modal_upd5) {
            modal_upd5.style.display = "none";
          }
        }
        </script>

    <div class="success-dsp-area-5">
    <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        ---
      </span>
    </div>
    <div class="failed-area-5">
    <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        ---
      </span>
    </div>
    <div class="attemp-area-5">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        ---
      </span>
    </div>
    <div class="dsp-area-5">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        DSP
      </div>
      <div id="label22" style="padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;">
        100%
      </div>

            <div id="modaldsp-5" class="modaldsp5">
              <div class="modal-content-dsp5">
                <span class="close-dsp5">&times;</span>
                <br>
              <div id="c-chart5-dsp"></div>
              </div>
            </div>


            <script>
            var modal_dsp5 = document.getElementById('modaldsp-5');
            var btn_dsp5 = document.getElementById("btndsp5");
            var span_dsp5 = document.getElementsByClassName("close-dsp5")[0];
            btn_dsp5.onclick = function() {
              modal_dsp5.style.display = "block";
            }
            span_dsp5.onclick = function() {
              modal_dsp5.style.display = "none";
            }
            window.onclick = function(event) {
              if (event.target == modal_dsp5) {
                modal_dsp5.style.display = "none";
              }
            }

            Highcharts.chart('c-chart5-dsp', {
              chart: {
                    backgroundColor: '#202020'
                },  
                title: {
                    text: '----',
                style: { color: '#FFFFFF'}
                },

                yAxis: {
                    title: {
                        text: '----',
                  style: { color: '#FFFFFF'}
                    }
                },
                legend: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
                itemStyle: {
                            font: '9pt Trebuchet MS, Verdana, sans-serif',
                            color: '#A0A0A0'
                  },
                  itemHoverStyle: { color: '#FFF'},
                  itemHiddenStyle: { color: '#444'}
                },
                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                        pointStart: 2010
                    }
                },
                series: [{
                    name: 'a',
                    data: [null, null, null, null, null, null, null, null]
                }, {
                    name: 'b',
                    data: [null, null, null, null, null, null, null, null]
                }, {
                    name: 'c',
                    data: [null, null, null, null, null, null, null, null]
                }],
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }
            });
            </script>

    </div>
    <div class="success-aparty-area-5">
    <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        ---
      </span>
    </div>
    <div class="failed-aparty-area-5">
    <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        ---
      </span>
    </div>
    <div class="attemp-aparty-area-5">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        ---
      </span>
    </div>
    <div class="aparty-area-5">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        A#Party
      </div>
      <div id="label23" style="padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;">
        100%
      </div>

            <div id="modalap-5" class="modalap5">
              <div class="modal-content-ap5">
                <span class="close-ap5">&times;</span>
                <br>
              <div id="c-chart5-ap"></div>
              </div>
            </div>


            <script>
            var modal_ap5 = document.getElementById('modalap-5');
            var btn_ap5 = document.getElementById("btnap5");
            var span_ap5 = document.getElementsByClassName("close-ap5")[0];
            btn_ap5.onclick = function() {
              modal_ap5.style.display = "block";
            }
            span_ap5.onclick = function() {
              modal_ap5.style.display = "none";
            }
            window.onclick = function(event) {
              if (event.target == modal_ap5) {
                modal_ap5.style.display = "none";
              }
            }

            Highcharts.chart('c-chart5-ap', {
              chart: {
                    backgroundColor: '#202020'
                },  
                title: {
                    text: '----',
                style: { color: '#FFFFFF'}
                },

                yAxis: {
                    title: {
                        text: '----',
                  style: { color: '#FFFFFF'}
                    }
                },
                legend: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
                itemStyle: {
                            font: '9pt Trebuchet MS, Verdana, sans-serif',
                            color: '#A0A0A0'
                  },
                  itemHoverStyle: { color: '#FFF'},
                  itemHiddenStyle: { color: '#444'}
                },
                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                        pointStart: 2010
                    }
                },
                series: [{
                    name: 'a',
                    data: [null, null, null, null, null, null, null, null]
                }, {
                    name: 'b',
                    data: [null, null, null, null, null, null, null, null]
                }, {
                    name: 'c',
                    data: [null, null, null, null, null, null, null, null]
                }],
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }
            }); 
            </script>

    </div>
    <div class="success-bparty-area-5">
    <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        ---
      </span>
    </div>
    <div class="failed-bparty-area-5">
    <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        ---
      </span>
    </div>
    <div class="attemp-bparty-area-5">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        ---
      </span>
    </div>
    <div class="bparty-area-5">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        B#Party
      </div>
      <div id="label24" style="padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;">
        100%
      </div>

            <div id="modalbp-5" class="modalbp5">
              <div class="modal-content-bp5">
                <span class="close-bp5">&times;</span>
                <br>
              <div id="c-chart5-bp"></div>
              </div>
            </div>

            <script>
            var modal_bp5 = document.getElementById('modalbp-5');
            var btn_bp5 = document.getElementById("btnbp5");
            var span_bp5 = document.getElementsByClassName("close-bp5")[0];
            btn_bp5.onclick = function() {
              modal_bp5.style.display = "block";
            }
            span_bp5.onclick = function() {
              modal_bp5.style.display = "none";
            }
            window.onclick = function(event) {
              if (event.target == modal_bp5) {
                modal_bp5.style.display = "none";
              }
            }

            Highcharts.chart('c-chart5-bp', {
              chart: {
                    backgroundColor: '#202020'
                },  
                title: {
                    text: '----',
                style: { color: '#FFFFFF'}
                },

                yAxis: {
                    title: {
                        text: '----',
                  style: { color: '#FFFFFF'}
                    }
                },
                legend: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
                itemStyle: {
                            font: '9pt Trebuchet MS, Verdana, sans-serif',
                            color: '#A0A0A0'
                  },
                  itemHoverStyle: { color: '#FFF'},
                  itemHiddenStyle: { color: '#444'}
                },
                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                        pointStart: 2010
                    }
                },
                series: [{
                    name: 'a',
                    data: [null, null, null, null, null, null, null, null]
                }, {
                    name: 'b',
                    data: [null, null, null, null, null, null, null, null]
                }, {
                    name: 'c',
                    data: [null, null, null, null, null, null, null, null]
                }],
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }
            });
            </script>

    </div>
    <div class="success-business-area-5">
    <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        ---
      </span>
    </div>
    <div class="failed-business-area-5">
    <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        ---
      </span>
    </div>
    <div class="attemp-business-area-5">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        ---
      </span>
    </div>
    <div class="business-area-5">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        Business
      </div>
      <div id="label25" style="padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;">
        100%
      </div>

            <div id="modalbisnis-5" class="modalbisnis5">
              <div class="modal-content-bisnis5">
                <span class="close-bisnis5">&times;</span>
              <br>
              <div id="c-chart5-bisnis"></div>
              </div>
            </div>


            <script>
            var modal_bisnis5 = document.getElementById('modalbisnis-5');
            var btn_bisnis5 = document.getElementById("btnbisnis52");
            var span_bisnis5 = document.getElementsByClassName("close-bisnis5")[0];
            btn_bisnis5.onclick = function() {
              modal_bisnis5.style.display = "block";
            }
            span_bisnis5.onclick = function() {
              modal_bisnis5.style.display = "none";
            }
            window.onclick = function(event) {
              if (event.target == modal_bisnis5) {
                modal_bisnis5.style.display = "none";
              }
            }

            Highcharts.chart('c-chart5-bisnis', {
              chart: {
                    backgroundColor: '#202020'
                },  
                title: {
                    text: '----',
                style: { color: '#FFFFFF'}
                },
                yAxis: {
                    title: {
                        text: '----',
                  style: { color: '#FFFFFF'}
                    }
                },
                legend: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
                itemStyle: {
                            font: '9pt Trebuchet MS, Verdana, sans-serif',
                            color: '#A0A0A0'
                  },
                  itemHoverStyle: { color: '#FFF'},
                  itemHiddenStyle: { color: '#444'}
                },
                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                        pointStart: 2010
                    }
                },
                series: [{
                    name: 'a',
                    data: [null, null, null, null, null, null, null, null]
                }, {
                    name: 'b',
                    data: [null, null, null, null, null, null, null, null]
                }, {
                    name: 'c',
                    data: [null, null, null, null, null, null, null, null]
                }],
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }
            });
            </script>

    </div>
   </div>
    
    
    
   <div class="aeaa-6">
    <div class="header-area-6">---</div>
    <div class="success-ati-area-6">
    <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        ---
      </span>
    </div>
    <div class="failed-ati-area-6">
    <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        ---
      </span>
    </div>
    <div class="attempt-ati-area-6">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        ---
      </span>
    </div>
    <div class="ati-area-6">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        ATI
      </div>
      <div id="label26" style="padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;">
        100%
      </div>

            <div id="modalati-6" class="modalati6">
              <div class="modal-content-ati6">
                <span class="close-ati6">&times;</span>
                <br>
              <div id="c-chart6-ati"></div>
              </div>
            </div>


            <script>
            var modal_ati6 = document.getElementById('modalati-6');
            var btn_ati6 = document.getElementById("btnati6");
            var span_ati6 = document.getElementsByClassName("close-ati6")[0];
            btn_ati6.onclick = function() {
              modal_ati6.style.display = "block";
            }
            span_ati6.onclick = function() {
              modal_ati6.style.display = "none";
            }
            window.onclick = function(event) {
              if (event.target == modal_ati6) {
                modal_ati6.style.display = "none";
              }
            }

            Highcharts.chart('c-chart6-ati', {
              chart: {
                    backgroundColor: '#202020'
                },  
                title: {
                    text: '----',
                style: { color: '#FFFFFF'}
                },

                yAxis: {
                    title: {
                        text: '----',
                  style: { color: '#FFFFFF'}
                    }
                },
                legend: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
                itemStyle: {
                            font: '9pt Trebuchet MS, Verdana, sans-serif',
                            color: '#A0A0A0'
                  },
                  itemHoverStyle: { color: '#FFF'},
                  itemHiddenStyle: { color: '#444'}
                },
                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                        pointStart: 2010
                    }
                },
                series: [{
                    name: 'a',
                    data: [null, null, null, null, null, null, null, null]
                }, {
                    name: 'b',
                    data: [null, null, null, null, null, null, null, null]
                }, {
                    name: 'c',
                    data: [null, null, null, null, null, null, null, null]
                }],
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }
            });
            </script>

    </div>
    <div class="success-dsp-area-6">
    <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        ---
      </span>
    </div>
    <div class="failed-dsp-area-6">
    <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        ---
      </span>
    </div>
    <div class="attemp-dsp-area-6">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        ---
      </span>
    </div>
    <div class="dsp-area-6">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        DSP
      </div>
      <div id="label27" style="padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;">
        100%
      </div>

            <div id="modaldsp-6" class="modaldsp6">
              <div class="modal-content-dsp6">
                <span class="close-dsp6">&times;</span>
              <br>
              <div id="c-chart6-dsp"></div>
              </div>
            </div>


            <script>
            var modal_dsp6 = document.getElementById('modaldsp-6');
            var btn_dsp6 = document.getElementById("btndsp6");
            var span_dsp6 = document.getElementsByClassName("close-dsp6")[0];
            btn_dsp6.onclick = function() {
              modal_dsp6.style.display = "block";
            }
            span_dsp6.onclick = function() {
              modal_dsp6.style.display = "none";
            }
            window.onclick = function(event) {
              if (event.target == modal_dsp6) {
                modal_dsp6.style.display = "none";
              }
            }

            Highcharts.chart('c-chart6-dsp', {
              chart: {
                    backgroundColor: '#202020'
                },  
                title: {
                    text: '----',
                style: { color: '#FFFFFF'}
                },

                yAxis: {
                    title: {
                        text: '----',
                  style: { color: '#FFFFFF'}
                    }
                },
                legend: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
                itemStyle: {
                            font: '9pt Trebuchet MS, Verdana, sans-serif',
                            color: '#A0A0A0'
                  },
                  itemHoverStyle: { color: '#FFF'},
                  itemHiddenStyle: { color: '#444'}
                },
                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                        pointStart: 2010
                    }
                },
                series: [{
                    name: 'a',
                    data: [null, null, null, null, null, null, null, null]
                }, {
                    name: 'b',
                    data: [null, null, null, null, null, null, null, null]
                }, {
                    name: 'c',
                    data: [null, null, null, null, null, null, null, null]
                }],
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }
            });
            </script>

    </div>
    <div class="success-aparty-area-6">
    <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        ---
      </span>
    </div>
    <div class="failed-aparty-area-6">
    <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        ---
      </span>
    </div>
    <div class="attempt-aparty-area-6">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        ---
      </span>
    </div>
    <div class="aparty-area-6">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        A#Party
      </div>
      <div id="label28" style="padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;">
        100%
      </div>

            <div id="modalap-6" class="modalap6">
              <div class="modal-content-ap6">
                <span class="close-ap6">&times;</span>
                <br>
              <div id="c-chart6-ap"></div>
              </div>
            </div>

            <script>
            var modal_ap6 = document.getElementById('modalap-6');
            var btn_ap6 = document.getElementById("btnap6");
            var span_ap6 = document.getElementsByClassName("close-ap6")[0];
            btn_ap6.onclick = function() {
              modal_ap6.style.display = "block";
            }
            span_ap6.onclick = function() {
              modal_ap6.style.display = "none";
            }
            window.onclick = function(event) {
              if (event.target == modal_ap6) {
                modal_ap6.style.display = "none";
              }
            }

            Highcharts.chart('c-chart6-ap', {
              chart: {
                    backgroundColor: '#202020'
                },  
                title: {
                    text: '----',
                style: { color: '#FFFFFF'}
                },

                yAxis: {
                    title: {
                        text: '----',
                  style: { color: '#FFFFFF'}
                    }
                },
                legend: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
                itemStyle: {
                            font: '9pt Trebuchet MS, Verdana, sans-serif',
                            color: '#A0A0A0'
                  },
                  itemHoverStyle: { color: '#FFF'},
                  itemHiddenStyle: { color: '#444'}
                },
                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                        pointStart: 2010
                    }
                },
                series: [{
                    name: 'a',
                    data: [null, null, null, null, null, null, null, null]
                }, {
                    name: 'b',
                    data: [null, null, null, null, null, null, null, null]
                }, {
                    name: 'c',
                    data: [null, null, null, null, null, null, null, null]
                }],
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }
            });
            </script>

    </div>
    <div class="success-bparty-area-6">
    <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        ---
      </span>
    </div>
    <div class="failed-bparty-area-6">
    <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        ---
      </span>
    </div>
    <div class="attempt-bparty-area-6">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        ---
      </span>
    </div>
    <div class="success-business-area-6">
    <span style="float: left;margin-left: 1px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        SUCCESS
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#015908;">
        ---
      </span>
    </div>
    <div class="failed-business-area-6">
          <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        FAILED
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#961d10;">
        ---
      </span>
    </div>
    <div class="attempt-business-area-6">
      <span style="float: left;margin-left: 3px; font-family: arial; font-weight: bold; color:#3d3b3b;">
        ATTEMP
      </span>
      <span style="float: right;margin-right: 3px;font-family:clock;color:#464702;">
        ---
      </span>
    </div>
    <div class="footer-area-6" id="btnupdate6">Last Update --- &#8505;</div>
       
        <div id="modalupdate6" class="modalupd6">
          <div class="modal-content-upd6">
            <span class="close-upd6">&times;</span>
            <p>Some text in the Modal..</p>
          </div>
        </div>


        <script>
        var modal_upd6 = document.getElementById('modalupdate6');
        var btn_upd6 = document.getElementById("btnupdate6");
        var span_upd6 = document.getElementsByClassName("close-upd6")[0];
        btn_upd6.onclick = function() {
          modal_upd6.style.display = "block";
        }
        span_upd6.onclick = function() {
          modal_upd6.style.display = "none";
        }
        window.onclick = function(event) {
          if (event.target == modal_upd6) {
            modal_upd6.style.display = "none";
          }
        }
        </script>
       
       
    <div class="bparty-area-6">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        B#Party
      </div>
      <div id="label29" style="padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;">
        100%
      </div>

            <div id="modalbp-6" class="modalbp6">
              <div class="modal-content-bp6">
                <span class="close-bp6">&times;</span>
                <br>
              <div id="c-chart6-bp"></div>
              </div>
            </div>

            <script>
            var modal_bp6 = document.getElementById('modalbp-6');
            var btn_bp6 = document.getElementById("btnbp6");
            var span_bp6 = document.getElementsByClassName("close-bp6")[0];
            btn_bp6.onclick = function() {
              modal_bp6.style.display = "block";
            }
            span_bp6.onclick = function() {
              modal_bp6.style.display = "none";
            }
            window.onclick = function(event) {
              if (event.target == modal_bp6) {
                modal_bp6.style.display = "none";
              }
            }

            Highcharts.chart('c-chart6-bp', {
              chart: {
                    backgroundColor: '#202020'
                },  
                title: {
                    text: '----',
                style: { color: '#FFFFFF'}
                },

                yAxis: {
                    title: {
                        text: '----',
                  style: { color: '#FFFFFF'}
                    }
                },
                legend: {

                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
                itemStyle: {
                            font: '9pt Trebuchet MS, Verdana, sans-serif',
                            color: '#A0A0A0'
                  },
                  itemHoverStyle: { color: '#FFF'},
                  itemHiddenStyle: { color: '#444'}
                },
                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                        pointStart: 2010
                    }
                },
                series: [{
                    name: 'a',
                    data: [null, null, null, null, null, null, null, null]
                }, {
                    name: 'b',
                    data: [null, null, null, null, null, null, null, null]
                }, {
                    name: 'c',
                    data: [null, null, null, null, null, null, null, null]
                }],
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }
            });
            </script>      

    </div>
    <div class="business-area-6">
      <div style="float: left;margin: 0px;  border-bottom-right-radius: 110px;border: 1px solid #a5a5a5;border-top: 0;border-left: 0;width:80px;background-color: #dbdcdd;color:#427a91;font-family: fantasy;">
        Business
      </div>
      <div id="label30" style="padding: 10px;margin-right:35px; color:#d9d9db; font-size: 28pt; font-family:clock;cursor:pointer;">
        100%
      </div>

            <div id="modalbisnis-6" class="modalbisnis6">
              <div class="modal-content-bisnis6">
                <span class="close-bisnis6">&times;</span>
              <br>
              <div id="c-chart6-bisnis"></div>
              </div>
            </div>


            <script>
            var modal_bisnis6 = document.getElementById('modalbisnis-6');
            var btn_bisnis6 = document.getElementById("btnbisnis62");
            var span_bisnis6 = document.getElementsByClassName("close-bisnis6")[0];
            btn_bisnis6.onclick = function() {
              modal_bisnis6.style.display = "block";
            }
            span_bisnis6.onclick = function() {
              modal_bisnis6.style.display = "none";
            }
            window.onclick = function(event) {
              if (event.target == modal_bisnis6) {
                modal_bisnis6.style.display = "none";
              }
            }

            Highcharts.chart('c-chart6-bisnis', {
              chart: {
                    backgroundColor: '#202020'
                },  
                title: {
                    text: '----',
                style: { color: '#FFFFFF'}
                },

                yAxis: {
                    title: {
                        text: '----',
                  style: { color: '#FFFFFF'}
                    }
                },
                legend: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)',
                itemStyle: {
                            font: '9pt Trebuchet MS, Verdana, sans-serif',
                            color: '#A0A0A0'
                  },
                  itemHoverStyle: { color: '#FFF'},
                  itemHiddenStyle: { color: '#444'}
                },
                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                        pointStart: 2010
                    }
                },
                series: [{
                    name: 'a',
                    data: [null, null, null, null, null, null, null, null]
                }, {
                    name: 'b',
                    data: [null, null, null, null, null, null, null, null]
                }, {
                    name: 'c',
                    data: [null, null, null, null, null, null, null, null]
                }],
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }
            });
            </script>

    </div>
  </div>
</div>


<?PHP
$_SESSION['scriptcase']['module']['contr_erro'] = 'off'; 
//--- 
       $this->Db->Close(); 
       if ($this->Change_Menu)
       {
           $apl_menu  = $_SESSION['scriptcase']['menu_atual'];
           $Arr_rastro = array();
           if (isset($_SESSION['scriptcase']['menu_apls'][$apl_menu][$this->sc_init_menu]) && count($_SESSION['scriptcase']['menu_apls'][$apl_menu][$this->sc_init_menu]) > 1)
           {
               foreach ($_SESSION['scriptcase']['menu_apls'][$apl_menu][$this->sc_init_menu] as $menu => $apls)
               {
                  $Arr_rastro[] = "'<a href=\"" . $apls['link'] . "?script_case_init=" . $this->sc_init_menu . "&script_case_session=" . session_id() . "\" target=\"#NMIframe#\">" . $apls['label'] . "</a>'";
               }
               $ult_apl = count($Arr_rastro) - 1;
               unset($Arr_rastro[$ult_apl]);
               $rastro = implode(",", $Arr_rastro);
?>
  <script type="text/javascript">
     link_atual = new Array (<?php echo $rastro ?>);
     parent.writeFastMenu(link_atual);
  </script>
<?php
           }
           else
           {
?>
  <script type="text/javascript">
     parent.clearFastMenu();
  </script>
<?php
           }
       }
       if (isset($this->redir_modal) && !empty($this->redir_modal))
       {
?>
              <HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
               <HEAD>
                <TITLE></TITLE>
               <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php
           if ($_SESSION['scriptcase']['proc_mobile'])
           {
?>
                <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
           }
?>
                <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>                <META http-equiv="Pragma" content="no-cache"/>
                <link rel="shortcut icon" href="../_lib/img/scriptcase__NM__ico__NM__favicon.ico">
                <script type="text/javascript" src="<?php echo $this->Ini->path_prod ?>/third/jquery/js/jquery.js"></script>
        <script type="text/javascript">
          var sc_pathToTB = '<?php echo $this->Ini->path_prod ?>/third/jquery_plugin/thickbox/';
          var sc_tbLangClose = "<?php echo html_entity_decode($this->Ini->Nm_lang['lang_tb_close'], ENT_COMPAT, $_SESSION['scriptcase']['charset']) ?>";
          var sc_tbLangEsc = "<?php echo html_entity_decode($this->Ini->Nm_lang['lang_tb_esc'], ENT_COMPAT, $_SESSION['scriptcase']['charset']) ?>";
        </script>
                <script type="text/javascript" src="<?php echo $this->Ini->path_prod ?>/third/jquery_plugin/thickbox/thickbox-compressed.js"></script>
                <link rel="stylesheet" href="<?php echo $this->Ini->path_prod ?>/third/jquery_plugin/thickbox/thickbox.css" type="text/css" media="screen" />
                <script type="text/javascript"><?php echo $this->redir_modal ?></script>
               </HEAD>
              </HTML>
<?php
       } 
       exit;
   } 
   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
} 
// 
//======= =========================
   if (!function_exists("NM_is_utf8"))
   {
       include_once("../_lib/lib/php/nm_utf8.php");
   }
   if (!function_exists("SC_dir_app_ini"))
   {
       include_once("../_lib/lib/php/nm_ctrl_app_name.php");
   }
   SC_dir_app_ini('Service_Call_Addict');
   $_SESSION['scriptcase']['module']['contr_erro'] = 'off';
   $Sc_lig_md5 = false;
   $Sem_Session = (!isset($_SESSION['sc_session'])) ? true : false;
   if (!empty($_POST))
   {
       foreach ($_POST as $nmgp_var => $nmgp_val)
       {
            if (substr($nmgp_var, 0, 11) == "SC_glo_par_")
            {
                $nmgp_var = substr($nmgp_var, 11);
                $nmgp_val = $_SESSION[$nmgp_val];
            }
            if ($nmgp_var == "nmgp_parms" && substr($nmgp_val, 0, 8) == "@SC_par@")
            {
                $SC_Ind_Val = explode("@SC_par@", $nmgp_val);
                 if (count($SC_Ind_Val) == 4 && isset($_SESSION['sc_session'][$SC_Ind_Val[1]][$SC_Ind_Val[2]]['Lig_Md5'][$SC_Ind_Val[3]]))
                 {
                     $nmgp_val = $_SESSION['sc_session'][$SC_Ind_Val[1]][$SC_Ind_Val[2]]['Lig_Md5'][$SC_Ind_Val[3]];
                     $Sc_lig_md5 = true;
                 }
                 else
                 {
                     $_SESSION['sc_session']['SC_parm_violation'] = true;
                 }
            }
            nm_limpa_str_module($nmgp_val);
            $nmgp_val = NM_decode_input($nmgp_val);
            $$nmgp_var = $nmgp_val;
       }
   }
   if (!empty($_GET))
   {
       foreach ($_GET as $nmgp_var => $nmgp_val)
       {
            if (substr($nmgp_var, 0, 11) == "SC_glo_par_")
            {
                $nmgp_var = substr($nmgp_var, 11);
                $nmgp_val = $_SESSION[$nmgp_val];
            }
            if ($nmgp_var == "nmgp_parms" && substr($nmgp_val, 0, 8) == "@SC_par@")
            {
                $SC_Ind_Val = explode("@SC_par@", $nmgp_val);
                 if (count($SC_Ind_Val) == 4 && isset($_SESSION['sc_session'][$SC_Ind_Val[1]][$SC_Ind_Val[2]]['Lig_Md5'][$SC_Ind_Val[3]]))
                 {
                     $nmgp_val = $_SESSION['sc_session'][$SC_Ind_Val[1]][$SC_Ind_Val[2]]['Lig_Md5'][$SC_Ind_Val[3]];
                     $Sc_lig_md5 = true;
                 }
                 else
                 {
                     $_SESSION['sc_session']['SC_parm_violation'] = true;
                 }
            }
            nm_limpa_str_module($nmgp_val);
            $nmgp_val = NM_decode_input($nmgp_val);
            $$nmgp_var = $nmgp_val;
       }
   }
   if ($Sem_Session && (!isset($nmgp_start) || $nmgp_start != "SC")) {
       $NM_dir_atual = getcwd();
       if (empty($NM_dir_atual)) {
           $str_path_sys  = (isset($_SERVER['SCRIPT_FILENAME'])) ? $_SERVER['SCRIPT_FILENAME'] : $_SERVER['ORIG_PATH_TRANSLATED'];
           $str_path_sys  = str_replace("\\", '/', $str_path_sys);
       }
       else {
           $sc_nm_arquivo = explode("/", $_SERVER['PHP_SELF']);
           $str_path_sys  = str_replace("\\", "/", getcwd()) . "/" . $sc_nm_arquivo[count($sc_nm_arquivo)-1];
       }
       $str_path_web    = $_SERVER['PHP_SELF'];
       $str_path_web    = str_replace("\\", '/', $str_path_web);
       $str_path_web    = str_replace('//', '/', $str_path_web);
       $path_aplicacao  = substr($str_path_web, 0, strrpos($str_path_web, '/'));
       $path_aplicacao  = substr($path_aplicacao, 0, strrpos($path_aplicacao, '/'));
       $root            = substr($str_path_sys, 0, -1 * strlen($str_path_web));
       if (is_file($root . $_SESSION['scriptcase']['module']['glo_nm_path_imag_temp'] . "/sc_apl_default_Service_Call_Addict.txt")) {
           $apl_def = explode(",", file_get_contents($root . $_SESSION['scriptcase']['module']['glo_nm_path_imag_temp'] . "/sc_apl_default_Service_Call_Addict.txt"));
           if (isset($apl_def[0]) && $apl_def[0] != "module") {
               if (strtolower(substr($apl_def[0], 0 , 7)) == "http://" || strtolower(substr($apl_def[0], 0 , 8)) == "https://" || substr($apl_def[0], 0 , 2) == "..") {
                   $_SESSION['scriptcase']['module']['session_timeout']['redir'] = $apl_def[0];
               }
               else {
                   $_SESSION['scriptcase']['module']['session_timeout']['redir'] = $path_aplicacao . "/" . $apl_def[0] . "/index.php";
               }
               $Redir_tp = (isset($apl_def[1])) ? strtoupper($apl_def[1]) : "";
               $_SESSION['scriptcase']['module']['session_timeout']['redir_tp'] = $Redir_tp;
           }
       }
       if (is_file($root . $_SESSION['scriptcase']['module']['glo_nm_path_imag_temp'] . "/sc_actual_lang_" . $_SERVER['SERVER_NAME'] . ".txt")) {
           $lang_atu = file_get_contents($root . $_SESSION['scriptcase']['module']['glo_nm_path_imag_temp'] . "/sc_actual_lang_" . $_SERVER['SERVER_NAME'] . ".txt");
           $_SESSION['scriptcase']['module']['session_timeout']['lang'] = $lang_atu;
       }
   }
   if (isset($SC_lig_apl_orig) && !$Sc_lig_md5 && (!isset($nmgp_parms) || ($nmgp_parms != "SC_null" && substr($nmgp_parms, 0, 8) != "OrScLink")))
   {
       $_SESSION['sc_session']['SC_parm_violation'] = true;
   }
   if (!empty($glo_perfil))  
   { 
      $_SESSION['scriptcase']['glo_perfil'] = $glo_perfil;
   }   
   if (isset($glo_servidor)) 
   {
       $_SESSION['scriptcase']['glo_servidor'] = $glo_servidor;
   }
   if (isset($glo_banco)) 
   {
       $_SESSION['scriptcase']['glo_banco'] = $glo_banco;
   }
   if (isset($glo_tpbanco)) 
   {
       $_SESSION['scriptcase']['glo_tpbanco'] = $glo_tpbanco;
   }
   if (isset($glo_usuario)) 
   {
       $_SESSION['scriptcase']['glo_usuario'] = $glo_usuario;
   }
   if (isset($glo_senha)) 
   {
       $_SESSION['scriptcase']['glo_senha'] = $glo_senha;
   }
   if (isset($glo_senha_protect)) 
   {
       $_SESSION['scriptcase']['glo_senha_protect'] = $glo_senha_protect;
   }
   if (isset($nmgp_outra_jan) && $nmgp_outra_jan == 'true')
   {
       $script_case_init = "";
   }
   if (!isset($script_case_init) || empty($script_case_init))
   {
       $script_case_init = rand(2, 10000);
   }
   $salva_iframe = false;
   if (isset($_SESSION['sc_session'][$script_case_init]['module']['iframe_menu']))
   {
       $salva_iframe = $_SESSION['sc_session'][$script_case_init]['module']['iframe_menu'];
       unset($_SESSION['sc_session'][$script_case_init]['module']['iframe_menu']);
   }
   if (isset($nm_run_menu) && $nm_run_menu == 1)
   {
        if (isset($_SESSION['scriptcase']['sc_aba_iframe']) && isset($_SESSION['scriptcase']['sc_apl_menu_atual']))
        {
            foreach ($_SESSION['scriptcase']['sc_aba_iframe'] as $aba => $apls_aba)
            {
                if ($aba == $_SESSION['scriptcase']['sc_apl_menu_atual'])
                {
                    unset($_SESSION['scriptcase']['sc_aba_iframe'][$aba]);
                    break;
                }
            }
        }
        $_SESSION['scriptcase']['sc_apl_menu_atual'] = "module";
        $achou = false;
        if (isset($_SESSION['sc_session'][$script_case_init]))
        {
            foreach ($_SESSION['sc_session'][$script_case_init] as $nome_apl => $resto)
            {
                if ($nome_apl == 'module' || $achou)
                {
                    unset($_SESSION['sc_session'][$script_case_init][$nome_apl]);
                    if (!empty($_SESSION['sc_session'][$script_case_init][$nome_apl]))
                    {
                        $achou = true;
                    }
                }
            }
            if (!$achou && isset($nm_apl_menu))
            {
                foreach ($_SESSION['sc_session'][$script_case_init] as $nome_apl => $resto)
                {
                    if ($nome_apl == $nm_apl_menu || $achou)
                    {
                        $achou = true;
                        if ($nome_apl != $nm_apl_menu)
                        {
                            unset($_SESSION['sc_session'][$script_case_init][$nome_apl]);
                        }
                    }
                }
            }
        }
        $_SESSION['sc_session'][$script_case_init]['module']['iframe_menu'] = true;
   }
   else
   {
       $_SESSION['sc_session'][$script_case_init]['module']['iframe_menu'] = $salva_iframe;
   }

   if (!isset($_SESSION['sc_session'][$script_case_init]['module']['initialize']))
   {
       $_SESSION['sc_session'][$script_case_init]['module']['initialize'] = true;
   }
   elseif (!isset($_SERVER['HTTP_REFERER']))
   {
       $_SESSION['sc_session'][$script_case_init]['module']['initialize'] = false;
   }
   elseif (false === strpos($_SERVER['HTTP_REFERER'], '.php'))
   {
       $_SESSION['sc_session'][$script_case_init]['module']['initialize'] = true;
   }
   else
   {
       $sReferer = substr($_SERVER['HTTP_REFERER'], 0, strpos($_SERVER['HTTP_REFERER'], '.php'));
       $sReferer = substr($sReferer, strrpos($sReferer, '/') + 1);
       if ('module' == $sReferer || 'module_' == substr($sReferer, 0, 7))
       {
           $_SESSION['sc_session'][$script_case_init]['module']['initialize'] = false;
       }
       else
       {
           $_SESSION['sc_session'][$script_case_init]['module']['initialize'] = true;
       }
   }

   $_POST['script_case_init'] = $script_case_init;
   if (isset($_SESSION['scriptcase']['sc_outra_jan']) && $_SESSION['scriptcase']['sc_outra_jan'] == 'module')
   {
       $_SESSION['sc_session'][$script_case_init]['module']['sc_outra_jan'] = true;
        unset($_SESSION['scriptcase']['sc_outra_jan']);
   }
   $_SESSION['sc_session'][$script_case_init]['module']['menu_desenv'] = false;   
   if (!defined("SC_ERROR_HANDLER"))
   {
       define("SC_ERROR_HANDLER", 1);
       include_once(dirname(__FILE__) . "/module_erro.php");
   }
   if (!empty($nmgp_parms)) 
   { 
       $nmgp_parms = str_replace("@aspass@", "'", $nmgp_parms);
       $nmgp_parms = str_replace("*scout", "?@?", $nmgp_parms);
       $nmgp_parms = str_replace("*scin", "?#?", $nmgp_parms);
       $todox = str_replace("?#?@?@?", "?#?@ ?@?", $nmgp_parms);
       $todo  = explode("?@?", $todox);
       $ix = 0;
       while (!empty($todo[$ix]))
       {
            $cadapar = explode("?#?", $todo[$ix]);
            if (1 < sizeof($cadapar))
            {
                if (substr($cadapar[0], 0, 11) == "SC_glo_par_")
                {
                    $cadapar[0] = substr($cadapar[0], 11);
                    $cadapar[1] = $_SESSION[$cadapar[1]];
                }
                nm_limpa_str_module($cadapar[1]);
                if ($cadapar[1] == "@ ") {$cadapar[1] = trim($cadapar[1]); }
                $Tmp_par   = $cadapar[0];;
                $$Tmp_par = $cadapar[1];
            }
            $ix++;
       }
   } 
   $GLOBALS["NM_ERRO_IBASE"] = 0;  
   $contr_module = new module_apl();
   $contr_module->controle();
//
   function nm_limpa_str_module(&$str)
   {
       if (get_magic_quotes_gpc())
       {
           if (is_array($str))
           {
               foreach ($str as $x => $cada_str)
               {
                   $str[$x] = stripslashes($str[$x]);
               }
           }
           else
           {
               $str = stripslashes($str);
           }
       }
   }
?>
